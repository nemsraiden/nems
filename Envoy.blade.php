@servers(['web' => 'servmana@lune.fr.planethoster.net -p 3727'])


@setup
    $dir = "/home/servmana/public_html/nems";
    $dirlinks = ['storage/logs','storage/framework/sessions'];
    $filelinks = ['.env'];
    $releases = 3;
    $remote = 'git@bitbucket.org:nemsraiden/nems.git';

    $shared = $dir . '/shared';
    $current = $dir . '/current';
    $repo = $dir . '/repo';
    $release = $dir . "/releases/" . date('YmdHis');
@endsetup

@macro('deploy')
    createrelease
    composer
    laravel
    links
    linkcurrent
@endmacro

@task('prepare')
    mkdir -p {{ $shared }};
    @if($remote)
        git clone --bare {{ $remote }} {{ $repo }}
    @else
        mkdir -p {{ $repo }};
        cd {{ $repo }};
        git init --bare;
        echo  "{{ $repo }}";
    @endif
    echo "termine";
@endtask

@task('createrelease')
    mkdir -p {{ $release }};
    @if($remote)
        [ -d {{ $repo }} ] || git clone {{ $remote }} {{ $repo }};
        cd {{ $repo }};
        git remote update;
    @endif
    cd {{ $repo }};
    git archive master | tar -x -C {{ $release }};
    echo "Création de {{ $release }}";
@endtask

@task('composer')
    mkdir -p {{ $shared }}/vendor;
    ln -s {{ $shared }}/vendor {{ $release }}/vendor;
    cd {{ $release }};
    curl -sS https://getcomposer.org/installer | php;
    {{--php composer.phar install;--}}
    php composer.phar update --no-dev --no-progress;
@endtask

@task('links')
    @foreach($dirlinks as $link)
        mkdir -p {{ $shared }}/{{ $link }};
        @if(strpos($link, '/'))
            mkdir -p {{ $release }}/{{ dirname($link) }};
        @endif
        chmod 777 {{ $shared }}/{{ $link }};
        ln -s {{ $shared }}/{{ $link }} {{ $release }}/{{ $link }};
    @endforeach
    @foreach($filelinks as $link)
        ln -s {{ $shared }}/{{ $link }} {{ $release }}/{{ $link }};
    @endforeach
    echo "Liens Finished";
@endtask

@task('laravel')
    cd {{ $repo }};
    php artisan migrate;
    php artisan config:cache;
    php artisan route:cache;
    php artisan optimize;
    php artisan view:clear;
    php artisan key:generate;
@endtask

@task('linkcurrent')
    rm -rf {{ $current }};
    ln -s {{ $release }} {{ $current }};
    ls {{ $dir }}/releases | sort -r | tail -n +{{ $releases + 1 }} | xargs -I{} -r rm -rf {{ $dir }}/releases/{};
    echo "Lien // {{ $current }} --> {{ $release }}";
@endtask



@task('rollback')
    rm -f {{ $current }};
    ls {{ $dir }}/releases | tail -n 2 | head -n 1 | xargs -I{} -r ln -s {{ $dir }}/releases/{} {{ $current }};
@endtask