@extends('layout/index')

@section('title') www.nemesis-spirit.com @endsection


@section('content')


    <!-- <div class="content-padding">
        <a href="podcasts-single.html" class="top-alert-message">
            <span><span class="pod-live">Custom message</span>An event is happening tonight 20:00! Be Prepared!</span>
        </a>
    </div> -->

    <h2><span>Les dernières news</span></h2>
    <div class="content-padding">
        <div class="home-article left">
            @if(count($news) > 0)
                <span class="article-image-out">
                    <span class="image-comments"><span>{{ count($news[0]->forum_posts) }}</span></span>
                    <span class="article-image">
                        <a href="{{ route('forumTopic',[1,'News',$news[0]->id,str_slug($news[0]->title)]) }}">{!! HTML::image('images/news.png') !!}</a>
                    </span>
                </span>
                <h3><a href="{{ route('forumTopic',[1,'News',$news[0]->id,str_slug($news[0]->title)]) }}">{{$news[0]->title}}</a></h3>
                {!! $news_first_post->message_short !!}
                <div>
                    <a href="{{ route('forumTopic',[1,'News',$news[0]->id,str_slug($news[0]->title)]) }}" class="defbutton"><i class="fa fa-reply"></i>Lire en entier</a>
                </div>
            @endif
        </div>
        <div class="home-article right">
            <ul>
                <?php $i=0; ?>
                @foreach($news as $topic)
                    @if($i != 0)
                        <li>
                            <a href="post.html">
                                <span class="image-comments"><span>{{ count($topic->forum_posts) }}</span></span>
                                {!! HTML::image('images/news4.png') !!}
                                <strong>{{ $topic->title }}</strong>
                                <span class="a-txt">En savoir plus sur cette news</span>
                            </a>
                        </li>
                    @endif
                    <?php $i++; ?>
                @endforeach
            </ul>

        </div>
        <div class="clear-float"></div>
    </div>


    <h2><span>Les derniers topics du forum</span></h2>
    <div class="content-padding">

        <div class="podcast-list">
            @foreach($last_topics as $last_topic)
                <a href="{{ route('forumTopic',[$last_topic->forum_category->id,str_slug($last_topic->forum_category->nom),$last_topic->id,str_slug($last_topic->title)]) }}" class="item">
                    <span class="podcast-li-nr">{!! $last_topic->created_at !!}</span>
                    <span class="podcast-li-title">{{ $last_topic->title }}</span>
                    <span class="podcast-li-time">Posté par : <br/>{{ $last_topic->users->pseudo }}</span>
                </a>
            @endforeach
        </div>

    </div>

    <h2><span>Les dernières actions effectuées</span></h2>

    <div class="content-padding">

        <div class="podcast-list">
            @foreach($historiques as $historique)
                <div class="item">
                    <span class="podcast-li-type">
                        @if($historique->type == 'inscription')
                            {!! HTML::image('images/user_small.png') !!}
                        @elseif($historique->type == 'vip')
                            {!! HTML::image('images/king-icon_small.png') !!}
                        @elseif($historique->type == 'admin')
                            {!! HTML::image('images/admin_small.png') !!}
                        @elseif($historique->type == 'forum_news')
                            {!! HTML::image('images/news_small.png') !!}
                        @elseif($historique->type == 'forum_topic')
                            {!! HTML::image('images/talk_small.png') !!}
                        @endif
                    </span>
                    <span class="podcast-li-date">{{ $historique->created_at }}</span>
                    <span class="podcast-li-desc">
                        @if($historique->type == 'inscription')
                            <strong>{{$historique->users->pseudo}}</strong> s'est inscrit sur le site Nemesis Spirit
                        @elseif($historique->type == 'vip')
                            <strong>{{$historique->users->pseudo}}</strong> est devenu VIP sur notre serveur
                        @elseif($historique->type == 'admin')
                            <strong>{{$historique->users->pseudo}}</strong> est devenu Admin sur notre serveur
                        @elseif($historique->type == 'forum_news')
                            <strong>{{$historique->users->pseudo}}</strong> a crée une news.
                        @elseif($historique->type == 'forum_topic')
                            <strong>{{$historique->users->pseudo}}</strong> a créer un topic dans le forum
                        @endif
                    </span>
                </div>
            @endforeach
        </div>


        <br/>
        <a href="{{url('/tchat')}}">{!! HTML::image('images/Discord_présentation1.png','discord',['class' => 'responsive-img']) !!}</a>
        <br/><br/>
        <h4>Discord</h4>
        Envie de discuter avec les NemS et les habitués ? <br/>
        <br/>
        Discord est une application qui permet de discuter par écrit ou par vocal. <br/>
        <br/>
        C'est un combiné de Skype et de Teamspeak <br/>
        Pas besoin d'avoir le logiciel, un simple navigateur web suffit ! <br/>
        <br/>
        <a class="button" href="{{url('/tchat')}}">Tester Discord</a>

    </div>









@endsection
