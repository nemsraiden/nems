@extends('layout/singlepage')

@section('title','Coins')
@section('pagename','Coins')


@section('content')

    <h2>
        <span>Coins</span>
    </h2>
    <div class="content-padding">

        @include('messages.error')
        @include('messages.success')

        <div class="row">
            <div class="col s4">
                <h3>À quoi ils servent ?</h3>
                <br/>
                Les coins te permettent d'acheter des services sur les serveurs ou le site. <br/>
                Tu peux par exemple <a class="visible" href="{{ url('/bajail/vip') }}">devenir VIP </a> ou utiliser tes coins dans notre boutique (pas encore disponible).
                <br/><br/>


            </div>
            <div class="col s8">
                {!! HTML::image('images/Coins2.png','Coins', ['class' => 'responsive-img']) !!}
            </div>
        </div>

        <div class="clear-float do-the-split"></div>

        <h3 class="center-align titleblue">Comment obtenir des coins</h3>

        <br/> <br/>


        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="center-align"><h4>Grâce a la présence</h4></th>
                    <th class="center-align"><h4>Avec des jetons</h4></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="center-align"><br/>Lorsque tu es connecté sur le bajail, tu gagnes automatiquement des coins. <br/>
                        Il n'y a rien à faire de spécial.</td>
                    <td class="center-align"><br/>Tu peux utiliser tes jetons pour acheter des coins
                        <br/> <br/>
                        {!! Form::open() !!}
                        <button type="submit" class="button" style="background-color: #DB6D1D;" href="">Acheter 500 coins pour 1 jeton</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>



@endsection