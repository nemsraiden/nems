@extends('layout/singlepage')

@section('title','Acheter des jetons')
@section('pagename','Jetons')


@section('content')

    <h2>Acheter des jetons</h2>

    <div class="content-padding">

        <div class="info-message" style="background-color: #a24026;">
            <p>
                Votre code est incorrect. ! <br/>
            </p>
        </div>

        <a class="defbutton" href="{{ url('/jetons') }}">
            <i class="fa fa-arrow-left "></i>
            Revenir en arrière
        </a>

    </div>





@endsection