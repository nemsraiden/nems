@extends('layout/singlepage')

@section('title','Acheter des jetons')
@section('pagename','Jetons')


@section('content')

    <h2>Acheter des jetons</h2>

    <div class="content-padding">

        <div class="row">

            <div class="col s6">

                {!! HTML::image('images/jetons-poker.png','Acheter des jetons', ['class' => 'responsive-img']) !!}
            </div>
            <div class="col s6">
                <h2>À quoi servent-ils ?</h2>

                Les jetons te permettent principalement de devenir <strong>admin</strong> ou de devenir <strong>vip</strong>.
                <br/>
                Tu peux également <strong>acheter des coins</strong> avec des jetons ou encore les utiliser dans notre boutique.
                <br/>
                <br/>
                Pour acheter des jetons, tu dois utiliser l'un des moyens de paiement ci-dessous.
            </div>

        </div>

        <div class="breaking-line"></div>

        @if(Auth::check())

            <h2>Payer via Paypal.</h2> <br/>
            Combien de jetons voulez-vous acheter ? (1 jetons = 1 euro) :
            <br/> <br/> <br/>
            <input class="nj_jetons_paypal" name="nombre_jeton_payspal" id="nombre_jeton_payspal" type="text" size="3"  style="display: none" />

            <div class="col s12" style="text-align:center;">
                Choisis le nombre de jetons que tu désire : <br>
            </div>
            <div class="col s12">
                <div id="jetonsslider"></div>
            </div>


            <br/><br/>




            <form name="formulaire" id="formulaire" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type='hidden' value="3" name="amount" id="amount" />
                <input name="currency_code" type="hidden" value="EUR" />
                <input name="shipping" type="hidden" value="0.00" />
                <input name="tax" type="hidden" value="0.00" />
                <input name="return" type="hidden" value="http://www.nemesis-spirit.com/jetons/valide-paypal" />
                <input name="cancel_return" type="hidden" value="http://www.nemesis-spirit.com/jetons/" />
                <input name="notify_url" type="hidden" value="http://www.nemesis-spirit.com/jetons/paypal-trait" />
                <input name="cmd" type="hidden" value="_xclick" />
                <input name="business" type="hidden" value="mort4l_raiden@hotmail.com" />
                <input name="item_name" type="hidden" value="Nemesis Spirit : Achat jetons" />
                <input name="no_note" type="hidden" value="1" />
                <input name="lc" type="hidden" value="FR" />
                <input name="bn" type="hidden" value="PP-BuyNowBF" />
                <input name="custom" type="hidden" value="{{ Auth::user()->id }}" />
                <input alt="Effectuez vos paiements via PayPal : une solution rapide, gratuite et sécurisée" name="submit" id="submit" src="https://www.paypal.com/fr_FR/FR/i/btn/btn_buynow_LG.gif" type="image" style="width:auto; background:none; border:none" /><img src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" border="0" alt="" width="1" height="1" />
            </form>

            <br/><br/>
            <h2>Payer via Starpass.</h2>
            <br/>
            <div id="starpass_351527"></div><script type="text/javascript" src="http://script.starpass.fr/script.php?idd=351527&amp;verif_en_php=1&amp;datas="></script><noscript>Veuillez activer le Javascript de votre navigateur s'il vous pla&icirc;t.<br /><a href="http://www.starpass.fr/">Micro Paiement StarPass</a></noscript>

        @else
            <div class="info-message">
                <p>
                    Il faut être connecté pour acheter des jetons <br/>
                    <br/>
                    <a class="button" href="{{ url('/compte') }}">Me connecter</a>
                </p>
            </div>
        @endif

    </div>





@endsection