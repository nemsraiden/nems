@extends('layout/singlepage')

@section('title','VIP')
@section('pagename','Confirmation')
@section('sub_pagename','Bajail')
@section('sub_pagename_link',url('/bajail'))
@section('sub_pagename_2','VIP')
@section('sub_pagename_link_2',url('/bajail/vip'))


@section('content')

    <h2>VIP</h2>

    <div class="content-padding">

        <div class="row">

            @if(isset($success) && $success === true)

                @if($success_type == 'gratuit')
                    <h2>Félicitation !</h2>
                     <br/>
                    Tu es désormais VIP sur notre serveur avec le grade <strong>{{ $grade }}</strong> pour une durée de <strong>{{ $duree_txt }}</strong>
                    <br/>
                    Tu peux modifier ton grade ou la durée à tous moment. <br/>
                    <br/>
                @endif

                @if($success_type == 'devenir')
                    <h2>Félicitation !</h2>
                    <br/>
                    Tu es désormais VIP sur notre serveur avec le grade <strong>{{ $grade }}</strong> pour une durée de <strong>{{ $duree_txt }}</strong>
                    <br/>
                    Tu peux modifier ton grade ou la durée à tous moment. <br/>
                    <br/>
                @endif

            @endif





        </div>

    </div>





@endsection