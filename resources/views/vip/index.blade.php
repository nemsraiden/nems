@extends('layout/singlepage')

@section('title','VIP')
@section('pagename','VIP')
@section('sub_pagename','Bajail')
@section('sub_pagename_link',url('/bajail'))


@section('content')

    <h2>Les VIPs</h2>

    <div class="content-padding">

        <div class="row">


            <div class="col s7">
                <p>
                    Un vip est une personne qui joue régulièrement sur nos serveurs. <br/>
                    Grâce a leur fidélité, ils sont favorisés par rapport aux autres joueurs. <br/>
                    Les VIP bénéficient de différents avantages sur les serveurs Nemesis Spirit. <br/>
                    <br/>

                </p>
            </div>
            <div class="col s5">
                {!! HTML::image('images/vip.png') !!}
                <br/>
                <br/>
            </div>

            <div class="col s12"><blockquote>Les vip ont accès à un menu !vip, qui leur permet d'activer ou de désactiver certains avantages parmi une longue liste. <br/></blockquote></div>

            <div class="col s12"><div class="breaking-line"></div></div>

            <div class="space"></div>


            <div class="col s12"><div class="login-passes"><h3>Comment devenir VIP</h3></div></div>

            <div class="col s12">

                <p>Il y a deux manières possibles d'être VIP :</p>
                <br/>
                <ul class="fa-ul">
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> Payer avec des jetons.
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> Payer avec des coins.
                    </li>
                </ul>
                <br/>
                <strong>Jetons : </strong> les jetons peuvent s'acheter avec de l'argent (Allopass, Paypal, etc).<br/>
                <strong>Coins : </strong>les coins se gagnent automatiquement lorsque tu joues sur notre serveur
                <br/><br/>
                <div class="center-align col s12">
                    <a class="newdefbutton margin-right" rel="strike-modal" href="{{ url('/jetons') }}" style="margin-right: 20px">
                        <i class="fa fa-info-circle"></i>
                        En savoir plus sur les jetons
                    </a>
                    <a class="newdefbutton margin-right" rel="strike-modal" href="{{ url('/coins') }}" style="margin-left: 20px">
                        <i class="fa fa-info-circle"></i>
                        En savoir plus sur les coins
                    </a>
                </div>


            </div>

            <div class="col s12"><div class="breaking-line"></div></div>

            <div class="space"></div>

            <div class="col s12"><div class="login-passes"><h3 >Le système de crédit</h3></div></div>

            <p>Chaque vip, possède un nombre de crédit. Celui-ci dépend du grade que possède le VIP (nous verront cela plus loin)</p>
            <br/>
            <p>Les crédits permettent d'activer des options dans le menu !vip. Chaque option coûte un certain nombre de crédits, il est donc impossible de toutes les activées</p>

            <div class="col s12"><div class="breaking-line"></div></div>

            <div class="space"></div>

            <div class="col s12"><div class="login-passes"><h3 >Les avantages</h3></div></div>

            <div class="col s6">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Grenade</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Flash</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Fumi</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Armure</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vie supplémentaire</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Usp</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Gravité</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vitesse</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Transparence</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Cut à lancer</li>
                </ul>
            </div>
            <div class="col s6">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Argent</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Auto respawn</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Regénération</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Double jump</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Parachute</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vote plus important</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Ghost</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Téléportation</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Skin</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vole de Skin gardien</li>
                </ul>
            </div>

            <div class="col s12">
                <br/>
                Cette liste n'est pas exhaustive, il y a d'autres avantages à découvrir
            </div>

            <div class="col s12"><div class="breaking-line"></div></div>

            <div class="space"></div>

            <div class="col s12"><div class="login-passes"><h3>Les grades</h3></div></div>
            <div class="vip-grade">
                <div class="col s3">
                    <div class="card">
                        <div class="card-image amber">
                            <h5>VIP Basic</h5>
                        </div>
                        <div class="card-content">
                            <strong>Durée</strong>:<br/> 1 mois /<br/> 2 semaines <br/>
                            <br/>
                            <strong>Nombre de crédits</strong>:<br/>{{ $config->vip_basic_credits }} <br/>
                            <br/>
                            <strong>Prix</strong>:<br/> {{ $config->vip_basic_jetons }} Jetons /<br/> {{ $config->vip_basic_coins }} Coins
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <div class="card">
                        <div class="card-image yellow darken-3">
                            <h5>VIP Medium</h5>
                        </div>
                        <div class="card-content">
                            <strong>Durée</strong>:<br/> 1 mois /<br/> 2 semaines <br/>
                            <br/>
                            <strong>Nombre de crédits</strong>:<br/>{{ $config->vip_medium_credits }} <br/>
                            <br/>
                            <strong>Prix</strong>:<br/> {{ $config->vip_medium_jetons }} Jetons /<br/> {{ $config->vip_medium_coins }} Coins
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <div class="card">
                        <div class="card-image lime orange darken-2">
                            <h5>VIP Gold</h5>
                        </div>
                        <div class="card-content">
                            <strong>Durée</strong>:<br/> 1 mois /<br/> 2 semaines <br/>
                            <br/>
                            <strong>Nombre de crédits</strong>:<br/>{{ $config->vip_gold_credits }} <br/>
                            <br/>
                            <strong>Prix</strong>:<br/> {{ $config->vip_gold_jetons }} Jetons /<br/> {{ $config->vip_gold_coins }} Coins
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <div class="card">
                        <div class="card-image deep-orange accent-3">
                            <h5>VIP Ultimate</h5>
                        </div>
                        <div class="card-content">
                            <strong>Durée</strong>:<br/> 1 mois /<br/> 2 semaines <br/>
                            <br/>
                            <strong>Nombre de crédits</strong>:<br/>{{ $config->vip_ultimate_credits }} <br/>
                            <br/>
                            <strong>Prix</strong>:<br/> {{ $config->vip_ultimate_jetons }} Jetons
                        </div>
                    </div>
                </div>

                <div class="col s12">
                    <div class="clear-float do-the-split"></div>
                    <div class="col s4">
                        {!! HTML::image('images/promo.png') !!}
                        <br/><br/>
                    </div>

                    <div class="col s2">
                        <br/>
                    </div>
                    <div class="col s6">
                        <h4>Profite de nos promos !</h4>
                        <p>Tu veux être <strong>ADMIN</strong> et <strong>VIP</strong> ?</p>
                        <p>Tu veux être admin et animateur ?</p>
                        <br/>
                        <a class="button" style="background-color: #DB6D1D;" href="{{ url('/promo') }}">Décrouvre toutes nos promos !</a>
                    </div>
                </div>
            </div>


            <div class="col s12"><div class="breaking-line"></div></div>

            <div class="space"></div>


            @if(!Auth::check())
                <div style="text-align:center;">
                    <p>Il faut être connecté pour devenir VIP</p>
                        <br/>
                    <a class="button" style="background-color: #519623;" href="{{ url('/auth') }}">Se connecter</a>
                </div>

            @else

                {!! Form::open(['url' => '/bajail/vip/confirmation']) !!}

                @if(Auth::check() && $user->vip_firsttime)
                    <h4>Teste le vip gratuitement</h4>
                    <br/>
                    <p>La première fois que tu t'inscris sur ce site, tu peux être VIP gratuitement</p>
                    <p>Tu ne seras VIP que <strong>5 jours</strong>, profitent en ! Tu auras le <strong>grade BASIC</strong></p>

                    <br/>
                    <button type="submit" class="button" style="background-color: #519623;" href="#">Je veux devenir VIP</button>

                    {!! Form::hidden('vip_gratuit') !!}

                @else

                    <div class="col s6">
                        <ul class="list1">
                            <li>
                                <div>
                                    {!! HTML::image('images/grade.png') !!}
                                    <strong>Grade actuel</strong>
                                    <span class="a-txt">
                                        @if($user->vip_grade != null)Tu as le grade <span class="txtblue">{{ $user->vip_grade  }}</span>
                                        @else Tu n'as pas de grade @endif
                                    </span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    {!! HTML::image('images/date.png') !!}
                                    <strong>Date de fin</strong>
                                    <span class="a-txt">
                                        @if($user->vip_fin!= null)Tu es VIP jusqu'au <span class="txtblue">{{ $user->vip_fin_fr  }}</span>
                                        @else /
                                        @endif
                                    </span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    {!! HTML::image('images/credit.png') !!}
                                    <strong>Nombre de crédits</strong>
                                    <span class="a-txt">
                                    @if($user->vip_credit != null)Tu bénéficie de <span class="txtblue">{{ $user->vip_credit  }}</span> crédits
                                    @else /
                                    @endif
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="{{url('/jetons')}}">
                                    {!! HTML::image('images/jetons.png') !!}
                                    <strong>Jetons</strong>
                                    <span class="a-txt">Tu as <span class="txtblue">{{ $user->jetons  }}</span> jetons</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/coins')}}">
                                    {!! HTML::image('images/coins.png') !!}
                                    <strong>Coins</strong>
                                    <span class="a-txt">Tu as <span class="txtblue">{{ $user->coins  }}</span> coins</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div id="devenirvip" class="col s6 leftbar">

                        @if(Session::has('need_more_coins'))
                            <div class="info-message" style="background-color: #a24026;">
                                <p>Tu n'a pas assez de coins pour effectuer cet achat</p>
                            </div>
                        @endif
                        @if(Session::has('need_more_jetons'))
                            <div class="info-message" style="background-color: #a24026;">
                                <p>Tu n'a pas assez de jetons pour effectuer cet achat</p>
                            </div>
                        @endif


                        @if(!$user->is_vip)
                            <h4>Devenir VIP sur le Bajail</h4>
                        @else
                            <h4>Prolonger mon status VIP sur le Bajail</h4>
                        @endif

                        <br/>

                        <div class="row">
                            <div class="col s6 no-padding">
                                <h5>Choix du grade :</h5>
                                <br/>

                                <p>
                                    {!! Form::radio('grade','Basic', true, ['id' => 'Basic']) !!}
                                    <label for="Basic">Basic</label>
                                </p>
                                <p>
                                    {!! Form::radio('grade','Medium', false, ['id' => 'Medium']) !!}
                                    <label for="Medium">Medium</label>
                                </p>
                                <p>
                                    {!! Form::radio('grade','Gold', false, ['id' => 'Gold']) !!}
                                    <label for="Gold">Gold</label>
                                </p>
                                <p>
                                    {!! Form::radio('grade','Ultimate', false, ['id' => 'Ultimate']) !!}
                                    <label for="Ultimate">Ultimate</label>
                                </p>
                            </div>
                            <div class="col s6 no-padding">
                                <h5>Durée :</h5>
                                <br/>

                                <p>
                                    {!! Form::radio('duree','2semaine', true, ['id' => '2semaine']) !!}
                                    <label for="2semaine">2 Semaines</label>
                                </p>
                                <p>
                                    {!! Form::radio('duree','1mois', false, ['id' => '1mois']) !!}
                                    <label for="1mois">1 Mois (-10%)</label>
                                </p>
                            </div>
                        </div>

                        <div class="clear-float do-the-split"></div>

                        @if(!$user->is_vip)
                            {!! Form::hidden('vip_devenir') !!}
                        @else
                            {!! Form::hidden('vip_prolonger') !!}
                        @endif


                        <div class="col s6">
                            <button name="grade_prixjetons" id="grade_prixjetons" type="submit" class="info-block">
                                <b >{{ $config->vip_basic_jetons }}</b>
                                <span>Jetons</span>
                            </button>
                        </div>

                        <div class="col s6">
                            <button name="grade_prixcoins" id="grade_prixcoins" type="submit" class="info-block">
                                <b >{{ $config->vip_basic_coins }}</b>
                                <span>Coins</span>
                            </button>
                        </div>


                    </div>

                @endif
            @endif

            {!! Form::close() !!}

            <div class="col s12"><div class="breaking-line"></div></div>

            <div class="space"></div>

            <div class="col s12"><div class="login-passes"><h3>Liste des VIPs</h3></div></div>

            <table class="descriptionJoueur" >
                <thead>
                <tr>
                    <th></th>
                    <th>Pseudo</th>
                    <th>SteamID</th>
                    <th>Grade</th>
                    <th>Date de début</th>
                    <th>Date de fin</th>
                </tr>
                </thead>

                <tbody>
                <?php $i=0; ?>
                @foreach($users as $user)
                    <tr>
                        <td><a target="_blank" href="http://steamcommunity.com/profiles/{{ $users_link[$i] }}">{!! HTML::image('images/steam2.png','steam',['width' => '30']) !!}</a></td>
                        <td>{{$user->pseudo}}</td>
                        <td>{{$user->steamid}}</td>
                        <td>{{ucfirst($user->vip_grade)}}</td>
                        <td>{{$user->vip_debut_fr}}</td>
                        <td>{{$user->vip_fin_fr}}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>



        </div>

    </div>





@endsection

<script>
    var basic_jetons = parseInt({{ $config->vip_basic_jetons }});
    var basic_coins = parseInt({{ $config->vip_basic_coins }});
    var medium_jetons = parseInt({{ $config->vip_medium_jetons }});
    var medium_coins = parseInt({{ $config->vip_medium_coins }});
    var gold_jetons = parseInt({{ $config->vip_gold_jetons }});
    var gold_coins = parseInt({{ $config->vip_gold_coins }});
    var ultimate_jetons = parseInt({{ $config->vip_ultimate_jetons }});
    var ultimate_coins = parseInt({{ $config->vip_ultimate_coins }});


</script>