@extends('layout/singlepage')

@section('title')
    @if($profil_exist)
        Profil de {{$profil->pseudo}}
    @else
        ce profil n'existe pas
    @endif
@endsection
@section('pagename')
    @if($profil_exist)
        Profil de {{$profil->pseudo}}
    @else
        ce profil n'existe pas
    @endif
@endsection


@section('content')


    <h2>@if($profil_exist)
            Profil de {{$profil->pseudo}}
        @else
            ce profil n'existe pas
        @endif
    </h2>

    <div class="content-padding">

        @if($profil_exist)

            <div class="row">
                <div class="col s4">
                    @if($profil->avatar == null)
                        {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive']) !!}
                    @else
                        <img src="{{url('/')}}/uploads/{{$profil->avatar}}" alt="{{$profil->pseudo}} avatar"/>
                    @endif
                        <br/><br/>
                        @if($profil->steamid != null)
                            <a target="_blank" href="http://steamcommunity.com/profiles/{{ $profil->steam_profil }}">
                                {!! HTML::image('images/steam_01.png','',['class' => 'img-responsive', 'width' => '140']) !!}
                            </a>
                        @endif


                </div>
                <div class="col s8">
                    <ul class="profile-info">
                        <li>
                            <span class="first">Login</span>
                            <span class="last">{{$profil->login}}</span>
                        </li>
                        <li>
                            <span class="first">Pseudo</span>
                            <span class="last">{{$profil->pseudo}}</span>
                        </li>
                        <li>
                            <span class="first">Email</span>
                            <span class="last">{{$profil->email}}</span>
                        </li>
                        <li>
                            <span class="first">Steamid</span>
                            <span class="last"><a target="_blank" href="http://steamcommunity.com/profiles/{{$profil->steam_profil}}">{{$profil->steamid}}</a></span>
                        </li>
                        <li>
                            <span class="first">Steamid3</span>
                            <span class="last">{{$profil->steamid3}}</span>
                        </li>
                        <li>
                            <span class="first">Pays</span>
                            <span class="last">{{$profil->pays}}</span>
                        </li>
                        <li>
                            <span class="first">Date de naissance</span>
                            <span class="last">{{$profil->birthday}}</span>
                        </li>
                        <li>
                            <span class="first">Jeux</span>
                            <span class="last">
                                @foreach($profil->jeux as $jeux)
                                    {!! HTML::image($jeux) !!}
                                @endforeach
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="breaking-line"></div>
                <div class="col s4 center">
                    @if($profil->is_admin)
                        <div style="height:210px">
                            {!! HTML::image('images/Policeman_Icon_256.png','', ['class' => 'responsive-img']) !!} <br/>
                        </div>
                        <h4>Ce joueur est Admin !</h4>
                    @endif
                </div>
                <div class="col s4 center">
                    @if($profil->is_vip)
                        <div style="height:210px">
                            {!! HTML::image('images/vip.png','', ['class' => 'responsive-img']) !!} <br/>
                        </div>
                        <h4>Ce joueur est VIP !</h4>
                    @endif
                </div>
                <div class="col s4 center">
                    @if($profil->is_nems)
                        <div style="height:210px">
                            {!! HTML::image('images/nems-au-four.jpg','', ['class' => 'responsive-img']) !!} <br/>
                        </div>
                        <h4>Ce joueur est NemS !</h4>
                    @endif
                </div>


            </div>
        @else
            <div class="info-message" style="background-color: #a24026;">
                <p>
                    Ce profil n'existe pas.
                </p>
            </div>
        @endif


    </div>



@endsection