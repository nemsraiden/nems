<div id="header-top">
    <div class="wrapper">
        @if(!Auth::check())
            <ul class="load-responsive" rel="Top menu">
                <li><a href="{{ url('/auth/register') }}">Créer un nouveau compte</a></li>
                <li><a href="{{ url('/compte') }}">J'ai déjà un compte</a></li>
            </ul>
        @else
            <ul class="right">
                <li><a href="{{ url('/jetons') }}">Mes jetons<small>{{ Auth::user()->jetons }}</small></a></li>
                <li><a href="{{ url('/coins') }}">Mes coins<small>{{ Auth::user()->coins }}</small></a></li>
            </ul>
            <ul class="load-responsive" rel="Top menu">
                <li><a href="{{url('/compte')}}">Connecté en tant que {{ Auth::user()->pseudo }}</a></li>
            </ul>
        @endif
    </div>
</div>