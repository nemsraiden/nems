@include('layout/common_top')
<body class="has-top-menu @yield('bodyclass')">



<!-- BEGIN #top-layer -->
<div id="top-layer">

    @include('layout/header_topmenu')

    <section id="content">

        @yield('header')

        <div id="main-box" class="full-width">

            <div id="main">

                @yield('content')

            </div>

            <div class="clear-float"></div>

        </div>

    </section>

    <!-- END #top-layer -->
</div>

<div class="clear-float"></div>
@yield('footer')


<div class="clear-float"></div>

@include('layout/common_bot')
