
<header id="header">
    <div id="menu-bottom">
        <!-- <nav id="menu" class="main-menu width-fluid"> -->
        @include('layout/header_menu');
    </div>
    <div id="slider">
        <div id="slider-info">
            <div class="padding-box">
                <ul>
                    <li>
                        <h2><a href="post.html">Counter strike : Global Offensive</a></h2>
                        <p>Les NemS sont régulièrement présents sur CSGO. Cependant, nous y jouons uniquement pour faire des compétitions.</p>
                    </li>
                    <li class="dis">
                        <h2><a href="post.html">League of legends</a></h2>
                        <p>Qui ne connaît pas encore ce célèbre jeu ? En ranked ou en normal, nous jouons souvent entre nous.</p>
                    </li>
                    <li class="dis">
                        <h2><a href="post.html">Counter Strike Source</a></h2>
                        <p>Retour aux origines ! Notre présence est moins affirmée qu'avant, mais notre Bajail tient bon !</p>
                    </li>
                    <li class="dis">
                        <h2><a href="post.html">Les NemS en vrai !</a></h2>
                        <p>Que ce soit en lan, en camping, au karting,... de nombreux NemS se voient régulièrement. Toi aussi rencontre les NemS en vrai :)</p>
                    </li>
                </ul>
            </div>
            <a href="javascript: featSelect(1);" id="featSelect-1" class="featured-select this-active">
                <span class="w-bar" id="feat-countdown-bar-1">.</span>
                <span class="w-coin" id="feat-countdown-1">0</span>
                <img src="images/header_csgo_75.png" alt="" title="" />
            </a>
            <a href="javascript: featSelect(2);" id="featSelect-2" class="featured-select">
                <span class="w-bar" id="feat-countdown-bar-2">.</span>
                <span class="w-coin" id="feat-countdown-2">0</span>
                <img src="images/header_league_of_legends3_75.png" alt="" title="" />
            </a>
            <a href="javascript: featSelect(3);" id="featSelect-3" class="featured-select">
                <span class="w-bar" id="feat-countdown-bar-3">.</span>
                <span class="w-coin" id="feat-countdown-3">0</span>
                <img src="images/header_counter_strike__source_75.png" alt="" title="" />
            </a>
            <a href="javascript: featSelect(4);" id="featSelect-4" class="featured-select">
                <span class="w-bar" id="feat-countdown-bar-4">.</span>
                <span class="w-coin" id="feat-countdown-4">0</span>
                <img src="images/dreamhack_75.png" alt="" title="" />
            </a>
        </div>
    </div>
</header>

<script>

</script>