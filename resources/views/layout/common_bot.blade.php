
{!! HTML::script('js/jquery-1.11.2.min.js') !!}
{!! HTML::script('js/modernizr.custom.50878.js') !!}
{!! HTML::script('js/iscroll.js') !!}
{!! HTML::script('js/dat-menu.js') !!}
{!! HTML::script('js/materialize.js') !!}
{!! HTML::script('js/jquery-ui.js') !!}
{!! HTML::script('js/nouislider.min.js') !!}

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('select').material_select();

        $("#birthday").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat : 'dd/mm/yy',
            firstDay: 1,
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
                'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
                'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            minDate: "-60Y",//go back 20 years
            maxDate: "+0",
            yearRange: 'c-100:c+10'
        });

        $("ul.home_menu .bajail").click(function(e){
            e.preventDefault();
            e.stopPropagation();

            if($("ul.home_menu .sous-menu").css('display') ==  'none'){
                $("ul.home_menu .sous-menu").slideDown(0500);
            }
            else if($("ul.home_menu .sous-menu").css('display') ==  'block'){
                $("ul.home_menu .sous-menu:visible").slideUp(0500);
            }

        });

        $("table.SimpleCalendar tbody td div.event").parent().addClass('eventParent');

        $("table.SimpleCalendar tbody td div.event").each(function(){
            var html = $(this).html();
            $(this).parent().attr('title',html);
        });

        $( ".eventParent" ).tooltip();

        if($("#ckeditor").length){
            CKEDITOR.replace( 'ckeditor' );
        }
        if($("#ckeditor_full").length){
            CKEDITOR.replace( 'ckeditor_full', {
                customConfig: '/vendor/unisharp/laravel-ckeditor/config_full.js',
                // Custom stylesheet for the contents
                contentsCss : '/css/ckeditor.css'

            });
        }


    });
</script>

@if (Request::is('/'))

<script type='text/javascript'>
    var strike_featCount = 4;
    var strike_autostart = true;
    var strike_autoTime = 7000;

    window.document.onload = function(e){starFnc();}
    window.onload = function(e){starFnc();}

    jQuery(document).scroll(function() {
        var position = jQuery(window).scrollTop();
        if(position <= 180) {
            jQuery("#header-top ul li a span").removeClass('gotop');
        }else{
            jQuery("#header-top ul li a span").addClass('gotop');
        }

        if(position >= 600) {
            if(strike_autostart){
                freezeAnim();
            }
        }

        if(jQuery(".the-profile-top").length > 0){
            var thetop = jQuery(".the-profile-top").offset();
            var naviheight = parseInt(jQuery(".the-profile-navi").css("height"))+parseInt(position);
            var leeet = parseInt(jQuery(document).height())-(naviheight);

            if(jQuery(".footer").height()+50 >= leeet){
                var fixedstyle = (jQuery(document).height()-((parseInt(jQuery(".footer").height())+50)+parseInt(jQuery(".the-profile-navi").height())))-parseInt(thetop.top);
                jQuery(".the-profile-navi").addClass("bottom");
                jQuery(".the-profile-navi").css("top", fixedstyle+"px");
                return false;
            }else{
                jQuery(".the-profile-navi").removeClass("bottom");
                jQuery(".the-profile-navi").css("top", "0px");
            }

            var theheight = parseInt(jQuery(".the-profile-top").css("height"))+parseInt(jQuery(".the-profile-top").css("padding-top"))+parseInt(jQuery(".the-profile-top").css("padding-bottom"));
            if(position >= thetop.top+theheight){
                jQuery(".the-profile-navi").addClass("floating");
            }else{
                jQuery(".the-profile-navi").removeClass("floating");
            }
        }
    });

</script>

@endif

{!! HTML::script('js/theme-script.js') !!}

{!! HTML::script('js/main.js') !!}


</body>
</html>