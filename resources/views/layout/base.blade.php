@include('layout/common_top')
<body class="has-top-menu @yield('bodyclass')">

@if (array_key_exists('ishome', View::getSections()))
    <!-- BEGIN #slider-imgs -->
    <div id="slider-imgs">
        <div class="featured-img-box">
            <div id="featured-img-1" class="featured-img"></div>
            <div id="featured-img-2" class="featured-img invisible"></div>
            <div id="featured-img-3" class="featured-img invisible"></div>
            <div id="featured-img-4" class="featured-img invisible"></div>
        </div>
        <!-- END #slider-imgs -->
    </div>
@endif


<!-- BEGIN #top-layer -->
<div id="top-layer">

    @include('layout/header_topmenu')

    <section id="content">

        @yield('header')

        <div id="main-box">

            <div id="main">

                @yield('content')

            </div>

            @yield('sidebar')

            <div class="clear-float"></div>

        </div>

    </section>

    <!-- END #top-layer -->
</div>

<div class="clear-float"></div>
@yield('footer')


<div class="clear-float"></div>

@include('layout/common_bot')
