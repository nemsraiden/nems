<nav id="menu" class="main-menu">
    <div class="blur-before"></div>
    <a href="{{ url() }}" class="header-logo left"><img src="{{ url('/') }}/images/logo.png" class="logo" alt="Revelio" title="" /></a>
    <a href="#dat-menu" class="datmenu-prompt"><i class="fa fa-bars"></i>Show menu</a>
    <ul class="load-responsive" rel="Main menu">
        <li><a href="{{ url('/') }}"><span><i class="fa fa-home "></i><strong>Home</strong></span></a></li>
        <li><a href="{{ url('/forum') }}"><span><i class="fa fa-comment"></i><strong>Forum</strong></span></a></li>

        <li><a href="{{url('/bajail')}}"><span><i class="fa fa-users"></i><strong>Ba-Jail</strong></span></a>
            <ul class="sub-menu">
                <li><a href="{{url('/bajail')}}">Informations</a></li>
                <li><a href="{{url('/bajail/admin')}}">Admin</a></li>
                <li><a href="{{url('/bajail/vip')}}">Vip</a></li>
                <li><a href="{{url('/bajail/regles')}}">Règles</a></li>
                <li><a href="{{url('/nemesis-spirit/sourceban')}}">Sourceban</a></li>
            </ul>
        </li>
        <li><a href="{{ url('/compte') }}"><span><i class="fa fa-cogs "></i><strong>Mon compte</strong></span></a></li>

        <li><a href="{{ url('/tchat') }}"><span><i class="fa  fa-microphone"></i><strong>Serveur vocal</strong></span></a></li>

        <li><a href="{{ url('/team-nems') }}"><span><i class="fa  fa-cutlery"></i><strong>Les NemS</strong></span></a></li>

        <!-- <li class="no-icon"><a href="#"><strong>Without icon</strong></a></li> -->
    </ul>
</nav>

