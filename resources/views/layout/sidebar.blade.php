<!-- BEGIN #sidebar -->
<div id="sidebar">


    <!-- BEGIN .panel -->
    <div class="panel">
        <h2>Mon compte</h2>
        <div class="panel-content">

            @if(Auth::check())


                <br/>
                <div class="info-blocks">
                    <ul style="margin-left: 20px">
                        <li>
                            <a class="info-block" href="{{ url('/jetons') }}">
                                <b>{{ $user->jetons }}</b>
                                <span>Jetons</span>
                            </a>
                        </li>
                        <li>
                            <a class="info-block" href="{{ url('/coins') }}">
                                <b>{{ $user->coins }}</b>
                                <span>Coins</span>
                            </a>
                        </li>
                    </ul>
                </div>



                <hr/>
                <div class="center-align">
                    <h4><u>Status sur les serveurs</u></h4>
                    @if($user->is_admin)
                        <ul>
                            <li>Tu est actuellement admin <br/></li>
                            <li>Type de droit : <strong>{{ $user-> type_droit }}</strong> <br/></li>
                            <li>Date de fin : <strong>{{ $user->admin_fin }}</strong> <br/></li>
                        </ul>
                        <br/>
                        <a class="defbutton" href="{{ url('/bajail/admin') }}">
                            <i class="fa fa-cogs"></i>
                            Gérer mes droits
                        </a>

                    @else
                        Tu n'es pas admin sur notre serveur. <br/>
                        <br/>
                        <a class="defbutton" href="{{ url('/bajail/admin') }}">
                            <i class="fa fa-cogs"></i>
                            Devenir admin
                        </a>
                    @endif

                    <br/> <br/>
                    @if($user->is_vip)
                        <ul style="margin-bottom: 30px">
                            <li>Tu est VIP sur le Bajail <br/></li>
                            <li>Grade actuel : <strong>{{ $user->vip_grade }}</strong> <br/></li>
                            <li>Date de fin : <strong>{{ $user->vip_fin_fr }}</strong> <br/></li>
                        </ul>
                        <a class="defbutton" href="{{ url('/bajail/vip') }}">
                            <i class="fa fa-cogs"></i>
                            Gérer mon status VIP
                        </a>

                    @else
                        Tu n'es pas VIP sur le Bajail. <br/>
                        <br/>
                        <a class="defbutton" href="{{ url('/bajail/vip') }}">
                            <i class="fa fa-cogs"></i>
                            Devenir VIP
                        </a>
                    @endif

                    <br/>
                    <hr/>

                    <a  href="{{url('/compte')}}" class="button">Consulter mon compte</a>
                </div>


            @else

                <div class="the-form">
                    {!! Form::open(array('url' => 'auth/login')) !!}
                    {!! Form::label('Pseudo') !!}
                    {!! Form::text('login') !!}
                    {!! Form::label('Password') !!}
                    <input type="checkbox" name="remember" style="display:none" checked>
                    {!! Form::password('password') !!}
                    <br/><br/>
                    <button class="btn btn-default" type="submit">Connexion</button>
                    {!! Form::close() !!}

                    <div class="helper">
                        <a href="{{url('/auth/register')}}"> > Créer un nouveau compte</a> <br/>
                        <a href="{{ url('/password/email') }}"> > Mot de passe oublié</a>
                    </div>

                </div>

            @endif

        </div>
    </div>


    <!-- BEGIN .panel -->
    <div class="panel">
        <h2>Menu</h2>
        <div class="panel-content no-padding">
            <ul class="home_menu">
                <li class="prior"><a href="{{ url('/') }}">&raquo; Home</a></li>
                <li class="prior"><a href="{{ url('/forum') }}">&raquo; Forum</a></li>

                <li class="prior">
                    <a href="#" class="bajail">&raquo; Bajail</a>
                    <ul class="sous-menu">
                        <li><a href="{{ url('/bajail') }}">Informations</a></li>
                        <li><a href="{{ url('/bajail/admin') }}">Admin</a></li>
                        <li><a href="{{ url('/bajail/vip') }}">VIP</a></li>
                        <li><a href="{{ url('/bajail/regles') }}">Règles</a></li>
                        <li><a href="{{ url('/sourceban') }}">Sourceban</a></li>
                    </ul>
                </li>
                <li class="prior"><a href="{{ url('/compte') }}">&raquo; Mon compte</a></li>
                <li class="prior"><a href="{{ url('/tchat') }}">&raquo; Serveur vocal</a></li>
                <li class="prior"><a href="{{ url('/team-nems') }}">&raquo; Les NemS</a></li>


            </ul>
        </div>
    </div>


    <!-- BEGIN .panel -->
    <div class="panel">
        <h2>Derniers posts</h2>
        <div class="panel-content no-padding">

            <div class="new-forum-line">
                @foreach($sidebar_last_posts as $sidebar_last_post)
                    <a href="{{ route('userProfil',['name' => str_slug($sidebar_last_post->user->pseudo), 'id' => $sidebar_last_post->user->id]) }}" class="avatar @if($sidebar_last_post->user->is_online) online @endif user-tooltip">
                        <div class="user-tooltip-id" style="display: none">{{$sidebar_last_post->user->id}}</div>
                        @if($sidebar_last_post->user->avatar == null)
                            {!! HTML::image('/uploads/tux-counter.png','',['width' => '40','height' => '40']) !!}
                        @else
                            <img src="{{url('/')}}/uploads/{{$sidebar_last_post->user->avatar}}" alt="{{$sidebar_last_post->user->pseudo}}" width="40px" height="40px"/>
                        @endif
                    </a>
                    <a href="{{ route('forumTopic',[$sidebar_last_post->forum_topics->forum_categories_id,$sidebar_last_post->forum_topics->parent_name,$sidebar_last_post->forum_topics->id,str_slug($sidebar_last_post->forum_topics->title)]) }}" class="f_content">
                        <strong>{{ $sidebar_last_post->forum_topics->title }}</strong>
                        <span><b class="user-tooltip">{{ $sidebar_last_post->user->pseudo }}</b>, {{ $sidebar_last_post->date }}</span>
                    </a>
                @endforeach
            </div>

        </div>
        <!-- END .panel -->
    </div>

     <div class="panel">
        <h2>{{$online_register}} Joueurs connecté, {{$online_guests}} Guest</h2>

        <div class="panel-content">

            <ul class="with-online-users">

                @foreach($online_users as $online_user)
                    <li>
                        <a class="avatar online user-tooltip" href="{{ route('userProfil',['name' => str_slug($online_user->user->pseudo), 'id' => $online_user->user->id]) }}">
                            <div class="user-tooltip-id" style="display: none">{{$online_user->user->id}}</div>
                            @if($online_user->user->avatar == null)
                                {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive','width' => '39', 'height' => '39']) !!}
                            @else
                                <img src="{{url('/')}}/uploads/{{$online_user->user->avatar}}" alt="{{$online_user->user->pseudo}} avatar" width="39" height="39"  />
                            @endif
                        </a>

                    </li>
                @endforeach
            </ul>
            <div class="clear-float"></div>

        </div>
    </div>




    <div class="panel">


        <div class="panel-content">

            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- NemS -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-1016722955468876"
                 data-ad-slot="6081097947"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

            <div class="clear-float"></div>

        </div>
    </div>



    <!-- BEGIN .panel -->
    <div class="panel">
        <h2>Calendrier</h2>
        <div class="panel-content">

            <div><div class="calendar-birthday-exemple"></div> Anniversaire</div>

            <table class="SimpleCalendar" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th colspan="7">{{ date('F') }} {{ date('Y') }}</th>
                </tr>
                </thead>
            </table>
            {!! $calendar !!}


        </div>
        <!-- END .panel -->
    </div>

    <!-- END #sidebar -->
</div>