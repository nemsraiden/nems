<!DOCTYPE HTML>
<html lang = "en">
<head>
    <meta charset="utf-8" />
    <title>Nemesis Spirit | @yield('title')</title>
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta property="og:type" content="company" />
    <meta property="og:title" content="Nemesis Spirit | @yield('title')" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="" />
    <meta property="og:site_name" content="Nemesis Spirit | @yield('title')" />
    <link rel="shortcut icon" type="image/png" href="images/favicon.ico" />

    {!! HTML::style('css/materialize.css') !!}
    {!! HTML::style('css/reset.css') !!}
    {!! HTML::style('css/font-awesome.min.css') !!}
    {!! HTML::style('css/dat-menu.css') !!}
    {!! HTML::style('css/main-stylesheet.css') !!}
    {!! HTML::style('css/responsive.css') !!}
    {!! HTML::style('css/style.css') !!}
    {!! HTML::style('css/jquery-ui.css') !!}
    {!! HTML::style('css/SimpleCalendar.css') !!}
    {!! HTML::style('css/nouislider.min.css') !!}



    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Oswald:300,400,700|Source+Sans+Pro:300,400,600,700&amp;subset=latin,latin-ext" />
    <!--[if lt IE 9 ]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #featured-img-1 {
            background-image: url('{{url('/')}}/images/header_csgo.png');
        }
        #featured-img-2 {
            background-image: url('{{url('/')}}/images/header_league_of_legends3.png');
        }
        #featured-img-3 {
            background-image: url('{{url('/')}}/images/header_counter_strike__source.png');
        }
        #featured-img-4 {
            background-image: url('{{url('/')}}/images/dreamhack.png');
        }

        /* Man content & sidebar top lne, default #256193 */
        #sidebar .panel,
        #main-box #main {
            border-top: 5px solid #256193;
        }

        /* Slider colors, default #256193 */
        a.featured-select,
        #slider-info .padding-box ul li:before,
        .home-article.right ul li a:hover {
            background-color: #256193;
        }

        /* Button color, default #256193 */
        .panel-duel-voting .panel-duel-vote a {
            background-color: #256193;
        }

        /* Menu background color, default #000 */
        #menu-bottom.blurred #menu > .blur-before:after {
            background-color: #000;
        }

        /* Top menu background, default #0D0D0D */
        #header-top {
            background: #0D0D0D;
        }

        /* Sidebar panel titles color, default #333333 */
        #sidebar .panel > h2 {
            color: #333333;
        }

        /* Main titles color, default #353535 */
        #main h2 span {
            color: #353535;
        }

        /* Selection color, default #256193 */
        ::selection {
            background: #256193;
        }

        /* Links hover color, default #256193 */
        .article-icons a:hover,
        a:hover {
            color: #256193;
        }

        /* Image hover background, default #256193 */
        .article-image-out,
        .article-image {
            background: #256193;
        }

        /* Image hover icons color, default #256193 */
        span.article-image span .fa {
            color: #256193;
        }
    </style>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52257844-4', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<!--<body class="no-slider">-->