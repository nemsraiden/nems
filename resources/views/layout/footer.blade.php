<div class="wrapper">
    <!-- BEGIN .footer -->
    <div class="footer">

        <div class="footer-top"></div>

        <div class="footer-content">

            <!-- BEGIN .panel -->
            <div class="panel">
                <h2>Nemesis Spirit</h2>
                <div class="panel-content">

                    <h4>Notre histoire en quelques mots</h4>

                    <p>Nemesis-Spirit est une team qui a démarrer  sur Age of Mythology il y a 10 ans et évolue désormais sur CSS, CSGO et League of legends.</p>

                    <p>Nous organisons régulièrement des events, mais également des rencontres IRL (lan, activités sportives, voyages, etc).</p>

                </div>
                <!-- END .panel -->
            </div>

            <!-- BEGIN .panel -->
            <div class="panel">
                <h2>Nos serveurs</h2>
                <div class="panel-content">

                    <div>
                        <h4>Comment jouer avec nous ?</h4>

                        <p>Tu peux nous rejoindre sur notre serveur Counter-Strike où  tu peux aussi venir nous parler sur notre serveur vocal</p>

                        <p>
                            <strong>Serveur CSS</strong> : 77.111.254.83:6666 <br/>
                            <strong>Serveur vocal</strong> : <a href="{{ url('/tchat') }}">Nous rejoindre sur discord</a>
                        </p>



                    </div>

                </div>
                <!-- END .panel -->
            </div>

            <!-- BEGIN .panel -->
            <div class="panel">
                <h2>Contact</h2>
                <div class="panel-content">

                    <div>
                        <h4>Contacter les NemS</h4>


                        <a href="{{ url('/forum') }}" class="icon-line">
                            <i class="fa fa-comments"></i><span>Le forum des NemS</span>
                        </a>

                        <a href="{{ url('/tchat') }}" class="icon-line">
                            <i class="fa fa-microphone"></i><span>Serveur vocal</span>
                        </a>

                        

                    </div>

                </div>
                <!-- END .panel -->
            </div>

        </div>

        <div class="footer-bottom">
            <div class="left">&copy; Copyright 2015 <strong>Nemesis Spirit</strong> designed by <strong>NemSRaiden</strong></div>
            <div class="right">
                <ul>
                    <li><a href="{{ url('/') }}">Homepage</a></li>
                    <li><a href="{{ url('/forum') }}">Forum</a></li>
                    <li><a href="{{ url('/compte') }}">Mon compte</a></li>
                    <li><a href="{{ url('/bajail') }}">Bajail</a></li>
                    <li><a href="{{ url('/tchat') }}">Serveur vocal</a></li>
                    <li><a href="{{ url('/nems') }}">Les NemS</a></li>
                </ul>
            </div>
            <div class="clear-float"></div>
        </div>

        <!-- END .footer -->
    </div>
</div>