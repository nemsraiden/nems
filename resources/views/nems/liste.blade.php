@extends('layout/singlepage')

@section('title','Liste des NemS')
@section('pagename','Liste des NemS')
@section('sub_pagename','nems')
@section('sub_pagename_link',url('/nems'))


@section('content')

    <h2>Liste des NemS</h2>

    <div class="content-padding">

            <p> <u>Informations</u> : <br/>
                Beaucoup de NemS ne sont plus actifs sur le site et ne seront donc pas dans cette liste
                <br/> Pour une liste plus complète, consulter
                <a class="visible" href="http://steamcommunity.com/groups/nemesisspirit" target="_blank">notre groupe steam</a></p>
        <br/>
        <a class="button" style="background-color: #519623;" href="{{ url('/nems') }}"><i class="fa fa-arrow-left"></i>
            Revenir à l'histoire des NemS</a> <br/>
        <br/>
        <br/>

            <div class="staff-block">

                @foreach($users as $user_)
                    <div class="item">
                        <div>
                            <a href="{{ route('userProfil',['name' => str_slug($user_->pseudo), 'id' => $user_->id]) }}">
                                @if($user_->avatar == null)
                                    {!! HTML::image('/uploads/tux-counter.png','',['class' => 'item-photo']) !!}
                                @else
                                    <img class="item-photo" src="{{url('/')}}/uploads/{{$user_->avatar}}" alt=""/>
                                @endif
                            </a>

                        </div>
                        <div class="item-content">
                            <a href="{{ route('userProfil',['name' => str_slug($user_->pseudo), 'id' => $user_->id]) }}">
                                <h3>
                                    {{ $user_->pseudo }}
                                </h3>
                            </a>
                            <div style="height: 40px">
                                @if($user_->is_admin)
                                    <span>Admin</span>
                                @endif
                                @if($user_->is_vip)
                                    <span>VIP</span>
                                @endif
                            </div>
                            @if($user_->jeux_css == 'on')
                            {!! HTML::image('images/icon_css.png') !!}
                            @endif

                            @if($user_->jeux_csgo == 'on')
                            {!! HTML::image('images/icon_csgo.png') !!}
                            @endif

                            @if($user_->jeux_lol == 'on')
                            {!! HTML::image('images/icon_lol.png') !!}
                            @endif
                        </div>
                    </div>
                @endforeach




            </div>

    </div>





@endsection
