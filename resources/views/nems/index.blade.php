@extends('layout/singlepage')

@section('title','La team et les membres')
@section('pagename','Les NemS')


@section('content')

    <h2>Nemesis Spirit</h2>

    <div class="content-padding">

        <div class="row">

            <blockquote>Nemesis Spirit signifie "Esprit de vengeance" dans <br/>la mythologie grecque</blockquote>
            <br/><br/>
            <div class="col s12 center-align" style="margin-bottom: 40px">
                <a class="button" style="background-color: #519623; margin-right: 5px" href="{{ url('team-nems/liste') }}">Afficher la liste des NemS</a>
                <a class="button" style="background-color: #519623; margin-left: 5px;" href="http://steamcommunity.com/groups/nemesisspirit" target="_blank">Consulter notre groupe steam</a>
            </div>
            <h3>Les origines</h3>
            <br/>
            <div class="col s2">
                {!! HTML::image('images/nems-au-four.jpg','NemS au four', ['class' => 'responsive-img']) !!}
            </div>
            <div class="col s10">
                Il y a déjà plus de 10 ans, Les NemS ont été créés par plusieurs personnes. <br/>
                C'est une team qui est issue d'une autre team, issue elle aussi d'une autre team.  <br/>
            </div>
            <br/>
            <div class="col s12">
                <div class="clear-float do-the-split"></div>
            </div>
            <div class="col s12">
                <div class="col s8 ">
                    <h4>Les NemS et age of mythology</h4>
                    <br/>
                    Les NemS est une team qui a démarré sur Age of Mythology. <br/>
                    <br/>
                    Nous avons joué sur ce jeux et également a son expansion pendant un long moment. <br/>
                    <br/>
                    Au meilleur de leur forme, les NemS était une des meilleur team francophone, elle a été "élue" team la plus fair-play pendant plusieurs saisons.
                    <br/>
                    Nous avons participé plusieurs fois à un tournois (aom-arena) très connus par la communauté. <br/>
                    <br/>
                    Malheureusement pour nous, Age of Empire 3 est arrivé... <br/>
                    Très peu de NemS ont apprécié ce Jeux, la team est "morte", petit à petit....
                </div>
                <div class="col s4">
                    {!! HTML::image('images/Age_of_Mythology_Liner.jpg','Age of mythology', ['class' => 'responsive-img']) !!}
                </div>
            </div>

            <div class="col s12">
                <br/><br/>
            </div>


            <div class=" col s12">
                <div class="col s7">
                    {!! HTML::image('images/CSS-counter-strike-33379437-1024-768.jpg','Counter strike source', ['class' => 'responsive-img circle']) !!}
                </div>
                <div class="col s5">
                    <h4>Une question de survie !</h4>
                    <br/>
                    J'ai donc dû chercher un  autre jeu.
                    <br/>
                    La réputation de Counter strike source n'était déjà plus à faire, je l'ai très vite adopté. <br/>
                    <br/>
                    D'autres NemS, m'ont suivi tel quel Lee, Moctab, L, Daisuke, ... <br/>
                    Le but, c'était avant tout de continuer à faire vivre les NemS, a notre manière... <br/>
                </div>
            </div>

            <div class="col s12">
                <br/><br/>
            </div>


            <div class=" col s12 nemsstory">
                <div class="col s10 offset-s1 center-align  ">

                    <br/>
                    <h4>Un mode de jeux étrange</h4>
                    <br/>
                    On a passé plusieurs mois sur un serveur (Community of newbie). je ne saurais plus dire combien, mais assez longtemps pour qu'ils me mettent admin et que j'améliore mon skill légendaire dans ce jeux (hum...)
                    <br/>
                    <br/>
                    {!! HTML::image('images/Ba_jail_mia_blackops_beta60003.png','Counter strike source : bajail', ['class' => 'responsive-img ']) !!}
                    <br/>
                    <br/>
                    Un jour, je ne sais pas trop comment, NemSLee a découvert un mode de jeux étrange et assez incompréhensible. <br/>
                    Très vite, j'ai accroché, le principe était génial. <br/>
                    À ce moment, on jouait sur une seul map, il n'y avait jamais d'admin, mais ce n'était pas trop le bordel.<br/>
                    <br/>
                    On a très vite fait des connaissances. On devenait important et apprécié sur le serveur. <br/>
                    Petit à petit, on constatait qu'il était peut-être temps pour les NemS de prendre un nouveau départ...
                    <br/>
                </div>
            </div>

            <div class=" col s12">

                <div class="col s7">
                    {!! HTML::image('images/phoenix_png.png','renaissance des nems', ['class' => 'responsive-img']) !!}
                </div>

                <div class="col s5">
                    <br/>
                    <h4>La renaissance des NemS</h4>
                    <br/>
                    Nous avons rapidement décidé de créer notre propre serveur. <br/>
                    <br/>
                    Une énorme majorité des habitués de l'ancien serveur nous ont logiquement suivis sur le nouveau. <br/>
                    <br/>
                    Cette fois, c'était sûr, les NemS étaient  de retour. L'histoire des NemS sur CSS a commencé il y a plus de 8 ans...
                </div>

                <div class="col s12">
                    <br/><br/>
                </div>

                <div class="col s6">
                    <br/>
                    <h4>L'âge d'or des Nemesis Spirit</h4>
                    <br/>
                    Les années suivantes, notre popularité a énormément augmenté. <br/>
                    De nombreuses personnes ont rejoint  la team NemS. <br/>
                    <br/>
                    Nous avons eu différents serveur, tel quel le Zombie, Gungame, FFA, Hide & Seek, etc. <br/>
                    Il était fréquent de voir notre serveur multigame full en même temps que le bajail. <br/>
                    <br/>


                </div>

                <div class="col s6">
                    {!! HTML::image('images/gold2.png','age dor des nems', ['class' => 'responsive-img']) !!}
                </div>

                <div class="col s12">
                    <br/><br/>
                </div>

                <div class="col s4">
                    {!! HTML::image('images/p2I18CW.png','les events', ['class' => 'responsive-img']) !!}
                </div>

                <div class="col s8">
                    <br/>
                    <h4>Des évents et du changement</h4>
                    <br/>
                    Notre moment de gloire a duré 2-3 ans. <br/>
                    Cette période coïncide avec la présence de membres tel que Devil, Piro, Melocake, Artiez, etc. <br/>
                    <br/>
                    Nous organisions des wars presque tous les soirs pour jouer ensemble. <br/>
                    <br/>
                    C'est aussi une période où  nous avons organisé beaucoup d'évents.
                    Le plus célèbre aujourd'hui, reste celui des "Jeux Olympique". <br/>
                    On avait tellement de succès, qu'on recrutait les nouveaux via un système de "star academy"


                </div>

            </div>

            <div class="col s12">
                <br/><br/>
                <br/><br/>
            </div>

            <div class=" col s12">

                <div class="col s12 center-align">
                    {!! HTML::image('images/760x350_beach-holidays-167598243-thinkstock.jpg','camping nems', ['class' => 'responsive-img circle']) !!}
                    <br/>
                    <br/>
                    <h4>Les NemS en vacances</h4>
                    <br/>
                    Les NemS ont commencé à se rencontrer en vrai. <br/>
                    Cela a commencé il y a déjà plusieurs années avec le tout premier camping NemS en Alsace il y a plus de 7 ans. <br/>
                    <br/>
                    Mais cela a surtout pris une autre tournure avec "la filière belge" de la team. Nous avons organisé des lans, du karting, des soirées et également d'autres camping
                    <br/>
                    <br/>
                    Si on me demande aujourd'hui quelle  est la force des NemS, je répondrais sans hésiter qu'on est devenu une bande de potes en vrai.
                    <br/>
                    Il est certain  que tout les NemS ne font pas parti de ce trip, mais une bonne partie des NemS, principalement les anciens, se connaissent désormais en vrai
                    <br/>
                </div>

            </div>

            <div class="col s12">
                <br/><br/>
                {!! HTML::image('images/separator.png','', ['class' => 'responsive-img']) !!}
                <br/><br/>
            </div>

            <div class="col s12 center-align">
                <div class="col s12">
                    <h4>Une histoire qui touche a sa fin ?</h4>
                    <br/>
                    Après tant d'années, il fallait bien que les NemS se retirent petit à petit. <br/>
                    On ne possède plus qu'un serveur sur CSS et il est assez rare d'y voir des NemS <br/>
                    Beaucoup d'ancien ne joue plus ou ont changé de jeux et il y a eu peu de nouveaux ces derniers mois / années.
                    <br/>
                    <br/>
                    Il reste cependant de l'espoir... <br/>
                    <br/>
                </div>

                <div class="col s6">
                    <h5>Counter Strike : Global Offensive</h5>
                    <br/>
                    {!! HTML::image('images/csgo_wallpaper.png','CSGO', ['class' => 'responsive-img']) !!} <br/>
                    <br/>
                    CSGO est en partie responsable  de la "fin" de son prédécesseur. <br/>
                    De nombreuses personnes ont désertées CSS pour se tourner faire CSGO qui est beaucoup plus orienté pour la compétition.
                    <br/>
                    <br/>
                    Nous avons décidé de rester sur css et le Bajail pour la partie fun, mais de se tourner vers CSGO pour la compétition.

                </div>
                <div class="col s6">
                    <h5>League of Legends</h5>
                    <br/>
                    {!! HTML::image('images/lol_wallpaper.png','League of Legends', ['class' => 'responsive-img']) !!}
                    <br/>
                    <br/>
                    League of legend est lui aussi en partie responsable du manque d'activité sur CSS ou CSGO <br/>
                    C'est devenu un incontournable  dans les jeux vidéo. Addictif et passionnants, beaucoup de NemS se sont tourner vers ce jeu
                    <br/>
                </div>

            </div>

            <div class="col s12">
                <br/><br/><br/><br/>
            </div>

            <div class="col s12">
                <div class="col s12 center-align">
                    {!! HTML::image('images/whats-next.png','Whats next', ['class' => 'responsive-img']) !!}
                    <br/>
                    <br/>
                    La question que l'on peut se poser, quel sera l'avenir des NemS ? <br/>
                    C'est difficile a dire... <br/>
                    <br/>
                    J'aimerais continuer à faire vivre le bajail aussi longtemps que possible. <br/>
                    Idéalement, je voudrais améliorer l'utilisation d'un serveur vocal pour rapprocher les NemS. <br/>
                    Organiser plus régulièrement des soirées sur des jeux tels que Left for dead, Age of mythology ou encore des wars CSGO et des ranked sur Lol
                    <br/>
                    <br/>
                    Je reste cependant lucide, la belle époque des NemS au niveau gaming est terminée. <br/>
                    <br/>
                    Je suis surtout concentré et intéressé par la partie IRL de la team. <br/>
                    Le camping 2016, la Dreamhack, le chalet NemS, des Lans et soirées, etc... <br/>
                    <br/>
                    <br/>
                    <blockquote>Les NemS ont beaucoup changé avec le temps. <br/>C'est désormais un concept, des potes, une partie de nous.</blockquote>

                </div>
            </div>












        </div>

    </div>





@endsection