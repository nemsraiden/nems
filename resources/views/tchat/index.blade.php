@extends('layout/doublepage')

@section('title','Serveur vocal des NemS')
@section('pagename','Serveur vocal')


@section('content')

    <style type="text/css">
        #main-box.full-width #main{
            padding-top: 0px;
            padding-bottom: 0px;
        }
        .row , .col.s5, .col.s7{
            margin: 0px;
            padding: 0px;
        }
    </style>
    <div class="row">
        <div class="col s5">
            <iframe src="https://discordapp.com/widget?id=96968204064137216&theme=dark&username={{$login}}" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>

        </div>
        <div class="col s7">
            <br/>
            <img src="http://vaughnwhiskey.tv/wp-content/uploads/2015/09/pb_rUu4L_400x400.jpg" width="500" alt=""/>
            <br/>
            <br/>

            <blockquote>It's time to ditch Skype and TeamSpeak. Discord is here.</blockquote>

            <p>
                All-in-one voice and text chat for gamers that’s free, secure, and works on both your desktop and phone. Stop paying for TeamSpeak servers and hassling with Skype. Simplify your life.
                <br/>
                <br/>
                <a style="background-color: #519623;" href="https://discord.gg/0Xa4uWZIu375JzS5" class="button">
                    Se connecter a Discord
                </a>
            </p>

        </div>
    </div>



@endsection