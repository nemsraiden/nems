@extends('layout/singlepage')

@section('title','Password oublié')
@section('pagename','Password oublié')


@section('content')

    <h2>
        <span>Récupérer mon mot de passe</span>
    </h2>
    <div class="content-padding">

        <div class="info-message" style="background-color: #75a226;">
            <a class="close-info" href="#">
                <i class="fa fa-times"></i>
            </a>
            <p>
                {{ Session::get('status') }}
            </p>
        </div>

        <form method="POST" action="/password/email">
            {!! csrf_field() !!}

            @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <div>
                <div class="input-field">
                    <input type="email" name="email" value="{{ old('email') }}">
                    <label for="email">Email</label>
                </div>
            </div>

            <div>
                <button class="btn waves-effect waves-light" type="submit">
                    Récupérer mon password
                </button>
            </div>
        </form>

    </div>



@endsection