@extends('layout/singlepage')

@section('title','Password oublié : récupération')
@section('pagename','Password oublié : récupération')


@section('content')

    <h2>
        <span>Modifier mon mot de passe</span>
    </h2>
    <div class="content-padding">

        <form method="POST" action="/password/reset">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

            @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <div class="input-field">
                <input type="email" name="email" value="{{ old('email') }}">
                <label for="">Email</label>
            </div>

            <div class="input-field">
                <input type="password" name="password">
                <label for="">Password</label>
            </div>

            <div class="input-field">
                <input type="password" name="password_confirmation">
                <label for="">Confirmer le password</label>
            </div>

            <div class="input-field">
                <button class="btn waves-effect waves-light" type="submit">
                    Modifier mon mot de passe
                </button>
            </div>
        </form>

    </div>



@endsection