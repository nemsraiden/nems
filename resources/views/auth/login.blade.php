@extends('layout/singlepage')

@section('title','Connexion')
@section('pagename','Connexion')


@section('content')





        <div class="signup-panel">
            <div class="left">
                <h2>
                    <span>Connexion au site</span>
                </h2>
                <div class="content-padding">

                    @if( count($errors) > 0)

                        <div class="info-message" style="background-color: #a24026;">
                            <a class="close-info" href="#">
                                <i class="fa fa-times"></i>
                            </a>
                            <p>
                                {{ $errors->first() }}
                            </p>
                        </div>

                    @endif

                    <div class="row">
                        <div class="login-passes">
                            <form method="POST" action="/auth/login">
                                {!! csrf_field() !!}
                                <div class="input-field">
                                    <input id="login" name="login" type="text" class="validate" value="{{old('login')}}">
                                    <label for="login">Login</label>
                                </div>
                                <div class="input-field">
                                    <input id="password" name="password" type="password" class="validate">
                                    <label for="password">Password</label>
                                </div>
                                <br/>
                                <input type="checkbox" name="remember" style="display:none" checked>
                                <button class="btn waves-effect waves-light" type="submit" name="action">Connexion</button>
                                <br/><br/>
                                <a href="{{ url('/password/email') }}">Mot de passe oublié ?</a>
                            </form>

                        </div>
                    </div>



                </div>
            </div>
            <div class="right">
                <h2><span>Inscription</span></h2>
                <div class="content-padding">


                <p>Prends 2 minutes pour t'inscrire, tu pourras faire partie de la communauté  NemS.</p>
                <p>Cela te permettra de participer à notre forum, de devenir admin et vip sur notre serveur, ...</p>
                <br/>
                <a href="{{ url('auth/register') }}" class="waves-effect waves-light btn">S'inscrire</a>
                </div>
            </div>
        </div>

@endsection