@extends('layout/singlepage')

@section('title','Inscription')
@section('pagename','Inscription')


@section('content')

    <h2>
        <span>Inscription</span>
    </h2>
    <div class="content-padding">

        <form method="POST" action="/auth/register">
            {!! csrf_field() !!}

            <p>En vous inscrivant, vous pourrez gérer votre compte sur le site Nemesis Spirit.
                Vous pourrez devenir VIP, admin, gérer vos jetons et vos coins, discuter sur notre forum, bénéficier de différents bonus et avantages, etc.</p>



            <hr class="dashed"/>

            @if(count($errors) > 0)
                <span class="the-error-msg">
                <i class="fa fa-warning"></i>
                    @if(count($errors) == 0)Une erreur a été trouvées :@endif
                    @if(count($errors) >= 0)Des erreurs ont été trouvées :@endif
                    <br/>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                    </ul>

                </span>
            @endif


            <div class="row">
                <div class="input-field col s12"><h3>Informations principales</h3></div>
                <div class="input-field col s12">
                    <input name="email" type="text" class="validate @if($errors->default->first('email') != '') invalid @endif" value="{{ old('email') }}">
                    <label for="email">Email</label>
                </div>

                <div class="input-field col s12">
                    <input name="login" type="text" class="validate @if($errors->default->first('login') != '') invalid @endif " value="{{ old('login') }}" >
                    <label for="login">Login</label>
                </div>

                <div class="input-field col s6">
                        <input name="password" type="password" class="validate @if($errors->default->first('password') != '') invalid @endif" >
                        <label for="password">Password</label>
                </div>
                <div class="input-field col s6">
                    <input name="password_confirmation" type="password" class="validate @if($errors->default->first('password_comfirmation') != '') invalid @endif" >
                    <label for="password_confirmation">Password confirmation</label>
                </div>


                <div class="input-field col s12"><h3>Informations secondaires</h3></div>

                <div class="input-field col s12">
                    <input name="pseudo" type="text" class="validate @if($errors->default->first('pseudo') != '') invalid @endif" value="{{ old('pseudo') }}" >
                    <label for="pseudo">Pseudo visible</label>
                </div>

                <div class="input-field col s12">
                    {!! Form::select('pays', array('France' => 'France', 'Belgique' => 'Belgique', 'Suisse' => 'Suisse', 'Autres' => 'Autres'), old('pays')) !!}
                    <label>Pays</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" id="birthday" name="birthday" value="{{ old('birthday') }}" />
                    <label for="birthday">Date de naissance</label>
                </div>

                <div class="input-field col s12"><h4>A quels jeux tu joues ?</h4></div>


                <div class="col s4">
                    <br>
                    <input type="checkbox" id="jeux_css" name="jeux_css" @if (old('jeux_css') == 'on') checked @endif />
                    <label for="jeux_css">CS : Source</label>
                </div>

                <div class="col s4">
                    <br>
                    <input type="checkbox" id="jeux_csgo" name="jeux_csgo" @if (old('jeux_csgo') == 'on') checked @endif />
                    <label for="jeux_csgo">CS : Global offensive</label>
                </div>

                <div class="col s4">
                    <br>
                    <input type="checkbox" id="jeux_lol" name="jeux_lol" @if (old('jeux_lol') == 'on') checked @endif />
                    <label for="jeux_lol">League of legends</label>
                </div>

                <div class="input-field col s12" style="margin-top: 20px"></div>

                {!! Recaptcha::render([ 'lang' => 'fr' ]) !!}


                <div class="col s12" style="margin-top: 60px">
                    <br/><br/>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Inscription</button>
                </div>





            </div>

        </form>

    </div>



@endsection