@extends('layout/singlepage')

@section('title','Test des règles')
@section('sub_pagename','BaJail')
@section('sub_pagename_link',url('/bajail'))
@section('sub_pagename_2','Admin')
@section('sub_pagename_link_2',url('/bajail/admin'))
@section('pagename','Test des règles')


@section('content')

    <h2>Faire le test des règles</h2>

    <div class="content-padding">

        <div class="row">

           @if(Session::has('test_before_admin'))
                <div class="info-message">
                    <p>Avant de devenir admin, tu dois réussir ce test</p>
                    <p>Ce test vérifiera que tu connais un minimum les règles du Bajail</p>
                </div>
           @endif

            @if(isset($reussi))


                <h3>Les résultats obtenu</h3>
                   <br/>
                   @if($reussi)
                       <div class="info-message" style="background-color: #75a226;">
                           <p>Félicitation ! Tu as réussi le test !</p>
                       </div>
                       <div class="row">
                           <div class="col s12">
                               <a class="defbutton" style="width: 100%; text-align:center;" href="{{ url('bajail/admin/devenir-admin') }}">
                                   Je veux devenir admin
                               </a>
                           </div>
                       </div>

                   @else
                       <div class="info-message" style="background-color: #a24026;">
                           <p>Dommage, tu as raté le test :(</p>
                       </div>
                   @endif

                <div style="text-align: center; padding: 20px 20px 40px 20px"><strong>Nombre de points : {{$points}} / 10 (7 minimum)</strong></div>

                @foreach($resultats as $resultat)

                    <div>
                        <strong>{{$resultat['question']}}</strong> <br/>
                        @if($resultat['bien_repondu'] == 'oui')
                            <span class="light-green lighten-2">Tu as bien répondu</span>,
                        @elseif($resultat['bien_repondu'] == 'non')
                            <span class="red lighten-2">Tu as mal répondu</span>,
                        @elseif($resultat['bien_repondu'] == 'no_rep')
                            <span class="grey lighten-1">Tu n'a pas répondu</span>,
                        @endif
                        la réponsse était : {{$resultat['reponsse']}} <br/>
                    </div>
                        <br/>


                @endforeach

                       <div class="row">
                           <div class="col s12">
                               <a class="defbutton" style="width: 100%; text-align:center;" href="{{ url('bajail/admin/devenir-admin') }}">
                                   Je veux devenir admin
                               </a>
                           </div>
                       </div>

                    <div class="breaking-line"></div>

            @endif

           <h3>Le principe du test</h3>

           <p>Tu dois répondre à <strong>10 questions</strong> sur les règles du Bajail</p>
           <p>Pour réussir le test, il te faut au <strong>minimum 7 points</strong></p>
           <br/>
           <p>1 bonne réponse = 1 point</p>
           <p>1 mauvaise réponse = -1 point</p>
           <p>Tu ne perds pas de point si tu ne réponds pas !</p>
           <br/>

               @if($bajail_test_etat == null)
                    <p>Tu ne pourras faire ce test que 2 fois !</p>
               @elseif($bajail_test_etat == 'rate_1')
                   <p class="red-text darken-3">Tu ne peux plus faire ce test que 1 fois !</p>
               @endif


               <div class="breaking-line"></div>

               <h3>Le test</h3>

               @if($bajail_test_etat == 'rate_2')
                   <br/>
                   <p class="red-text darken-3">Tu as déjà raté ce test 2 fois, tu ne peux plus recomencer <br/>
                   <p>Si tu veux obtenir une chance supplémentaire, demande sur le forum</p>

               @elseif($bajail_test_etat == 'reussi')
                   <br/>
                   <p class="light-green-text darken-3">Tu as déjà réussi ce test</p>

               @else

                   {!! Form::open() !!}
                   <div class="accordion">

                       @foreach( $questions as $question)
                           <div class="accordion-tab">
                               <a href="#">{{ $question->question }}</a>
                               <div class="accordion-block" style="display: none;">
                                   <p>
                                       {!! Form::radio('q'.$question->id,'oui', false, ['id' => 'q'.$question->id.'_oui']) !!}
                                       <label for="q{{$question->id}}_oui">Oui</label>
                                       {!! Form::radio('q'.$question->id,'non', false, ['id' => 'q'.$question->id.'_non']) !!}
                                       <label for="q{{$question->id}}_non">Non</label>
                                       {!! Form::radio('q'.$question->id,'ne_sais_pas', true, ['id' => 'q'.$question->id.'_ne_sais_pas']) !!}
                                       <label for="q{{$question->id}}_ne_sais_pas">Je ne sais pas</label>
                                   </p>
                               </div>
                           </div>
                       @endforeach

                   </div>

                   <button id="send_rep" type="submit" class="button greenbtn"  >Envoyer mes réponses</button>

                   {!! Form::close() !!}

               @endif


        </div>

    </div>




@endsection