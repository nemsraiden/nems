@extends('layout/singlepage')

@section('title','BaJail')
@section('pagename','BaJail')


@section('content')

    <h2>Serveur Bajail</h2>

    <div class="content-padding">

        <div class="row">

            <div class="col s12"><div class="login-passes"><h3 >Le mode de jeux</h3></div></div>

            <br/><br/>

            <div class="col s8">
                <p>Le ba_jail est un mode basé sur le jeu vidéo en ligne Counter Strike Source (CSS) simulant la vie carcérale. L’équipe des terroristes étant les prisonniers, et les anti-terroristes les gardiens.
                    Pour l’emporter, l’objectif du terroriste est d’éliminer l’intégralité des gardiens quant aux gardiens (Cts) leurs buts est de maîtriser et surveiller les prisonniers durant un temps imparti, tout en veillant à leurs propres vies.</p>

            </div>
            <div class="col s4">
                {!! HTML::image('images/ba_jail_electric_razor_v6.jpg','Bajail',['class' => 'responsive-img circle', 'width' => '150px']) !!}
                <br/><br/>
            </div>

            <div class="col s12"><div class="login-passes"><h3 >Informations</h3></div></div>

            <div class="col s3">
                {!! HTML::image('images/steam.png') !!}
            </div>

            <div class="col s9">
                <b>Ip :</b> 77.111.254.83:6666 <br/>
                <b>Nombre de place :</b> 30 <br/>
                <b>Map Principal :</b> ba_jail_electric_razor_v6 <br/>
                <br/>

            </div>



            <div class="col s12"><div class="login-passes"><h3 >Les règles</h3></div></div>

            <div class="col s3">
                {!! HTML::image('images/Law_hammer.png') !!}
            </div>

            <div class="col s9">
                <p>Contrairement aux autres mode de jeux de css, il est impossible de s'amuser sur un bajail avec des personnes qui ne respecte pas les règles</p>
                <p>Nous avons donc défini  une liste de règles à respecter.</p>
                <p><u>Attention, elles sont différentes des autres serveurs.</u></p>
                <br/>
                <a class="defbutton" href="{{ url('/bajail/regles') }}">
                    <i class="fa fa-plus"></i>
                    Découvrir les règles complètes
                </a> <br/>
                <br/>

            </div>





            <div class="col s12"><div class="login-passes"><h3 >Type de joueur : Administrateur</h3></div></div>

            <div class="col s3">
                {!! HTML::image('images/Policeman_Icon_256.png') !!}
            </div>

            <div class="col s9">
                <p>Un admin est une personne qui possède des droits sur le serveur</p>
                <p>Cette personne a accès au menu !admin. Elle peut gérer le serveur, faire respecter l'ordre et la discipline</p>
                <p>Sur le BaJail, sont principal objectif est de faire respecter les règles</p>
                <br/>
                <a class="defbutton" href="{{ url('/bajail/admin') }}">
                    <i class="fa fa-plus"></i>
                    En savoir plus sur les admins
                </a> <br/>
                <br/><br/>

            </div>





            <div class="col s12"><div class="login-passes"><h3 >Type de joueur : Vip</h3></div></div>

            <div class="col s3">
                {!! HTML::image('images/king-icon.png') !!}
            </div>

            <div class="col s9">
                <p>Un VIP est un joueur un peu particulier du serveur</p>
                <p>Un VIP possède différents avantages sur le serveur. Un vip pourra par exemple lancer des cuts, avoir plus de vie, avoir le double jump, etc</p>
                <br/>
                <a class="defbutton" href="{{ url('/bajail/vip') }}">
                    <i class="fa fa-plus"></i>
                    En savoir plus sur les vips
                </a> <br/>
                <br/>

            </div>





            <div class="col s12"><div class="login-passes"><h3 >Type de joueur : Animateurs</h3></div></div>

            <div class="col s3">
                {!! HTML::image('images/clown.png') !!}
            </div>

            <div class="col s9">
                <p>Un VIP animateur est un tout nouveau status sur nos serveurs</p>
                <p>Un animateur a pour mission, comme son nom l'indique, d'animer le serveur.</p>
                <p>Pour ce faire, il a accès au menu !minijeux. Il peut donc faire des mini jeux tel que le cache-cache, bataille de missile, etc</p>
                <br/>
                <a class="defbutton" href="{{ url('/bajail/animateur') }}">
                    <i class="fa fa-plus"></i>
                    Pas encore disponible
                </a> <br/>
                <br/>

            </div>



        </div>

    </div>


@endsection