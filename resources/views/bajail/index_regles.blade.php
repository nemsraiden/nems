@extends('layout/singlepage')

@section('title','Règles du Bajail')
@section('sub_pagename','BaJail')
@section('sub_pagename_link',url('/bajail'))
@section('sub_pagename_2','Admin')
@section('sub_pagename_link_2',url('/bajail/admin'))
@section('pagename','Règles')


@section('content')

    <h2>Serveur Bajail : Règles du serveur</h2>

    <div class="content-padding">

        <div class="row">

            <div class="col s12"><div class="login-passes"><h3>Le mode de jeux</h3></div></div>

            <br/><br/>

            <div class="col s8">
                <p>Le ba_jail est un mode basé sur le jeu vidéo en ligne Counter Strike Source (CSS) simulant la vie carcérale. L’équipe des terroristes étant les prisonniers, et les anti-terroristes les gardiens.
                    Pour l’emporter, l’objectif du terroriste est d’éliminer l’intégralité des gardiens quant aux gardiens (Cts) leurs buts est de maîtriser et surveiller les prisonniers durant un temps imparti, tout en veillant à leurs propres vies.</p>
                <br/>

            </div>
            <div class="col s4">
                {!! HTML::image('images/ba_jail_electric_razor_v6.jpg','Bajail',['class' => 'responsive-img circle', 'width' => '150px']) !!}
                <br/><br/>
            </div>

            <div class="col s12">
                <strong>Ce mode de jeu requiert un fair-play total entre les équipes, surtout de la part des gardes. Tout manquement aux règles qui suivent entraînera des sanctions</strong>
                <br/><br/>
                <div class="info-message">
                        <p>Raiden, même s'il ne respecte pas les règles, a toujours raison. <br/>
                            Quand Raiden a tort, faite lui croire qu'il a raison ! </p>
                </div>
                <br/>
            </div>



            <div class="col s12"><div class="login-passes"><h3 >Quelques termes techniques</h3></div></div>

            <br/><br/>

            <div class="col s12">
                <ul class="profile-info style2">
                    <li>
                        <span class="first">Les Zones Terro</span>
                        <span class="last">Ce sont des zones adapté aux terroristes, par exemple la piscine (dans l'eau), le terrain de foot, le sauna, l'intérieur de la cage et l'isoloir.</span>
                    </li>
                    <li>
                        <span class="first">Les Zones CT</span>
                        <span class="last">Ce sont des zones adaptées au terroriste pour leur permettre de contrôler les déplacements des terro. Exemple : hauteur, toit des jail, armurerie.</span>
                    </li>
                    <li>
                        <span class="first">Les zones neutres</span>
                        <span class="last">Zones d'évolution des joueurs, sans restrictions envers les 2 camps. Les échelles, les chemins et le sol comptent comme des zones neutres..</span>
                    </li>
                    <li>
                        <span class="first">Le mode alerte</span>
                        <span class="last"> C’est un mode activable par le capitaine des antiterroristes permettant aux gardiens de traquer plus facilement les prisonniers évadés. Durant le mode alerte, les zones Terros n'existe plus. Les gardiens peuvent donc se balader où ils veulent (même dans les conduits)</span>
                    </li>
                    <li>
                        <span class="first">Le dernier CT</span>
                        <span class="last">Le dernier CT (ou DCT) est autorisé qu'en présence du plugin l'annonçant (son & message). Il s’active parfois lorsque le gardien se retrouve tout seul. Quand le DCT est actif, le gardien peut aller où il veut et tuer qui il veut. Le DCT possède quelques bonus (vitesse, vie). Si le dernier prisonnier lance sa DV, le CT doit la respecter. (Le DCT n’est pas obligé d’abattre tous les prisonniers à l’ordre et peut simplement continuer les ordres ou chercher les évadés (c’est plus fairplay)).</span>
                    </li>
                    <li>
                        <span class="first">La Dernière Volonté</span>
                        <span class="last">Ce sont des jeux qui se passent en fin de round, lorsqu'il ne reste qu'un prisonnier. Il a le droit de choisir de faire un jeu 1v1 contre chaque ct restant. S'il souhaite se rebeller, il doit le dire rapidement. Tant qu'il n'a pas choisi, il est considéré comme un terro classique qui doit donc être à l'ordre. Seule les DV du plugin sont acceptées.</span>
                    </li>
                    <li>
                        <span class="first">Les Conduits</span>
                        <span class="last">Les conduits sont des passages secrets cachés dans la carte permettant aux prisonniers de s’évader plus facilement. Tout prisonnier, du moment qu’il a utilisé un conduit peut être abattu sans somations (dans une certaines limite de temps).</span>
                    </li>
                </ul>
            </div>


            <div class="col s12"><div class="login-passes"><h3 >Règles des CT</h3></div></div>

            <br/><br/>

            <div class="col s12">

                <ul class="fa-ul regles">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Le capitaine est prioritaire pour donner l'ordre et il doit le faire rapidement. En cas d'absence, un autre CT peut donner un ordre. L'ordre donné doit être respecté par tous, CT compris.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les gardiens doivent être joué de préférence par des joueurs fairplay et connaissant un minimum les règles..</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Le capitaine est prioritaire pour donner l'ordre et il doit le faire rapidement. En cas d'absence, un autre CT peut donner un ordre.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>L'ordre donné doit être respecté par tous, CT compris.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les quartiers limités sont interdits sur le serveur. Ils peuvent être toléré une fois ou deux.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les quartiers libres sont autorisés. Il ne faut pas en abuser</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Ne pas abuser de l'ordre guncheck. Il est impératif d'expliquer aux prisonniers ce qu'on attend d'eux si on fait cet ordre (ce dernier peut ce faire debout). Sommations obligatoires lors des guncheck. Il ne doit pas s'éterniser (en 20-30 secondes ça doit être bon).</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les gardiens doivent s'entraider et surveiller l'ordre, il est donc interdit de blesser volontairement un coéquipier gardien.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Il est autorisé d'interdire aux terros de creuser un passage.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les sommations sont obligatoires sauf en cas de rebellion d'un ou plusieurs terros ou d'une tentative de fugue.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les sommations se font oralement plutôt qu'avec une arme, interdit à l'awp ou m4 ou ak etc,,,</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Tout gardien se trouvant dans une zone réservé aux prisonniers pourra être tué par eux sans se défendre ou être défendu par ses coéquipiers. Attention tout de même, un terroriste à le droit de tuer le ct sans aucun intervention de sa part, mais un Prisonnier n'a pas le droit de tiré avec une arme, même légère, sous peine de mort (de la part des gardiens hors des zones terroristes).</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Un gardien peut réprimer un prisonnier que s'il l'a vu agir. Il ne dispose que de 10 secondes pour le punir en cas de rébellion. Passé ce délai, le prisonnier n'est plus rebelle.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Des jeux peuvent être réalisés, mais les règles doivent être annoncées clairement et le gardien faisant le jeu désigne qui tue les perdants.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Interdit de retourner dans l'armurerie ou de garder la porte ou l'échelle même si elle reste zone CT</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Il est interdit de gêner une DV en cours sous peine de sanction.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Une DV peut être refusée que si le prisonnier a tiré alors qu'il pouvait prendre sa DV. C'est le prisonnier qui choisit dans le menu ce qu'il veut faire et les gardiens ne peuvent ni refuser ni choisir à sa place.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Le serveur est un serveur SOMMATION donc tout écart dans un ordre n’est pas puni par la mort (les sommations se font ORALEMENT de préférence ou au pistolet dans les pieds).</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les ordres / Jeux suivants sont interdits : Quartier restreint (Quartier libre toléré), Jeux du triangle, Ordres abusifs (ne pas bouger le viseur, allumer la lampe torche,…), Jeux ou ordres ayant pour but de faire des kills facile, Attente de DV à x min.</li>

                </ul>
                <br/>
            </div>

            <div class="col s12"><div class="login-passes"><h3 >Règles des terroristes</h3></div></div>

            <br/><br/>

            <div class="col s12">

                <ul class="fa-ul regles">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Ils doivent obéir à l'ordre donné par le capitaine ou un autre gardien sous peines de sommations.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les armes légères et les grenades sont autorisées. Cependant les CT peuvent demander de les jeter.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Tout détenu qui vise ou qui tire avec une arme légère ou lourde, sera rebelle, même s'il vise le ballon ou le plafond</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Tout détenu faisant un détour pour fuir ou prendre une arme pourra être abattu sans somations.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Tout détenu portant une arme lourde, même dans le dos, pourra être abattu.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Tout détenu s'étant échappé peut demandé à se rendre. Les CT peuvent refuser. Si un CT s'engage à le laisser se rendre, les autres ne peuvent pas le tuer sauf avis contraire du capitaine.
                    <li><i class="fa-li fa fa-angle-double-right"></i>Il est interdit de se cacher et de camper sur la map.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Tout terro blessant ou tuant un CT devant un autre CT pourra être abattu (sauf zone terro)</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Pour les DV, un détenu est toujours concidéré détenu tant qu'il n'a pas pris sa dv.</li>

                </ul>
                <br/>
            </div>

            <div class="col s12"><div class="login-passes"><h3 >Règles supplémentaires</h3></div></div>

            <br/><br/>

            <div class="col s12">

                <ul class="fa-ul regles">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Afin de donner des ordres clairs et compréhensibles, pas besoin de dire par exemple « dans la cage accroupie sans détour, sauter somma, courir somma … », le serveur étant sans détour un simple « dans la cage accroupie » suffit et merci de donner les ordres distinctement.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les gardiens ne sont pas obligés de surveiller l’ordre à tout moment, ils peuvent partir chercher les prisonniers évadés (à condition de ne pas abandonner son ordre trop longtemps).</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Le serveur est un serveur sommation donc les ordres du type « une fois arrivé sauna tous ce qui dépasse kill » sont interdits. (sauf en sous nombre de la part des gardiens)</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Les morts n’ont pas le droit de donner des informations afin d’aider une équipe ou l’autre mais peuvent annoncer si il y a un AFK ou un camper dans une jail.</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Il est obligatoire de respecter les autres joueurs, les admins et surtout les Nems : sanction possible</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i> Tous TAG, PSEUDOS, AVATAR à caractère pornographique, politiques, nazis, racistes, haineux ou ayant un lien connu avec le terrorisme est STRICTEMENT INTERDIT : le BAN du serveur sera rapidement fait.</li>

                </ul>
                <br/>
            </div>

            <div class="col s12"><div class="login-passes"><h3 >Les zones</h3></div></div>

            <div class="row">
                <div class="col s6">
                    {!! HTML::image('images/1407965165-legende.png','',array('class' => 'responsive-img')) !!}
                </div>

                <div class="col s6">
                    {!! HTML::image('images/1407965279-foot.png','',array('class' => 'responsive-img')) !!}
                </div>
            </div>

            <div class="row">
                <div class="col s6">
                    {!! HTML::image('images/1407965283-cuisine.png','',array('class' => 'responsive-img')) !!}
                </div>

                <div class="col s6">
                    {!! HTML::image('images/1407965678-salle-principale.png','',array('class' => 'responsive-img')) !!}
                </div>
            </div>

            <div class="row">
                <div class="col s6">
                    {!! HTML::image('images/1408352748-piscine.png','',array('class' => 'responsive-img')) !!}
                </div>

                <div class="col s6">
                    {!! HTML::image('images/1407965704-vestiaire.png','',array('class' => 'responsive-img')) !!}
                </div>
            </div>

            <div class="breaking-line"></div>

            <div class="col s6">
                <a class="defbutton" href="{{ url('/bajail') }}">
                    <i class="fa fa-info"></i>
                    En savoir plus sur le bajail
                </a>
            </div>

            <div class="col s6">
                <a class="defbutton" href="{{ url('/bajail/admin') }}">
                    <i class="fa fa-info"></i>
                    En savoir plus sur les admins
                </a>
            </div>




        </div>

    </div>


@endsection