@extends('layout/singlepage')

@section('title','Admin')
@section('sub_pagename','BaJail')
@section('sub_pagename_link',url('/bajail'))
@section('pagename','Admin')


@section('content')

    <h2>Serveur Bajail : Admins</h2>

    <div class="content-padding">

        <div class="row">

            <div class="col s12"><div class="login-passes"><h3 >Description</h3></div></div>

            <div class="col s3">
                {!! HTML::image('images/Policeman_Icon_256.png') !!}
            </div>

            <div class="col s9">
                <p>Un admin possède un menu !admin sur le bajail des Nemesis Spirit.</p>
                <p>Il peut utiliser des commandes grâce à ce menu et gérer le serveur et les joueurs présents.</p>

                <p>Un admin peut par exemple kicker, bannir, change de map, etc.</p>

                <p>Tout le monde peut devenir admin à condition de respecter la <a class="visible" href="{{ url('/bajail/charte') }}">charte des admins</a></p>
                <p>Pour devenir admin, il faut <a class="visible" href="{{ url('/compte') }}">créer un compte sur le site</a>, <a class="visible" href="{{ url('/jetons') }}">acheter des jetons</a> et réussir un petit test</p>

                <div class="col s6">
                    <a class="defbutton" href="{{ url('/bajail/regles') }}">
                        <i class="fa fa-arrow-right"></i>
                        Règles du Bajail
                    </a>
                </div>
                <div class="col s6">
                    <a class="defbutton" href="{{ url('/bajail/charte') }}">
                        <i class="fa fa-arrow-right"></i>
                        Charte des admins
                    </a>
                </div>

                <div class="col s12">
                    <a class="defbutton" href="{{ url('/bajail/admin/devenir-admin') }}" style="width: 100%; text-align:center;">
                        <i class="fa fa-wrench "></i>
                        Je veux devenir admin
                    </a>
                </div>


            </div>





            <div class="col s12"><div class="login-passes"><h3 >Tarifs</h3></div></div>

            <div class="col s6">

                <table class="descriptionServeur">
                    <tbody>
                    <tr>
                        <td class="gauche">Type de droit</td>
                        <td class="gauche">Prix</td>
                    </tr>
                    <tr>
                        <td class="gauche">Droit normaux</td>
                        <td>{{ $config->tarif_droit_normal }} jetons </td>
                    </tr>
                    <tr>
                        <td class="gauche">Droit fun</td>
                        <td>{{ $config->tarif_droit_fun }} jetons </td>
                    </tr>
                    </tbody>
                </table>

                <a class="defbutton" href="{{ url('/bajail/admin/devenir-admin') }}">
                    <i class="fa fa-wrench "></i>
                    Je veux devenir admin !
                </a> <br/>

            </div>

            <div class="col s6">
                <ul class="fa-ul">
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> La durée des droits est de 30 jours
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> Tu devras passer un petit QCM avant d'être admin
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> En cas d'abus, tes droits seront retirés
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> En devenant admin, tu acceptes la <a class="visible" href="{{ url('/bajail/charte') }}">charte des admins</a>
                    </li>
                </ul>
                <br/><br/>

            </div>

            <div class="col s12">
                <div class="clear-float do-the-split"></div>
                <div class="col s4">
                    {!! HTML::image('images/promo.png') !!}
                    <br/><br/>
                </div>

                <div class="col s2">
                    <br/>
                </div>
                <div class="col s6">
                    <h4>Profite de nos promos !</h4>
                    <p>Tu veux être <strong>ADMIN</strong> et <strong>VIP</strong> ?</p>
                    <p>Tu veux être admin et animateur ?</p>
                    <br/>
                    <a class="button" style="background-color: #DB6D1D;" href="{{ url('/promo') }}">Décrouvre toutes nos promos !</a>
                </div>
            </div>






            <div class="col s12"><div class="login-passes"><h3>Les avantages</h3></div></div>

            <div class="col s12">
                <h4>Droit normaux</h4>
                <p>Voici une liste non-exhaustive des droits que possède un admin normal.</p>
                <br/>
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Kick</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Ban à durée limitée</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Slay</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Option fun (freeze,drug,slap,beacon,fire...)
                    <li><i class="fa-li fa fa-angle-double-right"></i>Changement de map</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vote map</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vote question</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vote</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Mute</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Swap player</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Restart round</li>
                </ul>
                <br/>
                <h4>Droit fun</h4>
                <p>Un admin avec les droits fun possède les même droit qu'un admin normal avec d'autres droits en +.
                    Il a donc plus de possibilités dans son menu !admin</p>
                <p>Voici une liste non-exhaustive</p>
                <br/>
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-angle-double-right"></i>Noclip</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Téléportation</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Gravite</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vie</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Armure</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Argent</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Armes</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>God Mode</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Invisible</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Respawn</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Vitesse</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i>Immunité supérieur  (un admin normal ne pourra pas te slay par exemple)</li>
                </ul>
                <br/>


            </div>


            <div class="col s12"><div class="login-passes"><h3 >Liste des admins</h3></div></div>

            <div class="col s12">
                <table class="descriptionJoueur" >
                    <thead>
                    <tr>
                        <th></th>
                        <th>Pseudo</th>
                        <th>SteamID</th>
                        <th>Type de droit</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php $i=0; ?>
                    @foreach($users as $one_user)
                        <tr>
                            <td><a target="_blank" href="http://steamcommunity.com/profiles/{{ $users_link[$i] }}">{!! HTML::image('images/steam2.png','steam',['width' => '30']) !!}</a></td>
                            <td>{{$one_user->pseudo}}</td>
                            <td>{{$one_user->steamid}}</td>
                            <td>@if($users_infosAdmins[$i]->droit_fun) Fun @else Normal @endif</td>
                            <td>{{ $users_infosAdmins[$i]->date_debut }}</td>
                            <td>{{ $users_infosAdmins[$i]->date_fin }}</td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach

                    </tbody>
                </table>
            </div>



        </div>

    </div>


@endsection