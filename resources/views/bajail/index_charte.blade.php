@extends('layout/singlepage')

@section('title','Charte')
@section('sub_pagename','BaJail')
@section('sub_pagename_link',url('/bajail'))
@section('sub_pagename_2','Admin')
@section('sub_pagename_link_2',url('/bajail/admin'))
@section('pagename','Charte')


@section('content')

    <h2>Serveur Bajail : Charte des admins</h2>

    <div class="content-padding">

        <div class="row">

            <strong>Tous les admins des serveurs Nemesis Spirit s'engagent a respecter cette charte.</strong>
            <strong>En cas de non-respect, n'importe quelles sanctions pourra être appliquée</strong>

            <div class="clear-float do-the-split"></div>

            <h4>Informations</h4>
            <br/>
            <p>Un admin représente les serveurs et la team NemS.</p>
            <p>
                Il est tenu de faire régner une atmosphère agréable sur les serveurs pour une meilleure convivialité du serveur.
                <br>
                Il est donc souhaitable de ne pas montrer sa mauvaise humeur et de s'occuper des joueurs qui puissent dégrader la bonne ambiance.
            </p>
            <p>
                Il doit privilégier le mute, le slay et le kick au ban.
                <br>
                De plus, la longueur du ban doit être justifié.
                <br>
                Vous ne pouvez pas bannir un joueur sur une longue durée sans une raison valable. Soyez compréhensif, expliquer aux joueurs concernés leurs erreurs.
            </p>
            <br/>
            <h4>Règles</h4>
            <br/>
            <p>Il est strictement interdit d'utiliser ces droits pour son propre compte. C'est-à-dire, de maltraiter un joueur que l'on n'aime pas sans raison, où se donner des pouvoirs pour augmenter son score.</p>
            <p>De même pour les droits funs, si vous en venez à les utiliser, cela doit être pour l'amusement de l'ensembles des joueurs du serveur.</p>
            <p>
                Il est strictement interdit à un admin non-NemS d'utiliser ces droits contre un autre admin ou joueur NemS quoi qu'il ait fait.
                <br>
                Si vous pensez qu'il a mal agi, notre forum est grand ouvert pour toutes plaintes.
                <br>
                Mais sachez qu'un admin NemS reste au-dessus de vous, si vous faites des erreurs, il n'hésitera pas à vous remettre dans le droit chemin avec ces propres droits.
            </p>
            <p>Même en étant admin, vous restez un joueur. Ne vous prenez pas pour dieu grâce à vos pouvoirs. Dieu c'est Raiden et personne d'autre !</p>

            <br/>
            <h4>Sanction</h4>
            <br/>
            <p>
                Vos droits peuvent vous être retiré à tous moment, pouvant même être suivi d'un ban.
                <br>
                À ce titre, aucun remboursement  ne sera effectué !
            </p>

            <br/>
            <h4>Défense</h4>
            <br/>
            <p>
                Si vous êtes accusé d'abus admin sur notre forum, vous aurez le droit de vous y défendre.
                <br>
                En cas d'absence, cela pourrait se retourner contre vous (ban/ perte des droits sans avertissement).
            </p>


            <br>
            <h4>Droit fun/normal</h4>
            <br/>
            Pour des raisons techniques, il est possible que les droits bugs ou que votre achat ne fonctionne pas directement.
            <br>
            N'oubliez pas que je ne suis pas le support officiel des NemS, Merci de me laisser le temps de régler le problème après m'en avoir informé.
            <br>
            <br>
            <a class="defbutton" href="{{ url('/bajail/admin') }}">
                <i class="fa fa-wrench "></i>
                Je veux devenir admin !
            </a>


        </div>

    </div>


@endsection