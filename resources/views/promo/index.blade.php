@extends('layout/singlepage')

@section('title','Les promo NemS')
@section('pagename','Promo')


@section('content')

    <h2>Promo</h2>



    <div class="content-padding">

        <div class="row">

            <div class="col s9">
                <div class="promo">
                    <h3>Payer moin pour avoir plus !</h3>
                    <br/>
                    <p>Tu veux être Admin et VIP ?</p>
                    <p>Profite de nos promotions et de nos offres avantageuses !</p>
                    <p>Choisis entre notre <strong>Package Basic</strong> ou le <strong>Package Advantage</strong></p>
                </div>
            </div>
            <div class="col s3">
                {!! HTML::image('images/promotion.png', 'Promotion NemS', ['class' => 'img-responsive']) !!}
            </div>


        </div>

    </div>

    <br/><br/>

    <div class="signup-panel center-align">
        <div class="left">

            <h2>Package Basic</h2>

            <div class="content-padding">

                <div class="promo-info">
                    <b>Les droits "normal"</b>
                    <p>En choisissant l'offre "Package basic", tu bénéficiera des droits sur le serveur des NemS.
                        <br/>Tu bénéficieras des droits "normal" qui correspondent aux droits de base (kick, ban, slay, swap, etc)
                        <br/>Pour plus d'infos, consulte la <a href="{{url('/promo/acheter/basic')}}">section admin</a></p>

                    <div class="breaking-line"></div>
                    <br/>

                    <b>Vip "Medium"</b>

                    <p>Ce package te permet également d'être VIP <br/>
                    Tu pourras  profiter du menu !vip est de ces différents avantages.
                        <br/>Le "Package basic" te permet d'être VIP avec le grade "Medium".
                        <br/> Besoins de plus d'infos ? Découvre <a href="{{url('/bajail/vip')}}">notre offre VIP</a>
                    </p>

                    <br/>

                    <a class="button big-size" style="background-color: #DB6D1D;" href="{{url('/promo/acheter/basic')}}"><u>Package Basic</u> <br/> Profiter de cette offre pendant 1 mois</a>

                </div>

                <br/>

                <div class="login-passes">
                    <b>Le prix de ce package : {{ $config->package_basic }} jetons</b>
                </div>


            </div>

        </div>
        <div class="right center-align">

            <h2>package Advantage</h2>

            <div class="content-padding">

                <div class="promo-info">
                    <b>Les droits "fun"</b>
                    <p>En choisissant l'offre "Package advantage", tu bénéficiera des droits sur le serveur des NemS.
                        <br/>Tu bénéficieras des droits "fun" qui correspondent aux droits de base + des droits amusant (respawn, teleport, ...)
                        <br/>Pour plus d'infos, consulte la <a href="{{url('/bajail/admin')}}">section admin</a></p>

                    <div class="breaking-line"></div>
                    <br/>

                    <b>Vip "Ultimate"</b>

                    <p>Ce package te permet également d'être VIP <br/>
                        Tu pourras profiter à 100% du menu !vip est de ces différents avantages.
                        <br/>Le "Package advantage" te permet d'être VIP avec le grade "Ultimate", ce qui est le meilleur grade.
                        <br/> Plus d'infos sur <a href="{{url('/bajail/vip')}}">notre offre VIP</a>
                    </p>

                    <br/>

                    <a class="button big-size" style="background-color: #DB6D1D;" href="{{url('/promo/acheter/advantage')}}"><u>Package Advantage</u> <br/> Profiter de cette offre pendant 1 mois</a>

                </div>

                <br/>

                <div class="login-passes">
                    @if(Auth::check())
                        @if(Auth::user()->is_nems)
                            <b>Le prix de ce package : {{ ($config->package_advantage - 1) }} jetons</b>
                        @else
                            <b>Le prix de ce package : {{ ($config->package_advantage) }} jetons</b>
                        @endif
                    @endif
                </div>

            </div>

        </div>
    </div>





@endsection