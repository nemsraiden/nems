@extends('layout/singlepage')

@section('title','Les promo NemS')
@section('pagename','Promo')


@section('content')

    <h2>Promo</h2>

    <div class="content-padding">

        <div class="row">

            @if(Session::has('error'))

                <div class="info-message" style="background-color: #a24026;">
                    <a class="close-info" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                    <p>
                        {{ Session::get('error') }}
                    </p>
                </div>

            @endif

            @if(Session::has('success'))
                <div class="info-message" style="background-color: #75a226;">
                    <a class="close-info" href="#">
                        <i class="fa fa-times"></i>
                    </a>
                    <p>
                        {{ Session::get('success') }}
                    </p>
                </div>

            @endif

            @if($type == 'basic')

                Tu as choisis le <strong>Package Basic</strong> <br/>
                <br/>
                <u>Prix</u> : {{ $prix }} Jetons<br/>
                <u>Durée</u> : 1 mois <br/>
                <br/>
                <i>Cette promo ne prolonge pas ton VIP / Admin actuelle, elle le remplace <br/>
                En achetant cette promo, tu acceptes la <a href="{{ url('/bajail/charte') }}">charte des admins</a></i>
                <br/>
                <br/>
                <a class="button big-size" style="background-color: #519623;" href="{{ route('acheterPromo', array('id' => $type )) }}">Acheter cette promo</a>

            @elseif($type == 'advantage')

                Tu as choisis le <strong>Package Advantage</strong> <br/>
                <br/>
                <u>Prix</u> : {{ $prix }} Jetons<br/>
                <u>Durée</u> : 1 mois <br/>
                <br/>
                <i>Cette promo ne prolonge pas ton VIP / Admin actuelle <br/>
                    En achetant cette promo, tu acceptes la <a href="{{ url('/bajail/charte') }}">charte des admins</a></i>
                <br/>
                <br/>
                <a class="button big-size" style="background-color: #519623;" href="{{ route('acheterPromo', array('id' => $type )) }}">Acheter cette promo</a>


            @endif

        </div>
    </div>





@endsection