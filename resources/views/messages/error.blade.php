@if (Session::has('error'))

    <div class="row">
        <div class="col s12">
            <div class="info-message" style="background-color: #a24026;">
                <a class="close-info" href="#">
                    <i class="fa fa-times"></i>
                </a>
                <p>
                    {!! Session::get('error') !!}
                </p>
            </div>
        </div>
    </div>

@endif