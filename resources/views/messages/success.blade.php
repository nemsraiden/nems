@if (Session::has('success'))

    <div class="info-message" style="background-color: #75a226;">
        <a class="close-info" href="#">
            <i class="fa fa-times"></i>
        </a>
        <p>
            {{ Session::get('success') }}
        </p>
    </div>

@endif