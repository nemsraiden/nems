@if( count($errors) > 0)

    <div class="info-message" style="background-color: #a24026;">
        <a class="close-info" href="#">
            <i class="fa fa-times"></i>
        </a>
        <p>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
        </p>
    </div>

@endif