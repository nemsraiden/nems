@extends('layout/singlepage')

@section('title','Confirmation')
@section('sub_pagename','Bajail')
@section('sub_pagename_link',url('/bajail/'))
@section('sub_pagename_2','Devenir admin')
@section('sub_pagename_link_2',url('/bajail/admin/devenir-admin'))
@section('pagename','Devenir admin')

@section('content')

    <h2>Devenir admin : Confirmation</h2>

    <div class="content-padding">

        <div class="row">

            @if(Session::has('confirm'))

                <div class="info-message" style="background-color: #75a226;">
                    <p>
                        Félicitation, tu es désormais admin sur le serveur Bajail. <br/>
                        Tu pourras profiter de tes droits pendant 1 mois
                    </p>
                </div>

            @endif



        </div>

    </div>




@endsection