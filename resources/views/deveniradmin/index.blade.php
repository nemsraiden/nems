@extends('layout/singlepage')

@section('title','Devenir admin du bajail')
@section('pagename','Devenir admin')


@section('content')

    <h2>Devenir admin du serveur Bajail</h2>

    <div class="content-padding">

        <div class="row">

            <div class="col s12"><div class="login-passes"><h3 >Tarifs</h3></div></div>

            <div class="col s6">

                <table class="descriptionServeur">
                    <tbody>
                    <tr>
                        <td class="gauche">Type de droit</td>
                        <td class="gauche">Prix</td>
                    </tr>
                    <tr>
                        <td class="gauche">Droit normaux</td>
                        <td><span id="tarif_droit_normal">{{ $config->tarif_droit_normal }}</span> jetons </td>
                    </tr>
                    <tr>
                        <td class="gauche">Droit fun</td>
                        <td><span id="tarif_droit_fun">{{ $config->tarif_droit_fun }}</span> jetons </td>
                    </tr>
                    </tbody>
                </table> <br/>
                <a class="button" style="background-color: #DB6D1D;" href="{{ url('/promo') }}">Découvrir nos promos</a>

            </div>

            <div class="col s6">
                <ul class="fa-ul">
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> La durée des droits est de 30 jours
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> Tu devras passer un petit QCM avant d'être admin
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> En cas d'abus, tes droits seront retirés
                    </li>
                    <li>
                        <i class="fa-li fa fa-hand-o-right"></i> En devenant admin, tu acceptes la <a class="visible" href="{{ url('/bajail/charte') }}">charte des admins</a>
                    </li>
                </ul>
                <br/><br/>

            </div>


            <div class="col s12">
                <div class="login-passes">
                    @if($user->is_admin)
                        <h3 >Modifier mon status admin</h3>
                    @else
                        <h3 >Formulaire pour devenir admin</h3>
                    @endif

                </div>
            </div>
            @include('messages/error')
            {!! Form::open() !!}


            <div class="col s6">

                @if($user->is_admin && $user->type_droit == 'Fun')

                    Je veux prolonger mes droits de 1 mois <br/> <br/>

                    <p>
                        {!! Form::radio('type_droit','fun', true, ['id' => 'droit_fun']) !!}
                        <label for="droit_fun">Fun</label>
                    </p>

                @elseif($user->is_admin && $user->type_droit == 'Normal')
                    Si tu prends les droits fun, la date de fin de tes droits sera le {{ $dans1mois }} <br/> <br/>

                    <p>
                        {!! Form::radio('type_droit','fun', true, ['id' => 'droit_fun']) !!}
                        <label for="droit_fun">Fun</label>
                    </p>
                @else


                    <p>
                        {!! Form::radio('type_droit','normal', false, ['id' => 'droit_normaux']) !!}
                        <label for="droit_normaux">Normaux</label>
                    </p>
                    <p>
                        {!! Form::radio('type_droit','fun', true, ['id' => 'droit_fun']) !!}
                        <label for="droit_fun">Fun</label>
                    </p>



                @endif

                    <p>
                        {!! Form::checkbox('charte','charte',false, ['id' => 'charte']) !!}
                        <label for="charte">J'accepte la <a href="{{ url('bajail/charte') }}">charte des admins</a></label>
                    </p>





            </div>

            <div class="col s6">

                <div class="info-blocks">
                    <ul style="text-align: center">
                        <li>
                            <a class="info-block" href="{{ url('/jetons') }}">
                                <b>{{ $user->jetons }}</b>
                                <span>Jetons</span>
                            </a>
                            <a class="defbutton" href="{{ url('/jetons') }}">
                                <i class="fa fa-credit-card"></i>
                                Acheter des jetons
                            </a>
                        </li>
                        <li>
                    </ul>
                </div>


            </div>
            <div class="col s12"><div class="clear-float do-the-split"></div></div>

            <div class="col s12" style="text-align: center">
                Tarif: <strong><span id="tarif">{{ $config->tarif_droit_fun }}</span> jetons</strong><br/>
                <button id="submit_droit" type="submit" class="button greenbtn"  >Valider cet achat</button>
                <div id="nombre_jetons" style="display: none">{{$user->jetons}}</div>

            </div>

            {!! Form::close() !!}




        </div>

    </div>




@endsection