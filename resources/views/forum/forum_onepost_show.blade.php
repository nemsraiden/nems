<!-- BEGIN .forum-thread -->
<div class="forum-thread last">
    <!-- BEGIN .forum-post -->
    <div class="forum-post " id="post-" >
        <div class="user-block">
            <div class="avatar @if($post->user->isOnline) online @endif user-tooltip">
                @if($post->user->avatar == null)
                    {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive','width' => '39', 'height' => '39']) !!}
                @else
                    <img src="{{url('/')}}/uploads/{{$post->user->avatar}}" alt="{{$post->user->pseudo}} avatar" width="39" height="39"  />
                @endif

                <?php $info_user = $post->user ?>

                @include('compte/user_tooltip_info')


            </div>
            <div class="user-account">
                <div>
                    <a href="user-single.html" class="forum-user user-tooltip"><b>{{$post->user->pseudo}}</b></a> <br/>
                    @if($post->user->is_nems)<span class="admin-ribbon">NemS</span> @endif
                    @if($post->user->is_admin)<span class="admin-ribbon">Admin</span> @endif
                    @if($post->user->is_vip)<span class="admin-ribbon">VIP</span> @endif
                    <br/>

                </div>

            </div>

            <div class="clear-float"></div>
        </div>
        <div class="post-text-block">

            {!! $post->message !!}

        </div>
        <div class="post-meta-block">
            <div>
                <a href="#post-{{$post->id}}">#</a> <span class="post-date">{{$post->date}}</span>
            </div>
            <!--<div class="bottom">
                <a href="#" class="defbutton admin-color"><i class="fa fa-trash-o"></i></a>
                <a href="#" class="defbutton admin-color"><i class="fa fa-pencil"></i></a>
                <a href="#quick-reply" class="defbutton scroll"><i class="fa fa-comment-o"></i>Reply</a>
            </div>-->
        </div>
        <!-- END .forum-post -->
    </div>

    <!-- END .forum-thread -->
</div>