@extends('layout/doublepage')

@section('title','Forum de la team')
@section('pagename','Forum')


@section('content')


    <div class="forum-block">

        <h2><span>La team Nemesis Spirit</span></h2>

        @include('messages/error')

        <!-- BEGIN .forum-group -->
        <div class="forum-group">

            @foreach($forumCategory_nems as $forum)
                @include('forum/forum_category_show')
            @endforeach


            <!-- END .forum-group -->
        </div>

        <h2><span>Le Bajail</span></h2>

        <!-- BEGIN .forum-group -->
        <div class="forum-group">

            @foreach($forumCategory_bajail as $forum)
                @include('forum/forum_category_show')
                @endforeach


                        <!-- END .forum-group -->
        </div>

        <h2><span>Divers / Autres jeux</span></h2>

        <!-- BEGIN .forum-group -->
        <div class="forum-group">

            @foreach($forumCategory_divers as $forum)
                @include('forum/forum_category_show')
                @endforeach


                        <!-- END .forum-group -->
        </div>

    </div>

@endsection