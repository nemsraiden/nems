<!-- BEGIN .thread-link -->
<div class="thread-link @if(in_array($topic->id,$topics_view)) thread-read @else thread-unread @endif @if($topic->pinned) thread-sticky @endif @if($topic->lock) thread-locked @endif ">
    <a href="{{ route('forumTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]) }}">
        <i class="forum-icon strike-tooltip" title="Topic">
            <i class="fa fa-comments-o"></i>
        </i>

        <span>
            @if($topic->pinned)
                <i class="sticky">Pinned</i>
            @endif
            @if($topic->lock)
                    <i class="thr-closed">Closed</i>
            @endif

            {{ $topic->title }}

            @if($topic->lock)<i class="fa fa-lock"></i>@endif
        </span>
    </a>
    <div class="thread-author">
        <span class="f-user-link">
            <a href="{{ route('userProfil',['id' => $topic->users->id,'name' => str_slug($topic->users->pseudo)]) }}">
                <strong>{{ $topic->users->pseudo }}</strong>
            </a>
        </span>
    </div>
    <div class="thread-replies">
        <span>{{ $topic->nb_posts -1 }}</span>
    </div>
    <div class="thread-views">
        <span>{{ $topic->nb_views }}</span>
    </div>
    <div class="thread-last">
        @if($topic->last_answers_name ==  null)
            <span class="no-response">No answers</span>
        @else
            <span class="f-user-link"><strong>{{ $topic->last_answers_name }}</strong></span>
            <span class="t-date">{{ $topic->updated_at }}</span>
        @endif
    </div>
    <!-- END .thread-link -->
</div>