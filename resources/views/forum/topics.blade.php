@extends('layout/doublepage')

@section('title')
    Forum {{ $category->nom  }}
@endsection
@section('pagename')
    Forum {{ $category->nom  }}
@endsection

@section('sub_pagename','Forum')
@section('sub_pagename_link',url('/forum'))


@section('content')


    <div class="forum-block">

        <h2><span>Forum : {{ $category->nom }}</span></h2>



        <div class="content-padding">
            <div class="forum-description">
                @if(Auth::check())
                    @if($category->id != 1 || Auth::user()->is_nems)
                        <!--pour la section news, seuls les nems peuvent créer-->
                        <a href="{{ route('createTopic',[$category->id,str_slug($category->nom)]) }}" class="defbutton big"><i class="fa fa-comment-o"></i>Créer un sujet</a>
                    @endif
                @endif
            </div>
        </div>

        <div class="forum-threads-head">
            <strong class="thread-subject"><span>Sujet</span></strong>
            <strong class="thread-author">Auteur</strong>
            <strong class="thread-replies">Réponses</strong>
            <strong class="thread-views">Vues</strong>
            <strong class="thread-last">Dernière réponse</strong>
        </div>
        <!-- BEGIN .forum-threads -->
        <div class="forum-threads">

            @foreach($topics as $topic)

                @include('forum/forum_list_topics')

            @endforeach





            <!-- END .forum-threads -->
        </div>

        <div class="content-padding">
            <div class="forum-description">

                @if(Auth::check())
                    @if($category->id != 1 || Auth::user()->is_nems)
                        <!--pour la section news, seuls les modo peuvent créer-->
                        <a href="{{ route('createTopic',[$category->id,str_slug($category->nom)]) }}" class="defbutton big"><i class="fa fa-comment-o"></i>Créer un sujet</a>
                    @endif
                @endif
            </div>
        </div>

    </div>

@endsection