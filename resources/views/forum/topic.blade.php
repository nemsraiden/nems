@extends('layout/doublepage')

@section('title')
    {{ $topic->title  }}
@endsection
@section('pagename')
    {{ $topic->title  }}
@endsection

@section('sub_pagename','Forum')
@section('sub_pagename_link',url('/forum'))

@section('sub_pagename_2')
    Forum {{ $category->nom }}
@endsection
@section('sub_pagename_link_2')
    {{ route('forumCategory',[$category->id,str_slug($category->nom)]) }}
@endsection


@section('content')


    <div class="forum-block">

        <h2><span>@if($topic->lock)<i class="fa fa-lock"></i>@endif{{ $topic->title  }}</span></h2>

        @include('messages/success')

        <div class="content-padding">
            <div class="forum-description">
                <div class="topic-right right">
                    @if($topic->lock) <span><i class="fa fa-unlock-alt"></i>Sujet fermé</span> @endif
                    <span>{{ $topic->nb_posts }} Posts</span>
                </div>

                @if(!$topic->lock or $user->is_modo)
                    <a href="{{ route('replyTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]) }}" class="newdefbutton margin-right"><i class="fa fa-comment-o"></i>Répondre</a>
                @endif

                @if(Auth::user()->is_modo)
                    <a class="newdefbutton margin-right admin-color" href="{{ route('lockTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]) }}">
                        <i class="fa fa-unlock"></i>
                        @if($topic->lock == false) Fermé ce topic @endif
                        @if($topic->lock == true) Ouvrir ce topic @endif
                    </a>
                    <a class="newdefbutton margin-right admin-color" href="{{ route('pinnedTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]) }}">
                        <i class="fa fa-level-up"></i>
                        @if($topic->pinned == false) Pin ce topic @endif
                        @if($topic->pinned == true) Unpin topic @endif
                    </a>
                @endif


            </div>
        </div>

        <div id="forumPostsList">

        @foreach($posts as $key => $post)
            @include('forum/forum_post_show')
        @endforeach

        </div>





        <div class="content-padding quick-reply" id="quick-reply">
            <div class="forum-description">
                @if(!$topic->lock or $user->is_modo)
                    <a href="#quick-reply" class="newdefbutton"><i class="fa fa-comment-o"></i>Réponse rapide</a>
                    <a style="margin-left: 15px" href="{{ route('replyTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]) }}" class="newdefbutton"><i class="fa fa-comment-o"></i>Répondre</a>
                @endif
            </div>
            {!! Form::open(array('id' => 'quick_reply_form', 'url' => route('forumTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]))) !!}

            <div class="reply-box">
                <div class="reply-textarea">
                    <form action="" method="get">
                        <div class="respond-textarea">
                                <textarea id="ckeditor" name="comment-text" class="ckeditor"></textarea>
                        </div>
                        <div class="respond-submit">
                            <!--<a href="forum-create.html#preview-post" class="newdefbutton">Preview</a>-->
                            <a href="{{ route('replyTopic',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title)]) }}" class="newdefbutton">Full version</a>
                            <input id="quick_reply_post" type="submit" name="send" value="Envoyé ma réponse">
                        </div>
                    </form>
                </div>
            </div>

            {!! Form::close() !!}

        </div>

        <div class="content-padding">
            <div class="forum-description">
                <div class="topic-right right">
                    @if($topic->lock)<span><i class="fa fa-unlock-alt"></i>Sujet fermé</span>@endif
                        <span>{{ $topic->nb_posts }} Posts</span>
                </div>
            </div>
        </div>

    </div>


@endsection
