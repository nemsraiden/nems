@extends('layout/doublepage')

@section('title')
    {{ $topic->title  }}
@endsection
@section('pagename')
    {{ $topic->title  }}
@endsection

@section('sub_pagename','Forum')
@section('sub_pagename_link',url('/forum'))

@section('sub_pagename_2')
    Forum {{ $category->nom }}
@endsection
@section('sub_pagename_link_2')
    {{ route('forumCategory',[$category->id,str_slug($category->nom)]) }}
@endsection


@section('content')


    <div class="forum-block">

        <h2><span>@if($topic->lock)<i class="fa fa-lock"></i>@endif Reply : {{ $topic->title  }}</span></h2>

        <div class="content-padding quick-reply" id="quick-reply">
            {!! Form::open() !!}

            <div class="reply-box" style="display: block">
                <div class="reply-textarea">
                    <form action="" method="get">
                        <div class="respond-textarea">
                            <textarea id="ckeditor_full" name="comment-text" class="ckeditor">
                                @if($post_id_content != null)
                                    <blockquote>{!! $post_id_content !!}</blockquote>
                                @endif
                                <p></p>
                            </textarea>
                        </div>
                        <div class="respond-submit">
                            <!--<a href="forum-create.html#preview-post" class="newdefbutton">Preview</a>-->
                            <input id="reply_post" type="submit" name="send" value="Poster ma réponse">
                        </div>
                    </form>
                </div>
            </div>

            {!! Form::close() !!}

        </div>

        <div id="forumPostsList">

            @foreach($posts as $key => $post)
                @include('forum/forum_post_show')
            @endforeach

        </div>

    </div>

@endsection