<!-- BEGIN .forum-link -->
<div class="forum-link">
    <a href="{{ route('forumCategory',[$forum->id,str_slug($forum->nom)]) }}">
        @if($forum->is_read)
            <i class="forum-icon">
                <i class="fa {{$forum->icone}}"></i>
            </i>
        @else
            <i class="forum-icon new">
                <span>new</span>
                <i class="fa {{$forum->icone}}"></i>
            </i>
        @endif
        <strong>{{$forum->nom}}</strong>
        <span>{!! $forum->description !!}</span>
    </a>

    <div class="right">
        <div class="forum-numbers">
            <span>{{$forum->nb_topics}} Topics</span>
            <span>{{$forum->nb_posts}} Post</span>
        </div>
        <div class="forum-recent">
            @if($forum->last_topic_edit != null )
                <a href="{{ route('userProfil',['id' => $forum->users[0]->id,'name' => str_slug($forum->users[0]->pseudo)]) }}" class="avatar @if($forum->users[0]->isOnline) online @endif">
                    <span class="wrapimg">

                        @if($forum->users[0]->avatar == null)
                            {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive','width' => '45', 'height' => '45']) !!}
                        @else
                            <img width="45" height="45" src="{{url('/')}}/uploads/{{$forum->users[0]->avatar}}" alt="{{$forum->users[0]->pseudo}} avatar"/>
                        @endif
                    </span>
                </a>

                <span>
                    <a href="{{ route('forumTopic',[$forum->id,str_slug($forum->nom),$forum->forumtopics[0]->id,str_slug($forum->forumtopics[0]->title)]) }}">{{ $forum->forumtopics[0]->title }}</a>
                    <span class="f-date">{{ $forum->forumtopics[0]->updated_at }}</span>
                    <span class="f-user-link"><a href="{{ route('userProfil',['id' => $forum->users[0]->id,'name' => str_slug($forum->users[0]->pseudo)]) }}"><strong>{{ $forum->users[0]->pseudo }}</strong></a></span>
                </span>
            @else
                <span class="f-no-posts">No thread to show</span>
            @endif
        </div>
    </div>
    <!-- BEGIN .forum-link -->
</div>