<!-- BEGIN .forum-thread -->
<div class="forum-thread">
    <!-- BEGIN .forum-post -->
    <div class="forum-post" id="post-{{$post->id}}">
        <div class="user-block">
            <div class="avatar @if($post->user->isOnline) online @endif ">
                <a href="{{ route('userProfil',['id' => $post->user->id,'name' => str_slug($post->user->pseudo)]) }}">
                    @if($post->user->avatar == null)
                        {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive','width' => '39', 'height' => '39']) !!}
                    @else
                        <img src="{{url('/')}}/uploads/{{$post->user->avatar}}" alt="{{$post->user->pseudo}} avatar" width="39" height="39"  />
                    @endif
                </a>
            </div>
            <div class="user-account">
                <div>
                    <a href="{{ route('userProfil',['id' => $post->user->id,'name' => str_slug($post->user->pseudo)]) }}" class="forum-user">
                        <b>{{$post->user->pseudo}}</b>
                    </a> <br/>
                    @if($post->user->is_nems)<span class="admin-ribbon">NemS</span> @endif
                    @if($post->user->is_admin)<span class="admin-ribbon">Admin</span> @endif
                    @if($post->user->is_vip)<span class="admin-ribbon">VIP</span> @endif
                    <br/>

                </div>

            </div>

            <div class="clear-float"></div>
        </div>
        <div class="post-text-block">

            {!! $post->message !!}

            @if($post->is_edit)
                <div class="comment-edited">
                    <span>
                    <i class="fa fa-pencil"></i>
                    Modifié par
                        <b>{{$post->editor_name}}</b>
                        le
                    {{ $post->updated_at }}
                    </span>
                </div>
            @endif

        </div>
        <div class="post-meta-block">
            <div>
                <a href="#post-{{$post->id}}">#{{$key+1}}</a>
            </div>
            <div>
                <span class="post-date">{{$post->date}}</span>
            </div>
            <div class="bottom">
                @if(!$topic->lock or $user->is_modo)
                    <!--<a href="#" class="defbutton admin-color"><i class="fa fa-trash-o"></i></a>-->
                    @if($post->user->id == $user->id || $user->is_modo)
                        <a href="{{ route('EditPost',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title),$post->id]) }}" class="defbutton admin-color"><i class="fa fa-pencil"></i></a>
                    @endif
                    @if($post->user->id == $user->id || $user->is_modo)
                        <a class="defbutton admin-color" href="{{ route('removePost',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title),$post->id]) }} }}">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    @endif
                    <a href="{{ route('replyTopicWithQuote',[$category->id,str_slug($category->nom),$topic->id,str_slug($topic->title),$post->id]) }}" class="defbutton"><i class="fa fa-comment-o"></i>Répondre</a>

                @endif
            </div>
        </div>
        <!-- END .forum-post -->
    </div>
    <!-- END .forum-thread -->
</div>