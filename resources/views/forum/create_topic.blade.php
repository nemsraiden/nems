@extends('layout/doublepage')

@section('title')
    Créer un topic
@endsection
@section('pagename')
    Créer un topic
@endsection

@section('sub_pagename','Forum')
@section('sub_pagename_link',url('/forum'))

@section('sub_pagename_2')
    Forum {{ $category->nom }}
@endsection
@section('sub_pagename_link_2')
    {{ route('forumCategory',[$category->id,str_slug($category->nom)]) }}
@endsection


@section('content')


    <div class="forum-block">

        <h2><span>Créer un topic</span></h2>




        @if(count($errors->all()) > 0)
            <div class="row">
                <div class="col-sm-12">
                    <div class="info-message" style="background-color: #a24026;">
                        <a class="close-info" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                        <p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        @endif



        <div class="content-padding full-reply">

            {!! Form::open() !!}
            <div class="reply-box">

                <div class="reply-textarea">
                    <form action="" method="get">
                        <div class="respond-input">
                            <div class="da-customs">
                                <input type="checkbox" name="pinned" id="pinned" />
                                <label for="pinned">Topic épinglé</label>
                            </div>
                            <input id="title" name="title" type="text" value="{{old('title')}}" placeholder="Sujet du topic" />
                        </div>
                        <div class="respond-textarea">
                           <textarea id="ckeditor_full" name="comment-text" class="ckeditor">{{old('comment-text')}}</textarea>
                        </div>
                        <div class="respond-submit">
                            <!--<a href="forum-create.html#preview-post" class="newdefbutton">Preview</a>-->
                            <input id="reply_post" type="submit" name="send" value="Créer ce topic">
                        </div>
                    </form>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>

@endsection