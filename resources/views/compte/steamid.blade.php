@extends('layout/singlepage')

@section('title','Gérer mon steamid')
@section('sub_pagename','Mon Compte')
@section('sub_pagename_link',url('/compte'))
@section('pagename','Steamid')


@section('content')

    <h2>Gérer son steamid</h2>

    <div class="content-padding">

        <div class="row">

            @if(isset($success))
                <div class="info-message" style="background-color: #75a226;">
                    <p>{{ $success }}</p>
                </div>
            @endif

            @include('messages/error')

            @if(Session::has('need_steamid'))
                <div class="info-message">
                    <p>{{ Session::get('need_steamid') }}</p>
                </div>
            @endif




            {!! Form::open() !!}

                <p>Pour être Admin ou VIP, vous devez indiquer votre steamid.</p>
                <p>Celui-ci permet de vous identifier sur le serveur</p>

                @if($user->steamid != null)
                    <div class="cadreGreen">
                        <p><strong>SteamID actuel</strong> : {{ $user->steamid }}</p>
                        <p><strong>Steamid3 actuel</strong> : {{ $user->steamid3 }}</p>
                    </div>
                @endif

                <strong>Comment trouver son steamid ?</strong> <br/>
                <br/>

                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-angle-double-right"></i> Accédez à votre page profil Steam (Steam > Votre pseudo > Profil)</li>
                    <li><i class="fa-li fa fa-angle-double-right"></i> Copier L'url qui est indiqué sous le menu</li>
                </ul>
            <br/>

                {!! HTML::image('images/steamcommunity.jpg') !!}
            <br/><br/>
            <b>Convertir le lien :</b> <br/>

                    <div class="input-field">
                        {!! Form::text('steamcommunity', null, ['required' => 'required']) !!}
                        <label for="">Indiquer votre lien</label>
                    </div>

            <div id="checksteamid" class="button">Verifier mon lien</div>

            <div class="show-infos-player" style="display: none">
                <br/>
                <br/>
                <strong>Est-ce votre profil ?</strong> <br/>
                <br/>
                <div class="col s4">
                    <img src="" alt=""/>
                </div>
                <div class="col s8">
                    <strong>Pseudo : </strong><span class="pseudo"></span> <br/>
                    <strong>Steamid : </strong><span class="steamid"></span> <br/>
                    <strong>Steamid3 : </strong><span class="steamid3"></span> <br/>
                    <br/>
                    <button type="submit" style="background-color: #519623;" class="button">Je confirme qu'il s'agit de mon profil</button>
                </div>
            </div>
                <div class="player-not-found" style="display:none">
                    <br/><br/>
                    <div class="info-message" style="background-color: #a24026;">
                        <p>Désolé, nous ne parvenons pas a trouver votre compte. <br/>
                            Votre url doit ressembler a celle-ci : http://steamcommunity.com/profiles/76561197985522057
                            <br/><br/>
                        Ce site peut éventuellement vous aider : <a href="https://steamid.io/lookup" target="_blank">https://steamid.io/lookup</a></p>
                    </div>
                </div>


            {!! Form::close() !!}

        </div>

    </div>


@endsection