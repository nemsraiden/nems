@extends('layout/singlepage')

@section('title','Modifier mon avatar')
@section('sub_pagename','Mon Compte')
@section('sub_pagename_link',url('/compte'))
@section('pagename','Modifier mon avatar')


@section('content')

    <h2>Modifier mon avatar</h2>

    <div class="content-padding">

        <div class="row">

            @if(Session::has('modifierAvatar'))
                <div class="info-message" style="background-color: #75a226;">
                    <p>Votre avatar a été modifié avec succès</p>
                </div>
            @endif

            @include('messages/error')
            @include('messages/success')

            <a class="defbutton" href="{{url('/compte')}}">
                <i class="fa fa-arrow-left"></i>
                Revenir a la gestion de mon compte
            </a>


            <h3>Choisir un avatar existant</h3>
            <br/>
            <p>Si tu ne sais pas quel avatar mettre, tu peux choisir l'un des avatar suivant : </p>
            <br/>


            @for ($i = 1; $i <= 20; $i++)
                <a class="avatar-demo" href="{{url('/compte/avatar/use',$i)}}">
                    <img src="{{url('/')}}/uploads/tux{{$i}}.png" alt=""/>
                </a>
            @endfor

            <br/>
            <h3>Uploader mon avatar</h3>
            <br/>

            {!! Form::open(array('files' => true)) !!}

                <div class="btn-file">
                    <div class="btn-file-left"></div>
                    <div class="btn-file-btn">
                        <input id="images_file" type="file" multiple="true" name="file">
                        <div>
                            <i class="fa fa-file "></i>
                            Parcourir...
                        </div>

                    </div>
                </div>

                <br/><br/>
                <button style="background-color: #DB6D1D;" class="button" type="submit">Uploader mon image</button>

            {!! Form::close() !!}


        </div>

    </div>


@endsection