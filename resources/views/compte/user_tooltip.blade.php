<div class="user-tooltip-info">
    <a href="{{ route('userProfil',['name' => str_slug($info_user->pseudo), 'id' => $info_user->id]) }}" class="username">{{ $info_user->pseudo }}</a>
    <a href="{{ route('userProfil',['name' => str_slug($info_user->pseudo), 'id' => $info_user->id]) }}" class="avatar @if($info_user->isOnline) online @endif ">
                                            <span class="wrapimg" style="background-color:#fff;">
                                                @if($info_user->avatar == null)
                                                    {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive','width' => '90', 'height' => '90']) !!}
                                                @else
                                                    <img src="{{url('/')}}/uploads/{{$info_user->avatar}}" alt="{{$info_user->pseudo}} avatar" width="90" height="90"  />
                                                @endif
                                            </span>
    </a>
    <div class="info">
        <div>
            <font>Status:</font>
            @if($info_user->is_nems)<span class="admin-ribbon">NemS</span> @endif
            @if($info_user->is_admin)<span class="admin-ribbon">Admin</span> @endif
            @if($info_user->is_vip)<span class="admin-ribbon">VIP</span> @endif
        </div>
        <div class="online-user-jeux">
            <font>Jeux:</font>
            @foreach($info_user->jeux as $jeux)
                {!! HTML::image($jeux) !!}
            @endforeach
        </div>
    </div>
    <div class="clear-float"></div>
    <div class="bottom">
        <a href="#" class="com-control"><i class="fa fa-envelope"></i>Message privé</a>
        <a href="{{ route('userProfil',['name' => str_slug($info_user->pseudo), 'id' => $info_user->id]) }}" class="com-control"><i class="fa fa-file-text-o"></i>Voir le profil</a>
    </div>
</div>