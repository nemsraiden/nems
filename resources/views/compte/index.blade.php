@extends('layout/singlepage')

@section('title','Mon Compte')
@section('pagename','Mon Compte')


@section('content')

    <div class="signup-panel">
        <div class="left">
            <h2>Mon profil</h2>

            <div class="content-padding">
                <div class="center-align">
                    @if($user->avatar == null)
                        {!! HTML::image('/uploads/tux-counter.png','',['class' => 'img-responsive']) !!}
                    @else
                        <img src="{{url('/')}}/uploads/{{$user->avatar}}" alt="{{$user->pseudo}} avatar"/>
                    @endif
                    <br/><br/>
                </div>

                <ul class="profile-info">
                    <li>
                        <span class="first">Login</span>
                        <span class="last">{{$user->login}}</span>
                    </li>
                    <li>
                        <span class="first">Pseudo</span>
                        <span class="last">{{$user->pseudo}}</span>
                    </li>
                    <li>
                        <span class="first">Email</span>
                        <span class="last">{{$user->email}}</span>
                    </li>
                    <li>
                        <span class="first">Steamid</span>
                        <span class="last"><a target="_blank" href="http://steamcommunity.com/profiles/{{$user->steam_profil}}">{{$user->steamid}}</a></span>
                    </li>
                    <li>
                        <span class="first">Steamid3</span>
                        <span class="last">{{$user->steamid3}}</span>
                    </li>
                    <li>
                        <span class="first">Pays</span>
                        <span class="last">{{$user->pays}}</span>
                    </li>
                    <li>
                        <span class="first">Date de naissance</span>
                        <span class="last">{{$user->birthday}}</span>
                    </li>
                    <li>
                        <span class="first">Jeux</span>
                        <span class="last">
                            @foreach($user->jeux as $jeux)
                                {!! HTML::image($jeux) !!}
                            @endforeach
                        </span>
                    </li>
                </ul>

                <a class="newdefbutton" href="{{url('/compte/avatar')}}">
                    <i class="fa fa-picture-o"></i>
                    Changer mon avatar
                </a>

                <br/><br/>

                <a class="newdefbutton" href="{{url('/compte/modifier')}}">
                    <i class="fa fa-wrench"></i>
                    Modifier mon profil
                </a>

                <br/><br/>

                <a class="newdefbutton" href="{{url('/compte/steamid')}}">
                    <i class="fa fa-wrench"></i>
                    Modifier mon steamid
                </a>

                <br/><br/>

                <a class="newdefbutton" href="{{url('/auth/logout')}}">
                    <i class="fa fa-sign-out"></i>
                    Me déconnecter
                </a>

            </div>
        </div>
        <div class="right">
            <h2>Informations</h2>


            <div class="info-blocks">
                <ul style="margin-left: 50px">
                    <li>
                        <a class="info-block" href="{{ url('/jetons') }}">
                            <b>{{$user->jetons}}</b>
                            <span>Jetons</span>
                        </a>
                    </li>
                    <li>
                        <a class="info-block" href="{{ url('/coins') }}">
                            <b>{{$user->coins}}</b>
                            <span>Coins</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="clear-float do-the-split"></div>

            <h3>Status sur les serveurs :</h3>
            <br/>
            <h4>Admin :</h4>
            <br/>
            @if($user->is_admin)
                <ul class="fa-ul" style="margin-bottom: 30px">
                    <li><i class="fa-li fa fa-angle-double-right"></i> Tu est actuellement admin <br/></li>
                    <li><i class="fa-li fa fa-angle-double-right"></i> Type de droit : <strong>{{ $user->type_droit }}</strong> <br/></li>
                    <li><i class="fa-li fa fa-angle-double-right"></i> Date de fin : <strong>{{ $user->admin_fin }}</strong> <br/></li>
                </ul>
                <a class="defbutton" href="{{ url('/bajail/admin') }}">
                    <i class="fa fa-cogs"></i>
                    Gérer mes droits admin
                </a>

            @else
                Tu n'es pas admin sur notre serveur. <br/>
                <br/>
                <a class="defbutton" href="{{ url('/bajail/admin') }}">
                    <i class="fa fa-cogs"></i>
                    Devenir admin
                </a>
            @endif
            <br/>
            <h4>VIP :</h4>
            <br/>
            @if($user->is_vip)
                <ul class="fa-ul" style="margin-bottom: 30px">
                    <li><i class="fa-li fa fa-angle-double-right"></i> Tu est VIP sur le Bajail <br/></li>
                    <li><i class="fa-li fa fa-angle-double-right"></i> Grade actuel : <strong>{{ $user->vip_grade }}</strong> <br/></li>
                    <li><i class="fa-li fa fa-angle-double-right"></i> Date de fin : <strong>{{ $user->vip_fin_fr }}</strong> <br/></li>
                </ul>
                <a class="defbutton" href="{{ url('/bajail/vip') }}">
                    <i class="fa fa-cogs"></i>
                    Gérer mes droits vip
                </a>

            @else
                Tu n'es pas vip sur notre serveur. <br/>
                <br/>
                <a class="defbutton" href="{{ url('/bajail/vip') }}">
                    <i class="fa fa-cogs"></i>
                    Devenir vip
                </a>
            @endif




        </div>
    </div>

    <div class="clear-float"></div>

    <div class="breaking-line"></div>


@endsection