@extends('layout/singlepage')

@section('title','Modifier mon profil')
@section('sub_pagename','Mon Compte')
@section('sub_pagename_link',url('/compte'))
@section('pagename','Modifier mon profil')


@section('content')

    <h2>Modifier mon profil</h2>

    <div class="content-padding">

        <div class="row">

            @include('messages/errors')

            @include('messages/success')

            {!! Form::model($user) !!}

            <div class="input-field col s12">
                {!! Form::text('login', null ,['readonly']) !!}
                {!! Form::label('Login') !!}
            </div>

            <div class="input-field col s12">
                {!! Form::text('email') !!}
                {!! Form::label('Email') !!}
            </div>

            <div class="input-field col s6">
                {!! Form::password('password') !!}
                {!! Form::label('Password') !!}
            </div>

            <div class="input-field col s6">
                {!! Form::password('password_confirmation') !!}
                {!! Form::label('Password comfirmation') !!}
            </div>

            <div class="input-field col s12">
                {!! Form::text('pseudo') !!}
                {!! Form::label('Pseudo') !!}
            </div>

            <div class="input-field col s12">
                {!! Form::select('pays', array('France' => 'France', 'Belgique' => 'Belgique', 'Suisse' => 'Suisse', 'Autres' => 'Autres')) !!}
                {!! Form::label('Pays') !!}
            </div>

            <div class="input-field col s12">
                {!! Form::text('birthday', $user->birthday, ['id' => 'birthday']) !!}
                {!! Form::label('birthday') !!}
            </div>

            <div class="col s4">
                {!! Form::checkbox('jeux_css', $user->jeux_css, 0,  ['id' => 'jeux_css']) !!}
                {!! Form::label('jeux_css') !!}
            </div>

            <div class="col s4">
                {!! Form::checkbox('jeux_csgo', $user->jeux_csgo, 0,  ['id' => 'jeux_csgo']) !!}
                {!! Form::label('jeux_csgo') !!}
            </div>

            <div class="col s4">
                {!! Form::checkbox('jeux_lol', $user->jeux_lol, 0,  ['id' => 'jeux_lol']) !!}
                {!! Form::label('jeux_lol') !!}
            </div>

            <div class="input-field col s12">
                <div class="clear-float do-the-split"></div>
            </div>

            <div class="input-field col s12">
                <button class="btn waves-effect waves-light" type="submit" name="action">Modifier mon profil</button>
            </div>

            <div class="input-field col s12">
                <a class="btn waves-effect waves-light" href="{{url('/compte/steamid')}}" >Je veux modifier mon steamid</a>
            </div>



            {!! Form::close() !!}

        </div>

    </div>


@endsection