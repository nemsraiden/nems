@extends('layout/doublepage')

@section('title','Erreur 404')
@section('pagename','Erreur 404')

@section('content')


    <div class="big-message">
        <h3>404</h3>
        <div>
            <h4>Page introuvable</h4>
        </div>
        <p>
            Tu sembles un peu perdu. <br/>
            Peut-être, tu te serais trompé d'URL ou cette page n'existe plus
        </p>
        <div class="msg-menu">
            <a href="{{ url('/') }}">Homepage</a>
            <a href="{{ url('/forum') }}">Forum</a>
            <a href="{{ url('/compte') }}">Mon compte</a>
            <a href="{{ url('/bajail') }}">Bajail</a>
            <a href="{{ url('/tchat') }}">Serveur vocal</a>
            <a href="{{ url('/nems') }}">Les NemS</a>
        </div>
    </div>


@endsection