<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le password doit faire minimum 6 caractères et doit correspondre.',
    'reset' => 'Votre password a été modifiée!',
    'sent' => 'Un email vous a été envoyé!',
    'token' => 'Ce token est invalide.',
    'user' => "Aucun utilisateur trouvé avec cet email.",

];
