<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class serversAdmins extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getDateDebutAttribute($value){
        $date = new Carbon($value);
        return $date->format('d/m/y');
    }
    public function getDateFinAttribute($value){
        $date = new Carbon($value);
        return $date->format('d/m/y');
    }
}
