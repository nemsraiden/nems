<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
    public function users()
    {
        return $this->hasMany('App\User','id','last_user_id');
    }
    public function ForumTopics()
    {
        return $this->hasMany('App\ForumTopics','id','last_topic_edit');
    }
}
