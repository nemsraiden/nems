<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Historique extends Model
{
    protected $fillable = array('type', 'users_id');

    public function users()
    {
        return $this->hasOne('App\User','id','users_id');
    }

    public function getCreatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->format('d/m/Y');
    }
}
