<?php

namespace App\Console;

use App\serversAdmins;
use App\User;
use DateTime;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {


            // retirer les vip
            User::where('is_vip',true)->where('vip_fin','<',new DateTime('today'))->update(['is_vip' => false,'vip_credit' => 0]);


            // retirer les admins
            $users = User::where('is_admin',true)->get();

            foreach($users as $user){

                $serversAdmins = serversAdmins::where('user_id',$user->id)->where('date_fin','<',new DateTime('today'))->first();

                if(!empty($serversAdmins)){
                    $sb_user = DB::select('SELECT * FROM sb_admins WHERE authid = ?', [$user->steamid]);
                    $idOnSourceban = $sb_user[0]->aid;

                    DB::update("UPDATE sb_admins SET srv_group = '' WHERE authid = ?", [$user->steamid]);
                    DB::delete("DELETE FROM sb_admins_servers_groups WHERE admin_id = ? AND server_id = '4'", [$idOnSourceban]);
                    serversAdmins::where('user_id',$user->id)->update(['is_admin' => false,'droit_fun' => false]);
                    User::where('id',$user->id)->update(['is_admin' => false]);
                }

            }

            // on regarde qui est admin sur sourceban pour vérifier si il est bien admin sur le site
            $sb_admins = DB::select("SELECT * FROM sb_admins WHERE srv_group != ''");
            foreach($sb_admins as $admin){
                $steamid = $admin->authid;
                $user = User::where('steamid',$steamid)->where('is_admin',false)->first();
                if(!empty($user)){
                    DB::delete("DELETE FROM sb_admins_servers_groups WHERE admin_id = ? AND server_id = '4'", [$admin->aid]);
                    DB::update("UPDATE sb_admins SET srv_group = '' WHERE aid = ?", [$admin->aid]);
                    serversAdmins::where('user_id',$user->id)->update(['is_admin' => false,'droit_fun' => false]);
                }
            }


        })->daily();
    }
}
