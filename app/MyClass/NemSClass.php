<?php

namespace App\MyClass;

trait NemSClass{

    static function steamid_to_url($steamid){
        if($steamid != null){
            $split = explode(":", $steamid); // STEAM_?:?:??????? format

            $y = $split[1];
            $z = $split[2];

            $community =  bcadd(bcadd(bcmul($z, '2'), '76561197960265728'), $y);
            $community = explode(".",$community);

            return $community[0];
        }
        return '';

    }
}