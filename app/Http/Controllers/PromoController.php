<?php

namespace App\Http\Controllers;

use App\Config;
use App\Historique;
use App\serversAdmins;
use App\usersAction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PromoController extends Controller
{
    public function index(){

        return view('promo/index');
    }

    public function acheter($type){

        $config = Config::get()->first();

        if($type == 'basic'){
            $prix = $config->package_basic;
        }
        if($type == 'advantage'){
            $prix = $config->package_advantage;
            $user = Auth::user();
            if($user->is_nems) $prix -= 1;
        }


        return view('promo/acheter',compact('type','prix'));

    }

    public function valider($type){
        $config = Config::get()->first();

        if($type == 'basic'){
            $prix = $config->package_basic;
        }
        if($type == 'advantage'){
            $prix = $config->package_advantage;
        }
        $user = Auth::user();
        if($user->is_nems) $prix -= 1;

        if($user->jetons < $prix){

            Session::flash('error',"Tu n'as pas assez de jetons pour effectuer cet achat. Il faut que tu achètes des jetons");
            return redirect('promo/acheter/'.$type);
        }
        else{
            if($user->bajail_test_etat != "reussi"){
                return redirect('/bajail/regles/test')->with('test_before_admin',true);
            }
            if($user->steamid == '' || $user->steamid == null){
                Session::flash('need_steamid','Tu dois posseder un steamid pour devenir Admin');

                return redirect('/compte/steamid');
            }



            $user->is_vip = true;
            if($type == 'basic'){
                $user->vip_credit = $config->vip_medium_credits;
                $user->vip_grade = 'medium';
            }
            else if($type == 'advantage'){
                $user->vip_credit = $config->vip_ultimate_credits;
                $user->vip_grade = 'ultimate';
            }

            $user->vip_firsttime = false;
            $user->vip_debut = Carbon::now();
            $user->vip_fin = Carbon::now()->addMonth(1);
            $user->jetons -= $prix;
            $user->is_admin = true;
            $user->save();


            $serversAdmins = $user->serversAdmins->first();
            if(empty($serversAdmins)){
                $serversAdmins = new serversAdmins();
                $serversAdmins->user_id = $user->id;
            }

            $serversAdmins->is_admin = true;
            if($type == 'basic'){
                $serversAdmins->droit_fun = false;
            }
            else if($type == 'advantage'){
                $serversAdmins->droit_fun = true;
            }
            $date_debut = Carbon::now();
            $serversAdmins->date_debut = $date_debut;
            $date_fin = Carbon::now()->addMonth(1);
            $serversAdmins->date_fin = $date_fin;

            $serversAdmins->save();


            $usersAction = new usersAction();
            $usersAction->users_id = $user->id;
            $usersAction->type = 'Promo';
            $usersAction->intitule = 'Promo '.$type;
            $usersAction->description = 'Met Admin et VIP grâce a la promo';
            $usersAction->montant = $prix;
            $usersAction->save();


            Historique::create([
                'type' => 'admin',
                'users_id' => Auth::user()->id,

            ]);
            Historique::create([
                'type' => 'vip',
                'users_id' => Auth::user()->id,

            ]);


            // mettre admin sur le sourceban
            // on regarde si on est admin.
            $sb_user = DB::select('SELECT * FROM sb_admins WHERE authid = ?', [Auth::user()->steamid]);


            if(empty($sb_user)){

                DB::insert("INSERT INTO sb_admins VALUES ('', ?, ?, 'eb52345055a591103866c8d59e4cfc7cdbd11855', '-1', ?, '0', '0', '0', '', '', '', NULL)",
                    [Auth::user()->pseudo,Auth::user()->steamid,Auth::user()->email]);

                $idOnSourceban = DB::connection() -> getPdo() -> lastInsertId();
            }
            else{
                $idOnSourceban = $sb_user[0]->aid;
            }
            // on regarde si il est deja admin
            $sb_admin = DB::select("SELECT * FROM sb_admins_servers_groups WHERE admin_id = ? AND server_id = '4'", [$idOnSourceban]);

            if($type == 'basic'){
                $group_id = 2; $srv_group = 'admin2';
            }
            else if($type == 'advantage'){
                $group_id = 1; $srv_group = 'admin1';
            }
            // on mets admin pour le bon serveur
            if(empty($sb_admin)){
                DB::insert("INSERT INTO sb_admins_servers_groups VALUES(?,?,'-1','4')",[$idOnSourceban,$group_id]);
            }
            else{
                DB::update("UPDATE sb_admins_servers_groups SET group_id = ? WHERE admin_id = ?", [$group_id,$idOnSourceban]);
            }
            // on précise que ce joueur est admin

            DB::update("UPDATE sb_admins SET srv_group = ? WHERE authid = ?", [$srv_group,Auth::user()->steamid]);


            Session::flash('success',"Félicitation, tu es désormais Admin et VIP sur notre serveur");
            return redirect('promo/acheter/'.$type);
        }

    }
}
