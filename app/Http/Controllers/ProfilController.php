<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfilController extends Controller
{
    public function index($id,$name){

        $profil = User::where('id',$id)->first();

        $profil_exist = true;

        if(!is_object($profil)){
            $profil_exist = false;
        }


        return view('profil/index',compact('profil','profil_exist'));

    }
    public function infoTooltip($id){

        $info_user = User::where('id',$id)->first();

        return view('compte/user_tooltip',compact('info_user'));
    }
}
