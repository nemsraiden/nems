<?php

namespace App\Http\Controllers\Auth;

use App\Historique;
use App\Http\Requests\Register;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
 * Create a new authentication controller instance.
 *
 * @return void
 */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * If not connected, redirect to login page
     *
     *
     */
    public function index()
    {
        return redirect('/auth/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'login' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|min:6|max:50',
            'pseudo' => 'required|max:255|unique:users',
            'g-recaptcha-response' => 'required|recaptcha',

        ],[
            'g-recaptcha-response.required' => 'Tu es un bot ? Merci de valider le captcha.',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        if(!isset($data['jeux_csgo'])) $data['jeux_csgo'] = '';
        if(!isset($data['jeux_css'])) $data['jeux_css'] = '';
        if(!isset($data['jeux_lol'])) $data['jeux_lol'] = '';

        $data_sql = substr($data['birthday'],6).'-'.substr($data['birthday'],3,2).'-'.substr($data['birthday'],0,2);


        $user = User::create([
            'login' => $data['login'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'pseudo' => $data['pseudo'],
            'pays' => $data['pays'],
            'jeux_csgo' => $data['jeux_csgo'],
            'jeux_css' => $data['jeux_css'],
            'jeux_lol' => $data['jeux_lol'],
            'birthday' => $data_sql

        ]);

        Historique::create([
            'type' => 'inscription',
            'users_id' => $user->id,

        ]);

        return $user;
    }

    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'login';
    }

    protected function getFailedLoginMessage()
    {
        return 'Le login et le password ne correspondent pas';
    }
}
