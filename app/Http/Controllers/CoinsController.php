<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CoinsController extends Controller
{
    public function index(){


        return view('coins/index');
    }


    public function post(){

        if(Auth::check()){
            $user = Auth::user();

            if($user->jetons < 1){
                Session::flash('error','Il faut au minimum 1 jeton pour acheter des coins');
                return redirect('/coins');
            }else{
                $user->jetons -= 1;
                $user->coins += 500;

                $user->save();

                Session::flash('success','Tu as gagné 500 coins');
                return redirect('/coins');
            }
        }
        else{
            return redirect('/auth/login');
        }



    }
}
