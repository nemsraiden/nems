<?php

namespace App\Http\Controllers;

use App\testRegles;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\MyClass\NemSClass;

class BajailController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bajail/index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_admin()
    {
        $users = User::where('is_admin',1)->get();

        $users_infosAdmins = [];
        $users_link = [];
        foreach($users as $user_){
            $users_link[] = NemSClass::steamid_to_url($user_->steamid);
            $users_infosAdmins[] = $user_->serversAdmins->first();
        }
        return view('bajail/index_admin',compact('users','users_infosAdmins','users_link'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_charte()
    {
        return view('bajail/index_charte');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_regles()
    {
        return view('bajail/index_regles');
    }


    /**
     * Display a listing of the resource.
     *
     * @param testRegles $listeRegles
     * @return \Illuminate\Http\Response
     */
    public function index_test(testRegles $listeRegles)
    {
        $questions = $listeRegles->all()->random(10);
        $bajail_test_etat = Auth::user()->bajail_test_etat;

        return view('bajail/index_test',compact('questions','bajail_test_etat'));
    }

    /**
     * verifie les résultats du test
     *
     * @param testRegles $listeRegles
     * @return \Illuminate\Http\Response
     */
    public function verifTest(Request $request, testRegles $listeRegles)
    {
        $questions = $listeRegles->all();
        $points = 0;
        $resultats = array();
        $reussi = false;
        $good_rep = 0;
        $bad_rep = 0;


        foreach($request->all() as $key => $value){
            // il s'agit d'une question
            if(substr($key,0,1) == 'q'){
                $id = substr($key,1);

                $resultat = array();
                $resultat['question'] = $questions[$id-1]->question;
                $resultat['reponsse'] = $questions[$id-1]->reponsse;

                if($value != 'ne_sais_pas'){
                    if($questions[$id-1]->reponsse == $value){
                        $points++;
                        $resultat['bien_repondu'] = 'oui';
                        $good_rep++;
                    }
                    else if($questions[$id-1]->reponsse != $value){
                        $points--;
                        $resultat['bien_repondu'] = 'non';
                        $bad_rep++;
                    }
                }
                else{
                    $resultat['bien_repondu'] = 'no_rep';
                }
                $resultats[] = $resultat;

            }
        }


        if($points >= 7){
            $reussi = true;
        }
        $user = Auth::user();
        if($reussi){
            $user->bajail_test_etat = 'reussi';
        }
        else{
            if($user->bajail_test_etat == null){
                $user->bajail_test_etat = 'rate_1';
            }
            else if($user->bajail_test_etat == 'rate_1'){
                $user->bajail_test_etat = 'rate_2';
            }
        }
        $user->bajail_test_bonne_reponsse = $good_rep;
        $user->bajail_test_mauvaise_reponsse = $bad_rep;

        $bajail_test_etat = $user->bajail_test_etat;

        $user->save();

        $questions = $listeRegles->all()->random(10);

        return view('bajail/index_test',compact('questions','resultats','reussi','points','bajail_test_etat'));

    }
}

