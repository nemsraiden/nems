<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NemsController extends Controller
{
    public function index(){
        return view('nems/index');
    }

    public function liste(){
        $users = User::where('is_nems',1)->get();

        return view('nems/liste',compact('users'));
    }
}
