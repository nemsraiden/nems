<?php

namespace App\Http\Controllers;

use App\ForumPosts;
use App\ForumTopics;
use App\Historique;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(){

        $news = ForumTopics::where('forum_categories_id',1)->orderBy('created_at', 'desc')->limit(5)->get();

        ($news->count() > 0) ? $news_first_post = ForumPosts::where('forum_topics_id',$news[0]->id)->orderBy('id','asc')->first() : $news_first_post = '';


        $last_topics = ForumTopics::orderBy('created_at', 'desc')->limit(5)->with('users')->with('forum_category')->get();

        $historiques = Historique::orderBy('id','desc')->limit(10)->with('users')->get();

        return view('index',compact('news','news_first_post','last_topics','historiques'));
    }
}
