<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\ImageManager;

class CompteController extends Controller
{
    public function __construct(){

        parent::__construct();

        $this->middleware('auth');
        $random = rand(1,10);

        View::share ( 'isCheck', Auth::check() );
        View::share ( 'random', $random );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('compte/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {


        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'password' => 'min:6|max:50|confirmed',
            'password_confirmation' => 'min:6|max:50',
            'pseudo' => 'required|max:255|unique:users,pseudo,'.$user->id
        ]);

        if ($validator->fails()) {
            return redirect('compte/modifier')
                ->withErrors($validator)
                ->withInput();
        }

        $user->email = $request->get('email');
        $user->pseudo = $request->get('pseudo');
        if($request->get('password') != null) $user->password = bcrypt($request->get('password'));
        $user->pays = $request->get('pays');

        if($request->exists('jeux_csgo')){
            $user->jeux_csgo = 'on';
        }
        if($request->exists('jeux_css')){
            $user->jeux_css = 'on';
        }
        if($request->exists('jeux_lol')){
            $user->jeux_lol = 'on';
        }


        $birthday = $request->get('birthday');
        $data_sql = substr($birthday,6).'-'.substr($birthday,3,2).'-'.substr($birthday,0,2);
        $user->birthday = $data_sql;

        $user->save();

        return redirect('compte/modifier')->with('success','Votre profil a été modifié');

    }


    /**
     * Show edit profil
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();


        return view('compte/modifier',compact('user'));
    }


    /**
     * Show edit avatar
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function avatar()
    {
        $user = Auth::user();
        return view('compte/avatar',compact('user'));
    }


    /**
     * Change avatar (choose by click on image)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeAvatar($id)
    {
        $avatar = 'tux'.$id.'.png';
        $public_path = public_path();

        $user = Auth::user();

        if(file_exists($public_path.'/uploads/'.$avatar)){
            $user->avatar = $avatar;
            $user->save();

            Session::flash('modifierAvatar', true);
            return redirect('/compte/avatar');
        }
    }


    /**
     * Change avatar (post)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postAvatar(Request $request)
    {

        if ($request->hasFile('file')) {
            $name = urlencode($request->file('file')->getClientOriginalName());
            $ext = strtolower($request->file('file')->getClientOriginalExtension());
            $size = $request->file('file')->getClientSize();

            $ext_autorise = ['png','jpg','jpeg'];

            $user = Auth::user();

            if(!in_array($ext,$ext_autorise)){
                Session::flash("error","Le format de l'image doit être png ou jpg");
                return view('compte/avatar',compact('user'));
            }
            else if($size > 1048576){
                Session::flash("error","La taille de l'image ne doit pas dépasser 1 MB");
                return view('compte/avatar',compact('user'));
            }
            else{
                $newname = date('Ymdhis').'_'.$user->id.'_'.$user->login.'.'.$ext;
                $thumb = date('Ymdhis').'_'.$user->id.'_'.$user->login.'_thumb.'.$ext;

                $request->file('file')->move('uploads/user_avatar/', $newname);

                $manager = new ImageManager();
                $manager->make('uploads/user_avatar/'.$newname)->fit(140, 140)->save('uploads/user_avatar/'.$thumb);

                $user->avatar = 'user_avatar/'.$thumb;
                $user->save();

                Session::flash("success","Votre avatar a été correctement uploadé");
                return view('compte/avatar',compact('user'));
            }


        }
    }

    public function steamid(){

        return view('compte/steamid');

    }

    public function steamidVerif(Request $request){

        if($request->ajax()) {

            $steamcommunity = $request->get('steamcommunity');
            $steamcommunity = str_replace(' ', '', $steamcommunity);

            $url = explode('/', $steamcommunity);
            $id = last(array_filter($url));



            if(strpos($steamcommunity,'/id/') !== false){
                $xml = simplexml_load_file('http://steamcommunity.com/id/' . $id . '/?xml=1');

                $steamid3 = $this->Steam64IDsToSteamVersion3((string) $xml->steamID64);
                $steamid = $this->IDfrom64((string) $xml->steamID64);

            }
            else{
                $xml = simplexml_load_file('http://steamcommunity.com/profiles/' . $id . '/?xml=1');
                $steamid3 = $this->Steam64IDsToSteamVersion3($id);
                $steamid = $this->IDfrom64($id);
            }



            if($xml->steamID == ''){
                $infos['reussis'] = false;
            }
            else{
                $infos['pseudo'] = (string) $xml->steamID;
                $infos['avatar'] = (string) $xml->avatarFull;
                $infos['steamid3'] = $steamid3;
                $infos['$steamid'] = $steamid;
                $infos['reussis'] = true;
            }



            return $infos;

        }

    }

    public function steamidPost(Request $request){

        $steamcommunity = $request->get('steamcommunity');
        $steamcommunity = str_replace(' ', '', $steamcommunity);

        $url = explode('/', $steamcommunity);
        $id = last(array_filter($url));

        if(strpos($steamcommunity,'/id/') !== false){
            $xml = simplexml_load_file('http://steamcommunity.com/id/' . $id . '/?xml=1');

            $id = (string) $xml->steamID64;

        }

        $steamid3 = $this->Steam64IDsToSteamVersion3($id);
        $steamid = $this->IDfrom64($id);

        // verifie si il est pris
        $user_alreadyUse = User::where('steamid',$steamid)->first();
        if(!empty($user_alreadyUse)){
            Session::flash("error","Ce steamid est déjà utilisé par une autre personne. <br/>Contactez-nous via notre forum pour plus d'informations");
            return view('compte/steamid');
        }

        $user = Auth::user();
        $user->steamid = $steamid;
        $user->steamid3 = $steamid3;

        $user->save();


        return view('compte/steamid')->with('success','Votre SteamID a été correctement modifié.');

    }

    function Steam64IDsToSteam32IDs($Steam64ID)
    {
        $offset = $Steam64ID - 76561197960265728;
        $id = ($offset/2);
        if($offset%2 != 0)
        {
            $Steam32ID = 'STEAM_0:1:' . bcsub($id, '0.5');
        }
        else
        {
            $Steam32ID = "STEAM_0:0:" . $id;
        }
        echo '<pre>';
        echo $Steam32ID;
    }

    function Steam64IDsToSteamVersion3($id)
    {
        if (strlen($id) === 17)
        {
            $converted = substr($id, 3) - 61197960265728;
        }
        else
        {
            $converted = '765'.($id + 61197960265728);
        }
        return (string) '[U:1:'.$converted.']';
    }
    function IDfrom64($steamId64) {
        $iServer = "1";
        if(bcmod($steamId64, "2") == "0") {
            $iServer = "0";
        }
        $steamId64 = bcsub($steamId64,$iServer);
        if(bccomp("76561197960265728",$steamId64) == -1) {
            $steamId64 = bcsub($steamId64,"76561197960265728");
        }
        $steamId64 = bcdiv($steamId64, "2");
        if (strpos($steamId64, ".")) {
            $steamId64=strstr($steamId64,'.', true);
        }
        return ("STEAM_0:" . $iServer . ":" . $steamId64);
    }








}
