<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class tchatController extends Controller
{
    public function index(){
        if(Auth::check()){
            $user = Auth::user();
            $login = $user->login;
        }
        else{
            $login = "";
        }

        return view('tchat/index',compact('login'));

    }
}
