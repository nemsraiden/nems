<?php

namespace App\Http\Controllers;

use App\Achat;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class JetonsController extends Controller
{
    public function index(){
        return view('jetons/index');
    }
    public function valide(){

        return view('jetons/valide');
    }
    public function validePaypal(){

        return view('jetons/valide_paypal');
    }
    public function erreur(){

        return view('jetons/erreur');
    }

    public function check(){
        Log::info('Allopass start');

    }
    public function checkPaypal(){
        Log::info('Paypal start');
        // lire le formulaire provenant du système PayPal et ajouter 'cmd'
        $req = 'cmd=_notify-validate';

        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }



        // renvoyer au système PayPal pour validation
        $header = '';
        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        $fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);

        $payment_status = $_POST['payment_status'];
        $payment_amount = $_POST['mc_gross'];
        $payment_currency = $_POST['mc_currency'];
        $txn_id = $_POST['txn_id'];
        $receiver_email = $_POST['receiver_email'];
        $id_user = $_POST['custom'];

        $error = 0;

        if (!$fp) {

        } else {
            fputs ($fp, $header . $req);

            while (!feof($fp)) {
                $res = fgets ($fp, 1024);

                if (strcmp ($res, "VERIFIED") == 0) {

                    // vérifier que payment_status a la valeur Completed
                    if ( $payment_status == "Completed") {
                        // vérifier que txn_id n'a pas été précédemment traité: Créez une fonction qui va interroger votre base de données

                        $txn_exist = Achat::where('paypal_txn_id',$txn_id)->first();

                        if($txn_exist == null){

                            // vérifier que receiver_email est votre adresse email PayPal principale

                            if ( "mort4l_raiden@hotmail.com" == $receiver_email) {
                                // vérifier que payment_amount et payment_currency sont corrects
                                // traiter le paiement

                                if($payment_currency == "EUR")
                                {
                                    // on inscrit la requete et augmente les jetons

                                    Achat::create([
                                        'users_id' => $id_user,
                                        'amount' => $payment_amount,
                                        'type' => 'paypal',
                                        'allopass_code' => '',
                                        'paypal_txn_id' => $txn_id
                                    ]);

                                    User::find($id_user)->increment('jetons',$payment_amount);


                                }
                            }
                            else {
                                $error = 1;
                                $error_message = "Mauvaise adresse Email";
                            }
                        }
                        else {
                            $error = 1;
                            $error_message = "ID de transaction déjà utilisé";
                            // ID de transaction déjà utilisé
                        }
                    }
                    else {
                        $error = 1;
                        $error_message = "Le payement à échoué";
                        // Statut de paiement: Echec
                    }

                }
                else if (strcmp ($res, "INVALID") == 0) {
                    $error = 1;
                    $error_message = "Transaction invalide";

                    // Transaction invalide
                }
            }
            fclose ($fp);
        }


        if($error == 1)
        {
            $sujet = 'NemS : Payement Paypal';

            $codehtml=
                '<html><body>
                Une erreur à été trouvée lors d\'un payement paypal :

                '.$error_message.';

                Vous pouvez réessayer ou contacter un administrateur.
                Membre : '.$id_user.'

                Cordiallement, le Staff
                </body></html>';

            mail("nooj_@hotmail.com",$sujet,$codehtml,"Content-Type: text/html; charset=\"iso-8859-1\"\r\n");

        }
    }



    public function validestarpass(){
        Log::info('startpass');
        // Déclaration des variables
        $ident=$idp=$ids=$idd=$codes=$code1=$code2=$code3=$code4=$code5=$datas='';
        $idp = 201480;
        // $ids n'est plus utilisé, mais il faut conserver la variable pour une question de compatibilité
        $idd = 351527;
        $ident=$idp.";".$ids.";".$idd;
        // On récupère le(s) code(s) sous la forme 'xxxxxxxx;xxxxxxxx'
        if(isset($_POST['code1'])) $code1 = $_POST['code1'];
        if(isset($_POST['code2'])) $code2 = ";".$_POST['code2'];
        if(isset($_POST['code3'])) $code3 = ";".$_POST['code3'];
        if(isset($_POST['code4'])) $code4 = ";".$_POST['code4'];
        if(isset($_POST['code5'])) $code5 = ";".$_POST['code5'];
        $codes=$code1.$code2.$code3.$code4.$code5;
        // On récupère le champ DATAS
        if(isset($_POST['DATAS'])) $datas = $_POST['DATAS'];
        // On encode les trois chaines en URL
        $ident=urlencode($ident);
        $codes=urlencode($codes);
        $datas=urlencode($datas);

        /* Envoi de la requête vers le serveur StarPass
        Dans la variable tab[0] on récupère la réponse du serveur
        Dans la variable tab[1] on récupère l'URL d'accès ou d'erreur suivant la réponse du serveur */
        $get_f=@file( "http://script.starpass.fr/check_php.php?ident=$ident&codes=$codes&DATAS=$datas" );
        if(!$get_f)
        {
            exit( "Votre serveur n'a pas accès au serveur de StarPass, merci de contacter votre hébergeur. " );
        }
        $tab = explode("|",$get_f[0]);

        if(!$tab[1]) $url = "http://script.starpass.fr/error.php";
        else $url = $tab[1];

        // dans $pays on a le pays de l'offre. exemple "fr"
        $pays = $tab[2];
        // dans $palier on a le palier de l'offre. exemple "Plus A"
        $palier = urldecode($tab[3]);
        // dans $id_palier on a l'identifiant de l'offre
        $id_palier = urldecode($tab[4]);
        // dans $type on a le type de l'offre. exemple "sms", "audiotel, "cb", etc.
        $type = urldecode($tab[5]);
        // vous pouvez à tout moment consulter la liste des paliers à l'adresse : http://script.starpass.fr/palier.php

        // Si $tab[0] ne répond pas "OUI" l'accès est refusé
        // On redirige sur l'URL d'erreur
        if( substr($tab[0],0,3) != "OUI" )
        {
            Log::info('Paypal startpass rep = non');
            header( "Location: $url" );
            exit;
        }
        else
        {
            Log::info('startpass rep = oui');
            /* Le serveur a répondu "OUI"

            // vous pouvez afficher les variables de cette façon :
            // echo "idd : $idd / codes : $codes / datas : $datas / pays : $pays / palier : $palier / id_palier : $id_palier / type : $type";
            */
            $allopass_code = Achat::where('allopass_code',$codes)->first();

            if($allopass_code == null || $codes == 'nemesiss') {

                Achat::create([
                    'users_id' => Auth::user()->id,
                    'amount' => 1,
                    'type' => 'allopass',
                    'allopass_code' => $codes,
                    'paypal_txn_id' => $type
                ]);


                User::find(Auth::user()->id)->increment('jetons', 1);

                Log::info('startpass code = '.$codes);
            }
            else{
                Log::info('startpass code exist');
            }
        }

        return view('jetons/valide');
    }
}
