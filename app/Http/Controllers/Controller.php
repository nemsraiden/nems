<?php

namespace App\Http\Controllers;

use App\Config;
use App\ForumPosts;
use App\Online;
use App\User;
use Carbon\Carbon;
use donatj\SimpleCalendar;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct() {
        $random = rand(1,10);

        View::share ( 'isCheck', Auth::check() );
        View::share ( 'random', $random );

        $user = Auth::user();

        View::share('user',$user);

        $config = Config::get()->first();

        View::share('config',$config);

        Online::updateCurrent();

        $registered = Online::registered();
        $totalGuests  = Online::guests()->count();

        View::share('online_guests',$totalGuests);
        View::share('online_register',$registered->count());
        View::share('online_users',$registered);

        $this->makeCalendar();

        $sidebar_last_posts = ForumPosts::orderBy('id','desc')->limit(5)->with('user')->with('forum_topics')->get();

        View::share('sidebar_last_posts',$sidebar_last_posts);
    }

    public function makeCalendar(){

        $users = User::where('birthday','LIKE','%-'.date("m").'-%')->get();
        require_once(base_path().'/vendor/donatj/simplecalendar/lib/donatj/SimpleCalendar.php');
        $Calendar = new SimpleCalendar();
        $Calendar->setStartOfWeek('Monday');

        foreach($users as $user) {
            $date = new Carbon($user->birthday_sql);
            $Calendar->addDailyHtml('Anniversaire de ' . $user->pseudo, date('Y').'-'.$date->format('m-d'));
        }

        View::share('calendar',$Calendar->show(false));
    }




}
