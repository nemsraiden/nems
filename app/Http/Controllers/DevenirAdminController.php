<?php

namespace App\Http\Controllers;

use App\Config;
use App\Historique;
use App\serversAdmins;
use App\usersAction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DevenirAdminController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
        parent::__construct();

    }


    /** return view to become admin
     *
     */
    public function index(Config $configs){

        $user = Auth::user();
        $config = $configs->get()->first();

        $dans1mois = $date_fin = Carbon::now()->addMonth(1)->format('d/m/Y');

        return view('deveniradmin/index',compact('user','config','dans1mois'));
    }


    /** verifie que tout est en ordre pour le paiement
     * @param Request $request
     */
    public function post(Request $request, Config $configs){

        $type_droit = $request->get('type_droit');

        $user = Auth::user();

        if($user->steamid == '' || $user->steamid == null){
            Session::flash('need_steamid','Tu dois posseder un steamid pour devenir Admin');

            return redirect('/compte/steamid');
        }


        if($type_droit == "normal"){
            $tarif = $configs->get()->first()->tarif_droit_normal;
        }
        else if($type_droit == "fun"){
            $tarif = $configs->get()->first()->tarif_droit_fun;
        }


        if($user->jetons < $tarif){
            return back()->with('error', "Tu n'a pas assez de jetons");
        }
        else{

            if(!$request->exists('charte')){
                return back()->with('error', "Tu dois accepter la charte des admins");
            }
            else{
                if($user->bajail_test_etat != "reussi"){
                    return redirect('/bajail/regles/test')->with('test_before_admin',true);
                }
                else{
                    // retirer les jetons

                    $user->jetons -= $tarif;
                    $prolonge = false;
                    if($user->is_admin && $user->type_droit == 'Fun') $prolonge = true;
                    $user->is_admin = true;
                    $user->save();


                    // mettre admin
                    $serversAdmins = $user->serversAdmins->first();
                    if(empty($serversAdmins)){
                        $serversAdmins = new serversAdmins;
                        $serversAdmins->user_id = $user->id;
                    }

                    $serversAdmins->is_admin = true;
                    if($type_droit == 'normal'){
                        $serversAdmins->droit_fun = false;
                    }
                    else if($type_droit == 'fun'){
                        $serversAdmins->droit_fun = true;
                    }
                    $date_debut = Carbon::now();
                    $serversAdmins->date_debut = $date_debut;
                    if($prolonge){
                        $datetmp = new Carbon($user->vip_fin);
                        $date_fin = $datetmp->addMonth(1);
                    }else{
                        $date_fin = Carbon::now()->addMonth(1);
                    }

                    $serversAdmins->date_fin = $date_fin;

                    $serversAdmins->save();

                    // mettre a jour l'historique
                    $usersAction = new usersAction();
                    $usersAction->users_id = $user->id;
                    $usersAction->type = 'Autoadmin';
                    if($prolonge) {
                        $usersAction->intitule = 'Prolonge admin';
                    }
                    else{
                        $usersAction->intitule = 'Mettre admin';
                    }
                    $usersAction->description = 'Serveur Bajail, droit = '.$type_droit.', date debut = '.$date_debut.', date fin = '.$date_fin;
                    $usersAction->montant = $tarif;
                    $usersAction->save();

                    Historique::create([
                        'type' => 'admin',
                        'users_id' => Auth::user()->id,

                    ]);

                    // mettre admin sur le sourceban
                    // on regarde si on est admin.
                    $sb_user = DB::select('SELECT * FROM sb_admins WHERE authid = ?', [Auth::user()->steamid]);


                    if(empty($sb_user)){

                        DB::insert("INSERT INTO sb_admins VALUES ('', ?, ?, 'eb52345055a591103866c8d59e4cfc7cdbd11855', '-1', ?, '0', '0', '0', '', '', '', NULL)",
                        [Auth::user()->pseudo,Auth::user()->steamid,Auth::user()->email]);

                        $idOnSourceban = DB::connection() -> getPdo() -> lastInsertId();
                    }
                    else{
                        $idOnSourceban = $sb_user['aid'];
                    }

                    // on regarde si il est deja admin
                    $sb_admin = DB::select("SELECT * FROM sb_admins_servers_groups WHERE admin_id = ? AND server_id = '4'", [$idOnSourceban]);

                    if($type_droit == 'normal'){
                        $group_id = 2; $srv_group = 'admin2';
                    }
                    else if($type_droit == 'fun'){
                        $group_id = 1; $srv_group = 'admin1';
                    }
                    // on mets admin pour le bon serveur
                    if(empty($sb_admin)){
                        DB::insert("INSERT INTO sb_admins_servers_groups VALUES(?,?,'-1','4')",[$idOnSourceban,$group_id]);
                    }
                    else{
                        DB::update("UPDATE sb_admins_servers_groups SET group_id = ? WHERE admin_id = ?", [$group_id,$idOnSourceban]);
                    }
                    // on précise que ce joueur est admin

                    DB::update("UPDATE sb_admins SET srv_group = ? WHERE authid = ?", [$srv_group,Auth::user()->steamid]);


                    return redirect('/bajail/admin/devenir-admin/confirmation')->with('confirm', 'confirm');

                }
            }
        }
    }


    /** confirm admin view
     */
    public function confirmation(){

        return view('deveniradmin/confirmation');
    }
}
