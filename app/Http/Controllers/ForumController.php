<?php

namespace App\Http\Controllers;

use App\ForumCategory;
use App\ForumPosts;
use App\ForumTopics;
use App\ForumTopicsView;
use App\Historique;
use App\Http\Requests\CreateTopicPostRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ForumController extends Controller
{
    //

    public function index(){

        $forumCategory_nems = ForumCategory::where('section','nems')->with('users')->with('ForumTopics')->get();
        $forumCategory_bajail = ForumCategory::where('section','bajail')->get();
        $forumCategory_divers = ForumCategory::where('section','divers')->get();

        foreach($forumCategory_nems as $category){
            $topics_view = ForumTopicsView::where('users_id',Auth::user()->id)->where('forum_category_id', $category->id)->select('id')->get();
            $topics_in_this_category = ForumTopics::where('forum_categories_id',$category->id)->get();
            if($topics_view->count() == $topics_in_this_category->count() || $topics_in_this_category->count() == 0) $category->is_read = true;
            else $category->is_read = false;
        }
        foreach($forumCategory_bajail as $category){
            $topics_view = ForumTopicsView::where('users_id',Auth::user()->id)->where('forum_category_id', $category->id)->select('id')->get();
            $topics_in_this_category = ForumTopics::where('forum_categories_id',$category->id)->get();
            if($topics_view->count() == $topics_in_this_category->count() || $topics_in_this_category->count() == 0) $category->is_read = true;
            else $category->is_read = false;
        }
        foreach($forumCategory_divers as $category){
            $topics_view = ForumTopicsView::where('users_id',Auth::user()->id)->where('forum_category_id', $category->id)->select('id')->get();
            $topics_in_this_category = ForumTopics::where('forum_categories_id',$category->id)->get();
            if($topics_view->count() == $topics_in_this_category->count() || $topics_in_this_category->count() == 0) $category->is_read = true;
            else $category->is_read = false;
        }



        return view('forum/index',compact('forumCategory_nems','forumCategory_bajail','forumCategory_divers'));
    }

    // liste des topics

    public function topicsList($id, $name){

        if($id == 8 && !Auth::user()->is_nems){
            // forum privé
            Session::flash('error', 'Cette section est réservée aux NemS');
            return redirect()->action('ForumController@index');
        }

        $category = ForumCategory::where('id',$id)->first();

        $topics = ForumTopics::where('forum_categories_id',$id)->with('users')->orderBy('pinned', 'desc')->orderBy('updated_at', 'desc')->get();

        $topics_view = ForumTopicsView::where('users_id',Auth::user()->id)->where('forum_category_id', $category->id)->select('forum_topics_id')->get()->toArray();

        $topics_view = array_map('current', $topics_view);


        return view('forum/topics',compact('category','topics','topics_view'));
    }

    // affichage de 1 topic

    public function topics($id, $name, $id_topic, $name_topic, ForumTopicsView $ForumTopicsView){

        if($id == 8 && !Auth::user()->is_nems){
            // forum privé
            Session::flash('error', 'Cette section est réservée aux NemS');
            return redirect()->action('ForumController@index');
        }

        $category = ForumCategory::where('id',$id)->first();
        $topic = ForumTopics::where('id',$id_topic)->first();
        $posts = ForumPosts::where('forum_topics_id',$id_topic)->orderBy('created_at','asc')->with('user')->get();


        $topic->nb_views +=1 ;
        $topic->save();

        $userId = Auth::user()->id;

        $ForumTopicsView->firstOrCreate(['users_id' => $userId,'forum_topics_id' => $id_topic, 'forum_category_id' => $category->id]);

        return view('forum/topic',compact('category','topic', 'posts'));
    }

    // quick reply

    public function replyTopicFast($id, $name, $id_topic, $name_topic, Request $request)
    {
        if ($request->ajax()) {

            $forumTopic = ForumTopics::where('id',$id_topic)->first();
            if($forumTopic->lock && !Auth::user()->is_modo) return;
            $forumTopic->nb_posts += 1;
            $forumTopic->last_answers_name = Auth::user()->pseudo;
            $forumTopic->updated_at = Carbon::now();
            $forumTopic->save();

            $post = new ForumPosts();
            $post->users_id = Auth::user()->id;
            $post->forum_topics_id = $id_topic;
            $post->message = $request->get('comment-text');

            $post->save();

            $this->updateCategoryPostsCount($id,Auth::user()->id,$forumTopic->id);



            $post = ForumPosts::where('id',$post->id)->with('user')->first();

            return view('forum/forum_onepost_show',compact('post'));
        }

    }

    // obtenir un post

    public function getOnePost($id){
        $id = 8;

        $post = ForumPosts::find($id)->get();
        return $post->created_at;

    }

    // affiche la page de réponse a un topic

    public function replyTopics($id, $name, $id_topic, $name_topic){

        if($id == 8 && !Auth::user()->is_nems){
            // forum privé
            Session::flash('error', 'Cette section est réservée aux NemS');
            return redirect()->action('ForumController@index');
        }

        $category = ForumCategory::where('id',$id)->first();
        $topic = ForumTopics::where('id',$id_topic)->first();
        $posts = ForumPosts::where('forum_topics_id',$id_topic)->with('user')->orderBy('id','desc')->get();

        $post_id_content = '';
        return view('forum/forum_post_reply',compact('category','topic', 'posts','post_id_content'));
    }

    // idem sauf que on répond directement a une personne

    public function replyTopicsWithQuote($id, $name, $id_topic, $name_topic, $post_id){

        if($id == 8 && !Auth::user()->is_nems){
            // forum privé
            Session::flash('error', 'Cette section est réservée aux NemS');
            return redirect()->action('ForumController@index');
        }

        $category = ForumCategory::where('id',$id)->first();
        $topic = ForumTopics::where('id',$id_topic)->first();
        $posts = ForumPosts::where('forum_topics_id',$id_topic)->with('user')->orderBy('id','desc')->get();

        $post_id_content = ForumPosts::where('id',$post_id)->first();
        $post_id_content = $post_id_content->message;

        return view('forum/forum_post_reply',compact('category','topic', 'posts','post_id_content'));
    }

    // répondre a un topic

    public function postReplyTopics($id, $name, $id_topic, $name_topic, Request $request){

        $forumTopic = ForumTopics::where('id',$id_topic)->first();
        if($forumTopic->lock && !Auth::user()->is_modo) return;
        $forumTopic->nb_posts += 1;
        $forumTopic->last_answers_name = Auth::user()->pseudo;
        $forumTopic->updated_at = Carbon::now();
        $forumTopic->save();

        $post = new ForumPosts();
        $post->users_id = Auth::user()->id;
        $post->forum_topics_id = $id_topic;
        $post->message = $request->get('comment-text');

        $post->save();


        $this->updateCategoryPostsCount($id,Auth::user()->id,$forumTopic->id);



        return redirect()->route('forumTopic', [$id,$name,$id_topic,$name_topic]);
    }

    // affiché la page d'edition d'un post

    public function editPost($id, $name, $id_topic, $name_topic, $post_id){

        if($id == 8 && !Auth::user()->is_nems){
            // forum privé
            Session::flash('error', 'Cette section est réservée aux NemS');
            return redirect()->action('ForumController@index');
        }

        $category = ForumCategory::where('id',$id)->first();
        $topic = ForumTopics::where('id',$id_topic)->first();


        $post = ForumPosts::where('id',$post_id)->first();
        if($post->users_id != Auth::user()->id  && !Auth::user()->is_modo){
            return back();
        }
        $post_id_content = $post->message;

        return view('forum/forum_post_edit',compact('category','topic','post_id_content'));
    }

    // envoyé l'edition d'un post

    public function editPostSend($id, $name, $id_topic, $name_topic, $post_id, Request $request){

        $topic = ForumTopics::where('id',$id_topic)->first();


        $post = ForumPosts::where('id',$post_id)->first();
        $user = Auth::user();

        if($topic->lock && !$user->is_modo) return back();

        if($post->users_id != $user->id && !$user->is_modo){
            return back();
        }
        $post->message = $request->get('comment-text');
        $post->is_edit = true;
        $post->editor_name = $user->pseudo;

        $post->save();

        return redirect()->route('forumTopic', [$id,$name,$id_topic,$name_topic]);
    }

    // créer un topic (vue)

    public function createTopic($id, $name){

        if(Auth::user()->steamid == '' && $id == 4 || Auth::user()->steamid == null && $id == 4){
            Session::flash('need_steamid','Tu dois posseder un steamid pour poster une plainte');

            return redirect('/compte/steamid');
        }

        if($id == 8 && !Auth::user()->is_nems){
            // forum privé
            Session::flash('error', 'Cette section est réservée aux NemS');
            return redirect()->action('ForumController@index');
        }

        $category = ForumCategory::where('id',$id)->first();

        $topics = ForumTopics::where('forum_categories_id',$id)->orderBy('pinned', 'updated_at')->get();

        return view('forum/create_topic',compact('category','topics'));
    }

    // créer un topic (post)

    public function createTopicPost($id, $name, CreateTopicPostRequest $request){

        $forumTopic = new ForumTopics();
        $forumTopic->users_id = Auth::user()->id;
        $forumTopic->forum_categories_id = $id;
        $forumTopic->title = $request->get('title');
        if($request->get('pinned') == 'on') $forumTopic->pinned = true;
        $forumTopic->nb_posts += 1;
        $forumTopic->last_answers_name = Auth::user()->pseudo;
        $forumTopic->updated_at = Carbon::now();
        $forumTopic->save();

        $post = new ForumPosts();
        $post->users_id = Auth::user()->id;
        $post->forum_topics_id = $forumTopic->id;
        $post->message = $request->get('comment-text');

        $post->save();

        $this->updateCategoryTopicsCount($id,Auth::user()->id, $forumTopic->id);
        $this->updateCategoryPostsCount($id,Auth::user()->id, $forumTopic->id);

        if($id == 1){
            Historique::create([
                'type' => 'forum_news',
                'users_id' => Auth::user()->id,

            ]);
        }
        else{
            Historique::create([
                'type' => 'forum_topic',
                'users_id' => Auth::user()->id,

            ]);
        }


        Session::flash('success', 'Votre sujet a bien été créé');

        return redirect()->route('forumTopic', [$id,$name,$forumTopic->id,$forumTopic->title])->with('successNewTopic',true);

    }


    public function lockTopic($id,$name,$id_topic,$name_topic){
        if(Auth::user()->is_modo){
            $topic = ForumTopics::where('id',$id_topic)->first();
            if($topic->lock) $topic->lock = false;
            else $topic->lock = true;
            $topic->save();


        }
        return back();
    }

    public function pinnedTopic($id,$name,$id_topic,$name_topic){
        if(Auth::user()->is_modo){
            $topic = ForumTopics::where('id',$id_topic)->first();
            if($topic->pinned) $topic->pinned = false;
            else $topic->pinned = true;
            $topic->save();


        }
        return back();
    }

    public function updateCategoryPostsCount($id,$user_id,$topic_id){
        $category = ForumCategory::where('id',$id)->first();
        $category->nb_posts += 1;
        $category->last_user_id = $user_id;
        $category->last_topic_edit = $topic_id;
        $category->save();
    }
    public function updateCategoryTopicsCount($id,$user_id,$topic_id){
        $category = ForumCategory::where('id',$id)->first();
        $category->nb_topics += 1;
        $category->last_user_id = $user_id;
        $category->last_topic_edit = $topic_id;
        $category->save();
    }

    // supprime un post
    public function removePost($id, $name, $id_topic, $name_topic, $post_id){

        $user = Auth::user();

        if(!$user->is_modo){
            return back();
        }

        ForumPosts::destroy($post_id);
        $topic = ForumTopics::where('id',$id_topic)->first();
        $topic->nb_posts -= 1;
        $topic->save();

        $category = ForumCategory::where('id',$id)->first();
        $category->nb_posts -= 1;
        $category->save();

        return redirect()->route('forumTopic', [$id,$name,$id_topic,$name_topic]);
    }

}
