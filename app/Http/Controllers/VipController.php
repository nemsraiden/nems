<?php

namespace App\Http\Controllers;

use App\Config;
use App\Historique;
use App\MyClass\NemSClass;
use App\User;
use App\usersAction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Config $configs)
    {
        $user = Auth::user();
        $config = $configs->get()->first();

        $users = User::where('is_vip',1)->get();
        $users_link = [];
        foreach($users as $user_){
            $users_link[] = NemSClass::steamid_to_url($user_->steamid);
        }



        return view ('vip/index',compact('user','config','users','users_link'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Config $configs)
    {
        $user = Auth::user();

        if($user->steamid == '' || $user->steamid == null){
            Session::flash('need_steamid','Tu dois posseder un steamid pour devenir VIP');

            return redirect('/compte/steamid');
        }


        if($request->exists('vip_gratuit') && $user->vip_firsttime){
            $user->is_vip = true;
            $user->vip_credit = 5;
            $user->vip_grade = 'basic';
            $user->vip_firsttime = false;
            $user->vip_debut = Carbon::now();
            $user->vip_fin = Carbon::now()->addDays(5);

            $user->save();

            $usersAction = new usersAction();
            $usersAction->intitule = 'VIP - firs time';
            $usersAction->users_id = $user->id;
            $usersAction->type = 'VIP';
            $usersAction->description = 'Devenir vip pour la première fois';
            $usersAction->montant = 0;
            $usersAction->save();

            $grade = 'BASIC';
            $duree_txt = '5 jours';
            $success = true;
            $success_type = 'gratuit';
            return view('vip/confirmation',compact('success','grade','duree_txt','success_type'));
        }

        $config = $configs->get()->first();

        if($request->exists('vip_devenir') || $request->exists('vip_prolonger')) {

            $jetons = $user->jetons;
            $coins = $user->coins;
            $oldgrade = $user->vip_grade;
            $grade = $request->get('grade');
            $duree = $request->get('duree');
            $ok_continue = false;

            $usersAction = new usersAction();
            $usersAction->users_id = $user->id;

            if($request->exists('grade_prixcoins')){

                switch($grade){
                    case 'Basic': $prix = $config->vip_basic_coins; $credits = $config->vip_basic_credits; break;
                    case 'Medium': $prix = $config->vip_medium_coins; $credits = $config->vip_medium_credits; break;
                    case 'Gold': $prix = $config->vip_gold_coins; $credits = $config->vip_gold_credits; break;
                    case 'Ultimate': $prix = $config->vip_ultimate_coins; $credits = $config->vip_ultimate_credits; break;
                }
                if($duree == '1mois'){
                    $prix = ceil(($prix*2) - ((($prix*2) / 100) * 10));
                }
                if($prix > $coins){
                    Session::flash('need_more_coins',true);
                    return redirect('bajail/vip/#devenirvip');
                }
                else{
                    $ok_continue = true;
                    $user->coins -= $prix;
                }


            }

            else if($request->exists('grade_prixjetons')){
                switch($grade){
                    case 'Basic': $prix = $config->vip_basic_jetons; $credits = $config->vip_basic_credits; break;
                    case 'Medium': $prix = $config->vip_medium_jetons; $credits = $config->vip_medium_credits; break;
                    case 'Gold': $prix = $config->vip_gold_jetons; $credits = $config->vip_gold_credits; break;
                    case 'Ultimate': $prix = $config->vip_ultimate_jetons; $credits = $config->vip_ultimate_credits; break;
                }
                if($duree == '1mois'){
                    $prix = ceil(($prix*2) - ((($prix*2) / 100) * 10));
                }

                if($prix > $jetons){
                    Session::flash('need_more_jetons',true);
                    return redirect('bajail/vip/#devenirvip');
                }
                else{
                    $ok_continue = true;
                    $user->jetons -= $prix;
                }
            }



            if($ok_continue){

                $user->is_vip = true;
                $user->vip_debut = Carbon::now();
                $user->vip_grade = $grade;
                $user->vip_credit = $credits;

                if($duree == '2semaine'){
                    if($request->exists('vip_prolonger')){
                        $date = new Carbon($user->vip_fin);
                        $user->vip_fin = $date->addWeek(2);
                        $usersAction->intitule = 'VIP - Prolonger';
                    }
                    else{
                        $user->vip_fin = Carbon::now()->addWeek(2);
                        $usersAction->intitule = 'VIP - Devenir';
                    }


                    $duree_txt = '2 semaines';
                }
                else if($duree == '1mois'){
                    if($request->exists('vip_prolonger')){
                        $date = new Carbon($user->vip_fin);
                        $user->vip_fin = $date->addMonth(1);
                        $usersAction->intitule = 'VIP - Prolonger';
                    }
                    else{
                        $user->vip_fin = Carbon::now()->addMonth(1);
                        $usersAction->intitule = 'VIP - Devenir';
                    }

                    $duree_txt = '1 mois';
                }

                $usersAction->description = 'Modifier le status de VIP ('.$oldgrade.' -> '.$grade.')';

                $user->save();
                $success = true;
                $success_type = 'devenir';


                $usersAction->type = 'VIP';

                $usersAction->montant = $prix;
                $usersAction->save();

                Historique::create([
                    'type' => 'vip',
                    'users_id' => Auth::user()->id,

                ]);

                return view('vip/confirmation',compact('success','grade','duree_txt','success_type'));
            }

        }

    }


}
