<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/home', 'IndexController@index');


// Authentication routes...
Route::get('auth/', 'Auth\AuthController@index');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


// Compte
Route::get('/compte', array (
    'uses' => 'CompteController@index'
));
Route::get('/compte/modifier', array (
    'uses' => 'CompteController@edit'
));
Route::post('/compte/modifier', array (
    'uses' => 'CompteController@update'
));
Route::get('/compte/avatar', array (
    'uses' => 'CompteController@avatar'
));
Route::post('/compte/avatar', array (
    'uses' => 'CompteController@Postavatar'
));
Route::get('/compte/avatar/use/{id}', array (
    'uses' => 'CompteController@changeAvatar'
));
//info tooltip
Route::get('/info-tooltip/{id}', array (
    'uses' => 'ProfilController@infoTooltip',
));



// bajail
Route::get('/bajail', array (
    'uses' => 'BajailController@index'
));
Route::get('/bajail/charte', array (
    'uses' => 'BajailController@index_charte'
));
Route::get('/bajail/regles', array (
    'uses' => 'BajailController@index_regles'
));




// admin
Route::get('/bajail/admin', array (
    'uses' => 'BajailController@index_admin'
));
Route::get('/bajail/admin/devenir-admin', array (
    'uses' => 'DevenirAdminController@index'
));
Route::post('/bajail/admin/devenir-admin', array (
    'uses' => 'DevenirAdminController@post'
));
Route::get('/bajail/admin/devenir-admin/confirmation', array (
    'uses' => 'DevenirAdminController@confirmation',
    'middleware' => 'auth'
));




// test des règles
Route::get('/bajail/regles/test', array (
    'uses' => 'BajailController@index_test',
    'middleware' => 'auth'
));
Route::post('/bajail/regles/test', array (
    'uses' => 'BajailController@verifTest',
    'middleware' => 'auth'
));



// vocal
Route::get('/tchat', array (
    'uses' => 'tchatController@index'
));


// vip
Route::get('/bajail/vip', array (
    'uses' => 'VipController@index'
));

Route::post('/bajail/vip/confirmation', array (
    'uses' => 'VipController@store',
    'middleware' => 'auth'
));


// nems
Route::get('/team-nems', array (
    'uses' => 'NemsController@index'
));
Route::get('/team-nems/liste', array (
    'uses' => 'NemsController@liste'
));


// promo
Route::get('/promo', array (
    'uses' => 'PromoController@index'
));
// promo
Route::get('/promo/acheter/{id}', array (
    'uses' => 'PromoController@acheter',
    'middleware' => 'auth'
));

Route::get('/promo/acheter/{id}/valider', array (
    'uses' => 'PromoController@valider',
    'as' => 'acheterPromo',
    'middleware' => 'auth'
));



//steamid
Route::get('/compte/steamid', array (
    'uses' => 'CompteController@steamid',
    'middleware' => 'auth'
));

Route::post('/compte/steamid', array (
    'uses' => 'CompteController@steamidPost',
    'middleware' => 'auth'
));

Route::post('/compte/steamid/verif', array (
    'uses' => 'CompteController@steamidVerif',
    'middleware' => 'auth'
));


//profil
Route::get('/profil/{id}-{name}', array (
    'uses' => 'ProfilController@index',
    'as' => 'userProfil'
));



//jetons
Route::get('/jetons', array (
    'uses' => 'JetonsController@index',
));
Route::any('/jetons/check', array (
    'uses' => 'JetonsController@check',
));
Route::get('/jetons/valide', array (
    'uses' => 'JetonsController@valide',
));
Route::any('/jetons/valide2', array (
    'uses' => 'JetonsController@validestarpass',
));
Route::get('/jetons/erreur', array (
    'uses' => 'JetonsController@erreur',
));
Route::get('/jetons/paypal-trait', array (
    'uses' => 'JetonsController@checkPaypal',
));
Route::post('/jetons/paypal-trait', array (
    'uses' => 'JetonsController@checkPaypal',
));
Route::post('/jetons/valide-paypal', array (
    'uses' => 'JetonsController@validePaypal',
));


// coins
Route::get('/coins', array (
    'uses' => 'CoinsController@index',
));
Route::post('/coins', array (
    'uses' => 'CoinsController@post',
));



Route::group(['middleware' => ['auth']], function () {
    //forum, liste des forums
    Route::get('/forum', array (
        'uses' => 'ForumController@index'
    ));
// liste des forum dans une categorie
    Route::get('/forum/category/{id}-{name}', array (
        'uses' => 'ForumController@topicsList',
        'as' => 'forumCategory'
    ));

// créer un topic dans une catégorie
    Route::get('/forum/category/{id}-{name}/create', array (
        'uses' => 'ForumController@createTopic',
        'as' => 'createTopic',
        'middleware' => 'auth'
    ));
    Route::post('/forum/category/{id}-{name}/create', array (
        'uses' => 'ForumController@createTopicPost',
        'as' => 'createTopicPost',
        'middleware' => 'auth'
    ));


// affiche un topic
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}', array (
        'uses' => 'ForumController@topics',
        'as' => 'forumTopic'
    ));
// répondre par ajax a un topic (quick reply)
    Route::post('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}', array (
        'uses' => 'ForumController@replyTopicFast',
        'as' => 'forumTopic'
    ));

// lock topic
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/lock', array (
        'uses' => 'ForumController@lockTopic',
        'as' => 'lockTopic',
        'middleware' => 'auth'
    ));

// pinned un topic
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/pinned', array (
        'uses' => 'ForumController@pinnedTopic',
        'as' => 'pinnedTopic',
        'middleware' => 'auth'
    ));


// affiche la page de réponse normale a un topic
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/reply', array (
        'uses' => 'ForumController@replyTopics',
        'as' => 'replyTopic',
        'middleware' => 'auth'
    ));
// répondre normalement a un topic
    Route::post('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/reply', array (
        'uses' => 'ForumController@postReplyTopics',
        'middleware' => 'auth',
        'as' => 'replyTopicPost'
    ));


// répondre a un post en particulier d'un topic
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/reply/{post_id}', array (
        'uses' => 'ForumController@replyTopicsWithQuote',
        'as' => 'replyTopicWithQuote',
        'middleware' => 'auth'
    ));

    Route::post('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/reply/{post_id}', array (
        'uses' => 'ForumController@postReplyTopics',
        'middleware' => 'auth'
    ));


// modifier le post d'un topic
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/edit/{post_id}', array (
        'uses' => 'ForumController@EditPost',
        'as' => 'EditPost',
        'middleware' => 'auth'
    ));
// modifier le post d'un topic
    Route::post('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/edit/{post_id}', array (
        'uses' => 'ForumController@EditPostSend',
        'as' => 'EditPostSend',
        'middleware' => 'auth'
    ));


    // supprime un post
    Route::get('/forum/category/{id}-{name}/topics/{id_topic}-{name_topic}/remove/{post_id}', array (
        'uses' => 'ForumController@RemovePost',
        'as' => 'removePost',
        'middleware' => 'auth'
    ));
});








