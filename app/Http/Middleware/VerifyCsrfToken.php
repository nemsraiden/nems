<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/jetons/paypal-trait',
        '/jetons/check',
        '/jetons/valide',
        '/jetons/valide-paypal',
        '/jetons/*',
        'jetons/*'
    ];
}
