<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achat extends Model
{
    protected $fillable = array('users_id', 'type','amount','allopass_code','paypal_txn_id');
}
