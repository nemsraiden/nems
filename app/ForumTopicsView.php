<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumTopicsView extends Model
{
    protected $fillable = array('users_id', 'forum_topics_id', 'forum_category_id');
}
