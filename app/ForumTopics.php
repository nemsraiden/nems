<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ForumTopics extends Model
{
    public function forum_posts()
    {
        return $this->hasMany('App\ForumPosts','forum_topics_id','id');
    }
    public function users()
    {
        return $this->hasOne('App\User','id','users_id');
    }
    public function forum_category()
    {
        return $this->hasOne('App\ForumCategory','id','forum_categories_id');
    }

    public function getUpdatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->format('d/m/Y').' à '.$date->format('H:i');
    }

    public function getCreatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->format('d/m/Y');
    }

    public function getParentNameAttribute(){
        $parent = $this->forum_category()->first();
        return $parent->nom;

    }

}
