<?php

namespace App;

use App\MyClass\NemSClass;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded  = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $_serversAdmins = [];
    protected $_birthday_sql;

    public function usersAction()
    {
        return $this->hasMany('App\usersAction');
    }

    public function serversAdmins()
    {
        return $this->hasMany('App\serversAdmins');
    }

    public function getAdminFinAttribute(){
        if(empty($this->_serversAdmins)){
            $this->_serversAdmins = $this->serversAdmins->first();
        }
        return $this->_serversAdmins->date_fin;
    }

    public function getTypeDroitAttribute(){
        if(empty($this->_serversAdmins)){
            $this->_serversAdmins = $this->serversAdmins->first();
        }
        if($this->_serversAdmins->droit_fun){
            return 'Fun';
        }
        else{
            return 'Normal';
        }
    }
    public function getJeuxAttribute(){
        $jeux = [];
        if($this->jeux_css == 'on'){
            $jeux[] = 'images/icon_css.png';
        }
        if($this->jeux_csgo == 'on'){
            $jeux[] = 'images/icon_csgo.png';
        }
        if($this->jeux_lol == 'on'){
            $jeux[] = 'images/icon_lol.png';
        }
        return $jeux;
    }

    public function getVipFinFrAttribute(){
        $date = new Carbon($this->vip_fin);
        return $date->format('d/m/Y');
    }

    public function getVipDebutFrAttribute(){
        $date = new Carbon($this->vip_debut);
        return $date->format('d/m/Y');
    }

    public function getBirthdaySqlAttribute(){
        $this->birthday; // on simule l'appel de cet attribut
        return $this->_birthday_sql;
    }

    public function getBirthdayAttribute($value){
        $this->_birthday_sql = $value;
        if($value != null) {
            $timestamp = strtotime($value);
            $date = Carbon::createFromTimestamp($timestamp);
            return $date->format('d/m/Y');
        }
        else{
            return '';
        }
    }

    public function getSteamProfilAttribute(){
        return NemSClass::steamid_to_url($this->steamid);
    }

    public function getIsOnlineAttribute(){

        $online = Online::where('users_id',$this->id)->where('last_activity','>',strtotime('-15 min'))->first();

        if($online == null) return false;
        return true;
    }



}
