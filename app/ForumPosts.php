<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use tidy;

class ForumPosts extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User','users_id');
    }

    public function forum_topics()
    {
        return $this->belongsTo('App\ForumTopics','forum_topics_id','');
    }

    public function getDateAttribute(){
        $date = new Carbon($this->created_at);
        return $date->format('d/m/Y').' à '.$date->format('H:i');
    }
    public function getDateShortAttribute(){
        $date = new Carbon($this->created_at);
        return $date->format('d/m/Y');
    }

    public function getUpdatedAtAttribute($value){
        $date = new Carbon($value);
        return $date->format('d/m/Y').' à '.$date->format('H:i');
    }
    public function getMessageShortAttribute(){
        return $this->get_words($this->message);
    }

    function get_words($sentence, $count = 50) {
        $pieces = explode(" ", $sentence);
        $first_part = implode(" ", array_splice($pieces, 0, $count)).'...';
        $tidy = new Tidy();
        $tidy->parseString($first_part);
        $tidy->cleanRepair();

        echo $tidy;
    }
}
