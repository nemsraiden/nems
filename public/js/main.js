
$(document).ready(function() {


    $("label").each(function(){
        if($(this).closest('input[type="text"]').val() != 'undefinned'){
            $(this).addClass('active');
        }
    });

    if($("#tarif").html() != undefined){
        var value = $("input[type=radio][name=type_droit]:checked").val();

        prix_type_droit(value);

        $("input[type=radio][name=type_droit]").change(function () {

            prix_type_droit($(this).val());


        });
    }

    $("input[type=radio][name=grade]").change(function(){
        vip_devenir();
    });

    $("input[type=radio][name=duree]").change(function(){
        vip_devenir();
    });

    if($("#grade_prixcoins b").html() != ''){
        vip_devenir();
    }

    $(".btn-file-btn").click(function(){
    });
    $('input[type=file]').change(function(){
        $(".btn-file .btn-file-left").html($(this).val());
    });

    $('#checksteamid').click(function(){

        $(".show-infos-player").hide();
        $(".player-not-found").hide();

        $.ajax({
            url: '/compte/steamid/verif',
            type: "post",
            data: {'steamcommunity':$('input[name=steamcommunity]').val(), '_token': $('input[name=_token]').val()},
            success: function(data){
                if(data.reussis){
                    $(".player-not-found").hide();
                    $(".show-infos-player .s4 img").attr('src',data.avatar);
                    $(".show-infos-player .pseudo").html(data.pseudo);
                    $(".show-infos-player .steamid").html(data.$steamid);
                    $(".show-infos-player .steamid3").html(data.steamid3);
                    $(".show-infos-player").slideDown();
                }
                else{
                    $(".show-infos-player").hide();
                    $(".player-not-found").slideDown();
                }

            }
        });
    });

    $('#quick_reply_post').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        var action = $("#quick_reply_form").attr('action');

        var value = CKEDITOR.instances['ckeditor'].getData();

        $.ajax({
            url: action,
            type: "post",
            data: {'comment-text':value, '_token': $('input[name=_token]').val()},
            success: function(data){
                $("#forumPostsList").append(data);
                $(".forum-description").fadeIn(0300);
                $(".reply-box").fadeOut(0300);
                CKEDITOR.instances['ckeditor'].setData('');
                $('html,body').animate({
                        scrollTop: $("#forumPostsList .last").offset().top - 60},
                    'slow');


            }
        });
    });


    var slider = document.getElementById('jetonsslider');
    if(slider != null){
        noUiSlider.create(slider, {
            start: 0,
            connect: 'lower',
            step: 1,
            tooltips: [ true ],
            range: {
                'min': 3,
                'max': 20
            }
        });
        // When the slider value changes, update the input and span
        slider.noUiSlider.on('update', function ( values, handle ) {
            $("#nombre_jeton_payspal").val(values[handle]);
            $("#amount").val(values[handle]);
        });
    }




});

function vip_devenir(){

    var duree = $("input[type=radio][name=duree]:checked").val();
    var grade = $("input[type=radio][name=grade]:checked").val();


    var prix_grade_jetons = 0;
    var prix_grade_coins = 0;
    if(grade == 'Basic'){
        prix_grade_jetons = basic_jetons;
        prix_grade_coins = basic_coins
    }
    else  if(grade == 'Medium'){
        prix_grade_jetons = medium_jetons;
        prix_grade_coins = medium_coins
    }
    else if(grade == 'Gold'){
        prix_grade_jetons = gold_jetons;
        prix_grade_coins = gold_coins
    }
    else if(grade == 'Ultimate'){
        prix_grade_jetons = ultimate_jetons;
        prix_grade_coins = ultimate_coins
    }

    if(duree == '1mois'){
        prix_grade_jetons *= 2;
        prix_grade_coins *= 2;

        var remise_jetons = (prix_grade_jetons / 100) * 10;
        var remise_coins = (prix_grade_coins / 100) * 10;

        prix_grade_jetons = Math.ceil(prix_grade_jetons - remise_jetons);
        prix_grade_coins = Math.ceil(prix_grade_coins - remise_coins);


    }
    if(grade == "Ultimate") $("#grade_prixcoins").hide();
    else $("#grade_prixcoins").show();

    $("#grade_prixcoins b").html(prix_grade_coins);
    $("#grade_prixjetons b").html(prix_grade_jetons);
}

// sur la page pour devenir admin, gere le radio des droits
function prix_type_droit(value){

    if(value == "normal"){
        $("#tarif").html('4');
        tarif = parseInt($("#tarif_droit_normal").html());

    }
    if(value == "fun"){
        $("#tarif").html('6');
        tarif = parseInt($("#tarif_droit_fun").html());
    }
    var jetons = $("#nombre_jetons").html();
    if(jetons < tarif){
        $("#submit_droit").addClass('disabled');
        $("#submit_droit").prop("disabled",true);
    }
    else{
        $("#submit_droit").removeClass('disabled');
        $("#submit_droit").prop("disabled",false);
    }
}
