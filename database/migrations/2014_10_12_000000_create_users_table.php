<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login')->unique();;
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('pseudo')->unique();
            $table->string('steamid');
            $table->string('pays')->nullable();
            $table->string('jeux_csgo')->nullable();
            $table->string('jeux_css')->nullable();
            $table->string('jeux_lol')->nullable();
            $table->boolean('is_nems')->default(false);
            $table->integer('jetons')->default(0);
            $table->boolean('is_vip')->default(false);
            $table->integer('vip_credit')->nullable();
            $table->dateTime('vip_debut')->nullable();
            $table->dateTime('vip_fin')->nullable();
            $table->boolean('is_admin')->default(false);
            $table->string('bajail_test_etat')->nullable();
            $table->integer('bajail_test_bonne_reponsse')->default(0);
            $table->integer('bajail_test_mauvaise_reponsse')->default(0);
            $table->boolean('is_animateur')->default(false);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
